//
//  QuantityCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 27/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class QuantityCell: UICollectionViewCell {
    
    @IBOutlet var lblQuantity: CustomLabel!
}

//
//  CompareCell.swift
//  Addirham
//
//  Created by Haresh on 11/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class CompareCell: UICollectionViewCell
{
    //MARK:- Outlet zone
    @IBOutlet weak var imgOfProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblDesciption: UILabel!
    @IBOutlet weak var lblColorName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnAddToCartOutlet: CustomButton!
}

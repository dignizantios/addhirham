//
//  HomeLatestProductCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 25/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class HomeLatestProductCell: UICollectionViewCell {
    
    //MARK:- Outlet ZOne
    
    @IBOutlet var imgOfProduct: UIImageView!
    
    @IBOutlet var lblProductPrice: UILabel!
    @IBOutlet var lblProductName: UILabel!
    
}

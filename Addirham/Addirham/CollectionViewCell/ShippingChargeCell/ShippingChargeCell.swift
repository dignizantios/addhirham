//
//  ShippingChargeCell.swift
//  Addirham
//
//  Created by Haresh on 11/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class ShippingChargeCell: UICollectionViewCell
{
    @IBOutlet weak var btnSelectedChargeOutlet: UIButton!
    @IBOutlet weak var lblChargeName: UILabel!
    @IBOutlet weak var lblCharge: UILabel!
    
}

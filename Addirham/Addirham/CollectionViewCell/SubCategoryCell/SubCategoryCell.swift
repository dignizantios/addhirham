//
//  SubCategoryCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 01/11/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class SubCategoryCell: UICollectionViewCell {
    
    @IBOutlet var imgOfSubCategory: UIImageView!
    @IBOutlet weak var lblSubcategoryName: UILabel!
}

//
//  HomeCategoryCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 18/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class HomeCategoryCell: UICollectionViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet var imgOfCategory: CustomImageView!
    @IBOutlet var lblCategoryName: UILabel!
    
    
}

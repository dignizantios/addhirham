//
//  FilterColorCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 26/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class FilterColorCell: UICollectionViewCell {
    
    //MARK:- Outlet ZOne
    
    @IBOutlet weak var viewOfColor: UIView!
    @IBOutlet var imgOfColor: CustomImageView!
    
}

//
//  FilterCategoryCell.swift
//  Addirham
//
//  Created by Haresh on 11/9/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class FilterCategoryCell: UICollectionViewCell
{
    //MARK:- Outlet Zone2017
    
    @IBOutlet weak var lblCategoryName: CustomLabel!
}

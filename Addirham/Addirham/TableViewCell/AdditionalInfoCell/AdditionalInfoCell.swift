//
//  AdditionalInfoCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 27/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class AdditionalInfoCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet var viewOfSep: UIView!
    @IBOutlet var lblTitleName: CustomLabel!
    @IBOutlet var lblInfoName: CustomLabel!
    @IBOutlet var topOfStackView: NSLayoutConstraint!
    
    
    //MARK:- Tableview Life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

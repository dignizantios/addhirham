//
//  CartCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 27/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {
    
    //MARK:- Variable Declaration
    
    var pangesture : UIPanGestureRecognizer?
    var panstartpoint = CGPoint.zero
    var startingRightLayoutConstraintConstant: CGFloat = 0.0
    let kBounce : CGFloat = 20.0
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var viewOfEdit: CustomView!
    @IBOutlet weak var lblOutOfStock: UILabel!
    @IBOutlet var viewOfDelEdit: UIView!
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblSellerName: UILabel!
    @IBOutlet var lblNewPrice: UILabel!
    @IBOutlet var lblOldPrice: UILabel!
    @IBOutlet var btnAddOutlet: CustomButton!
    @IBOutlet var lblTotalQuantity: UILabel!
    @IBOutlet var btnMinusOutlet: CustomButton!
    @IBOutlet var btnDeleteOutlet: UIButton!
    @IBOutlet var btnEditOutlet: UIButton!
    @IBOutlet var leadingOfMainView: NSLayoutConstraint!
    @IBOutlet var trailingOfMainView: NSLayoutConstraint!
    @IBOutlet var viewOfOuter: CustomView!
    

    //MARK:- ViewLifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
       // pangesture = UIPanGestureRecognizer(target: self, action: #selector(panthiscell))
     //   pangesture?.delegate = self
      //  viewOfOuter.addGestureRecognizer(pangesture!)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    //MARK:- PanGestuew Method
    
   /* @objc func panthiscell(_ recognizer : UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            panstartpoint = recognizer.translation(in: viewOfOuter)
            startingRightLayoutConstraintConstant = trailingOfMainView.constant
            print("Panstart point : \(NSStringFromCGPoint(panstartpoint))")
        case .changed:
            let currentpoint : CGPoint = recognizer.translation(in: viewOfOuter)
            let deltaX: CGFloat = currentpoint.x - panstartpoint.x
            var panningleft : Bool = false
            if currentpoint.x < panstartpoint.x{
                panningleft = true
            }
            
            if startingRightLayoutConstraintConstant == 0{
                if !panningleft{
                    let constant:CGFloat = max(-deltaX, 0)
                    if constant == 0{
                        resetConstraintContstants(toZero: true, notifyDelegateDidClose: false)
                    }else{
                        trailingOfMainView.constant = constant
                    }
                }else{
                    let constant:CGFloat = min(-deltaX, btnwidth())
                    if constant == btnwidth(){
                        setConstraintsToShowAllButtons(animated: true, notifyDelegateDidOpen: false)
                    }else{
                        trailingOfMainView.constant = constant
                    }
                    
                }
            }else{
                let adjustment:CGFloat = startingRightLayoutConstraintConstant - deltaX
                if !panningleft{
                    let constant:CGFloat = max(adjustment, 0)
                    if constant == 0{
                        resetConstraintContstants(toZero: true, notifyDelegateDidClose: false)
                    }else{
                        trailingOfMainView.constant = constant
                    }
                }else{
                    let constant:CGFloat = min(adjustment, btnwidth())
                    if constant == btnwidth(){
                        setConstraintsToShowAllButtons(animated: true, notifyDelegateDidOpen: false)
                    }else{
                        trailingOfMainView.constant = constant
                    }
                    
                }
                
            }
            
            leadingOfMainView.constant = -trailingOfMainView.constant
            print("Pan move : \(deltaX)")
        case .ended:
            if startingRightLayoutConstraintConstant == 0 {
                
                let halfbutton : CGFloat = self.viewOfDelEdit.frame.width / 2
                
                if trailingOfMainView.constant >= halfbutton{
                    setConstraintsToShowAllButtons(animated: true, notifyDelegateDidOpen: true)
                }else{
                    resetConstraintContstants(toZero: true, notifyDelegateDidClose: true)
                }
            }else{
               // let buttononeplushalf : CGFloat = (self.vwplusminus.frame.width) + (self.vwdelete.frame.width / 2)
                let buttononeplushalf : CGFloat = (self.viewOfDelEdit.frame.width) //+ (self.viewOfDelEdit.frame.width / 2)
                
                if self.trailingOfMainView.constant >= buttononeplushalf {
                    self.setConstraintsToShowAllButtons(animated: true, notifyDelegateDidOpen: true)
                }
                else {
                    self.resetConstraintContstants(toZero: true, notifyDelegateDidClose: true)
                }
            }
            print("Pan ended")
        case .cancelled:
            if startingRightLayoutConstraintConstant == 0 {
                resetConstraintContstants(toZero: true, notifyDelegateDidClose: true)
            }
            else{
                setConstraintsToShowAllButtons(animated: true, notifyDelegateDidOpen: true)
            }
            print("pan is cancelled")
        default:
            print("DO something")
        }
    }
    
    func btnwidth() -> CGFloat{
        
        return frame.width - viewOfDelEdit.frame.minX
    }
    
    func resetConstraintContstants(toZero animated: Bool, notifyDelegateDidClose endEditing: Bool) {
        
        if endEditing {
            
            celldidClose(self)
        }
        
        if startingRightLayoutConstraintConstant == 0 && trailingOfMainView.constant == 0{
            return
        }
        self.trailingOfMainView.constant = -kBounce
        self.leadingOfMainView.constant = kBounce
        updateConstraintsIfNeeded(animated: animated, completion: { (finished:Bool) in
            self.leadingOfMainView.constant = 9
            self.trailingOfMainView.constant = 9
            self.updateConstraintsIfNeeded(animated: animated, completion: { (finished:Bool) in
                self.startingRightLayoutConstraintConstant = self.trailingOfMainView.constant
                
            })
        })
        
    }
    
    func setConstraintsToShowAllButtons(animated: Bool, notifyDelegateDidOpen notifyDelegate: Bool) {
        
        if notifyDelegate{
            celldidOpen(self)
        }
        
        if startingRightLayoutConstraintConstant == btnwidth() && trailingOfMainView.constant == btnwidth(){
            return
        }
        leadingOfMainView.constant = -btnwidth() - kBounce
        trailingOfMainView.constant = btnwidth() + kBounce
        updateConstraintsIfNeeded(animated: animated, completion: { (finished:Bool) in
            self.leadingOfMainView.constant = -self.btnwidth()
            self.trailingOfMainView.constant = self.btnwidth()
            self.updateConstraintsIfNeeded(animated: animated, completion: { (finished:Bool) in
                self.startingRightLayoutConstraintConstant = self.trailingOfMainView.constant
                
            })
        })
        
    }
    
    
    func updateConstraintsIfNeeded(animated: Bool, completion: @escaping (_ finished: Bool) -> Void) {
        var duration: Float = 0
        if animated {
            duration = 0.1
        }
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.layoutIfNeeded()
        }, completion: completion)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        resetConstraintContstants(toZero: false, notifyDelegateDidClose: false)
    }
    
    //MARK: - UIGestureRecognizerDelegate Method
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func celldidOpen(_ cell : UITableViewCell){
        //        let currentEditingIndexPath: IndexPath? = tableView.indexPath(for: cell)
        //        cellsCurrentlyEditing.append(currentEditingIndexPath)
    }
    
    func celldidClose(_ cell : UITableViewCell){
        
    }
    
    func opencell(){
        setConstraintsToShowAllButtons(animated: false, notifyDelegateDidOpen: false)
    }*/
    

}

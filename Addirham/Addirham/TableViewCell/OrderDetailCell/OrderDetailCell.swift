//
//  OrderDetailCell.swift
//  Addirham
//
//  Created by Jaydeep on 22/02/18.
//  Copyright © 2018 Jaydeep Virani. All rights reserved.
//

import UIKit

class OrderDetailCell: UITableViewCell {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblInnerDetail: UITableView!
    @IBOutlet weak var heightOfInnerTable: NSLayoutConstraint!
    @IBOutlet var lblShippingAddress: UILabel!
    @IBOutlet var lblOrderStatus: UILabel!
    @IBOutlet var lblOrderNo: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblTotalAmountPay: UILabel!
    @IBOutlet var lblShippingType: UILabel!
    @IBOutlet var lblTotalAmountSmall: UILabel!
    @IBOutlet var lblTotalAmountBig: UILabel!    
    @IBOutlet weak var lblDeliveryType: UILabel!
   
    
    //MARK:- View lifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

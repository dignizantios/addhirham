//
//  TrackOrderCell.swift
//  Addirham
//
//  Created by Haresh on 11/13/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class TrackOrderCell: UITableViewCell {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnUpparSepOutlet: UIButton!
    @IBOutlet weak var topOfProgressView: NSLayoutConstraint!
    @IBOutlet weak var viewOfProgressview: CustomView!
    @IBOutlet weak var btnOfBootomLineOutlet: UIButton!
    @IBOutlet weak var btnImgOfProgress: UIButton!
    @IBOutlet weak var btnProgressName: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    
    //MARK:- ViewLife cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

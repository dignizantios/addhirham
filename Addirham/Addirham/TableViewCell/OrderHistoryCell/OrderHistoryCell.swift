//
//  OrderHistoryCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 30/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class OrderHistoryCell: UITableViewCell {

    //MARK:- Outlet ZOne
    
    @IBOutlet weak var btnTrackOrderOutleet: CustomButton!
    @IBOutlet var imOfProduct: UIImageView!
    @IBOutlet var lblProduName: UILabel!
    @IBOutlet var lblProductPrice: UILabel!
  
    @IBOutlet var viewOfRatting: TQStarRatingView!
    @IBOutlet var btnViewOrderDetailOutlet: CustomButton!
    @IBOutlet var lblOrderDate: UILabel!
    @IBOutlet var lblPaymentMethod: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    //MARK:- ViewLifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  OtherAddressCell.swift
//  Addirham
//
//  Created by Jaydeep on 23/02/18.
//  Copyright © 2018 Jaydeep Virani. All rights reserved.
//

import UIKit

class OtherAddressCell: UITableViewCell {

    
    //MARK:- Outlet zone
    
    @IBOutlet weak var viewAnotherInner: UIView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblTelephone: UILabel!
    @IBOutlet weak var lblZipcode: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var btnDeleteOutlet: UIButton!
    @IBOutlet weak var btnEditOutlet: UIButton!
    
    //MARK:- ViewLife cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  RearCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 17/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class RearCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet var imgOfMenu: UIImageView!
    @IBOutlet var lblMenuName: UILabel!
    @IBOutlet var viewOfSep: UIView!
    

    //MARK:- TableView Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

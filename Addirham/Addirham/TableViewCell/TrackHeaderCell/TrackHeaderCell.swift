//
//  TrackHeaderCell.swift
//  Addirham
//
//  Created by Jaydeep on 22/02/18.
//  Copyright © 2018 Jaydeep Virani. All rights reserved.
//

import UIKit

class TrackHeaderCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var imgOfHeader: CustomImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblOrderNo: UILabel!
    

    //MARK:- View LifeCycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   

}

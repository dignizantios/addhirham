//
//  HomePoductCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 18/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class HomePoductCell: UITableViewCell
{
    
    //MARK:- Outlet Zone
    
    @IBOutlet var btnViewAllOutlet: CustomButton!
    @IBOutlet var collectionOfLatestPoduct: UICollectionView!
    @IBOutlet weak var lblTitleName: UILabel!
    
    
    //MARK:- ViewLife cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  CategoryCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 01/11/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    @IBOutlet var imgOfProduct: UIImageView!
    @IBOutlet var lblCategryName: UILabel!
    @IBOutlet var collectionOfCategory: UICollectionView!
    @IBOutlet var btnExpandOutlet: CustomButton!
    @IBOutlet var heightOfCollectionCategory: NSLayoutConstraint!
    
    //MARK:- ViewLifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  OverviewCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 28/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class OverviewCell: UITableViewCell {

    //MARK: -Outlet ZOne
    
    @IBOutlet var imgOfProduct: UIImageView!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblQuantity: UILabel!
    @IBOutlet var lblProductTotalPrice: UILabel!
    
    //MARK:- TAbleView life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

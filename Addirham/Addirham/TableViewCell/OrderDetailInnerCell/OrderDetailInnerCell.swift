//
//  OrderDetailInnerCell.swift
//  Addirham
//
//  Created by Jaydeep on 24/02/18.
//  Copyright © 2018 Jaydeep Virani. All rights reserved.
//

import UIKit

class OrderDetailInnerCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var imgOfProduct: CustomImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var lblTotalQuantity: UILabel!
    
    
    //MARK:- ViewLifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

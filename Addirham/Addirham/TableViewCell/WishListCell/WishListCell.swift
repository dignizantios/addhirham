//
//  WishListCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 30/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class WishListCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    @IBOutlet var imgOfProduct: UIImageView!
    @IBOutlet var lblProduName: UILabel!
    @IBOutlet var lblProductPrice: UILabel!
    @IBOutlet var lblStockStatus: UILabel!
    
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet var viewOfRatting: TQStarRatingView!
    @IBOutlet var btnAddCartOutlet: CustomButton!
    
    //MARK:- TableviewLifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

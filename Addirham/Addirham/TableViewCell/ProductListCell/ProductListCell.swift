//
//  ProductListCell.swift
//  Addirham
//
//  Created by Jaydeep Virani on 25/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class ProductListCell: UITableViewCell {
    
    //MARK:- Outletone
    
    @IBOutlet var imgOfProduct: UIImageView!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblProductOldPrice: UILabel!
    @IBOutlet var lblProductNewPrice: UILabel!
    @IBOutlet var collectionOfQuantity: UICollectionView!
    @IBOutlet var btnAddtoCartOutlet: CustomButton!
    @IBOutlet var btnBuyNowOutlet: CustomButton!
    @IBOutlet var btnFavOutlet: CustomButton!
    @IBOutlet var btnCompareOutlet: CustomButton!
    
    @IBOutlet var btnMinusOutlet: CustomButton!
    @IBOutlet var btnAddOutlet: CustomButton!
    @IBOutlet var lblNewOutlet: UILabel!
    
    //MARK:- ViewLife cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  PaymentCell.swift
//  Addirham
//
//  Created by Jaydeep on 28/02/18.
//  Copyright © 2018 Jaydeep Virani. All rights reserved.
//

import UIKit

class PaymentCell: UITableViewCell {
    
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnCheckedOutlet: UIButton!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    
    
    //MARK:- ViewLifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

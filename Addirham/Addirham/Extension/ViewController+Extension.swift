//
//  ViewController+Extension.swift
//  Addirham
//
//  Created by Jaydeep on 29/01/18.
//  Copyright © 2018 Jaydeep Virani. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController
{
    
    //MARK: - Image Choose Methods
    
    func ShowChooseImageOptions(picker : UIImagePickerController)
    {
        let alertController = UIAlertController(title: AppName, message: "Choose Option", preferredStyle: .actionSheet)
        
        let cameraButton = UIAlertAction(title: "Take a photo", style: .default, handler: { (action) -> Void in
            print("camera button tapped")
            self.openCamera(picker : picker)
        })
        
        let  galleryButton = UIAlertAction(title: "Select from library", style: .default, handler: { (action) -> Void in
            
            print("gallery button tapped")
            self.openGallary(picker : picker)
        })
        let  dismissButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(cameraButton)
        alertController.addAction(galleryButton)
        alertController.addAction(dismissButton)
        
        self.navigationController!.present(alertController, animated: true, completion: nil)
        
    }
    func openGallary(picker : UIImagePickerController)
    {
        picker.allowsEditing = true
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    func openCamera(picker : UIImagePickerController)
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerControllerSourceType.camera
            present(picker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title:"" , message: "Device not support to take photos", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func setUnselectedTab()
    {       
        appDelagte.tabBarController.tabBar.selectionIndicatorImage = UIImage()
        appDelagte.tabBarController.tabBar.items![0].image = #imageLiteral(resourceName: "ic_bottom_tab_home_unselected").withRenderingMode(.alwaysOriginal)
        appDelagte.tabBarController.tabBar.items![1].image = #imageLiteral(resourceName: "ic_bottom_tab_hot_deals_unselected").withRenderingMode(.alwaysOriginal)
        appDelagte.tabBarController.tabBar.items![2].image = #imageLiteral(resourceName: "ic_bottom_tab_my_cart_unselected").withRenderingMode(.alwaysOriginal)
        appDelagte.tabBarController.tabBar.items![3].image = #imageLiteral(resourceName: "ic_bottom_tab_favorite_unselected").withRenderingMode(.alwaysOriginal)
        appDelagte.tabBarController.tabBar.items![4].image = #imageLiteral(resourceName: "ic_bottom_tab_profile_unselected").withRenderingMode(.alwaysOriginal)
    }
    
    func setSelectedTab()
    {
        let tabBarItemSize = CGSize(width: (UIScreen.main.bounds.size.width / 5),
                                    height: 48)
        appDelagte.tabBarController.tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor(red: 90/255.0, green: 43/255.0, blue:123/255.0, alpha: 1.0), size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.zero)
        appDelagte.tabBarController.tabBar.items![0].selectedImage = #imageLiteral(resourceName: "ic_bottom_tab_home_selected")
        appDelagte.tabBarController.tabBar.items![1].selectedImage = #imageLiteral(resourceName: "ic_bottom_tab_hot_deals_selected")
        appDelagte.tabBarController.tabBar.items![2].selectedImage = #imageLiteral(resourceName: "ic_bottom_tab_my_cart_selected")
        appDelagte.tabBarController.tabBar.items![3].selectedImage = #imageLiteral(resourceName: "ic_bottom_tab_favorite_selected")
        appDelagte.tabBarController.tabBar.items![4].selectedImage = #imageLiteral(resourceName: "ic_bottom_tab_profile_selected")
    }
}

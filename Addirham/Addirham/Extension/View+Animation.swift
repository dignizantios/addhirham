//
//  View+Animation.swift
//  Networker
//
//  Created by haresh on 28/06/17.
//  Copyright © 2017 haresh. All rights reserved.
//

import Foundation


public extension UIView {
    
    func fadeIn(withDuration duration: TimeInterval = 0.5)
    {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    func fadeOut(withDuration duration: TimeInterval = 0.5)
    {
        UIView.animate(withDuration: duration, animations:
            {
                self.alpha = 0.0
        })
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}

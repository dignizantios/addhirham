//
//  Label+Extension.swift
//  Addirham
//
//  Created by Jaydeep Virani on 26/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import Foundation

public extension UILabel
{
    func roundLabelCorners(_ corners: UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        
    }
    
}

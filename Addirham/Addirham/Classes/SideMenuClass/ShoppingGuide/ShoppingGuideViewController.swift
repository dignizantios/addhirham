//
//  ShoppingGuideViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 01/11/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import NVActivityIndicatorView
import KSToastView

class ShoppingGuideViewController: UIViewController,NVActivityIndicatorViewable
{
    //MARK:- Variable Declaration
    
    var strScreenTitle = String()
    var strSubtitle = String()
    var strDescription = String()
    var strId = String()
    
     //MARK:- Outlet ZOne
    @IBOutlet weak var wenOfCMS: UIWebView!    
    @IBOutlet var lblShoppingGuideDescription: UILabel!
    @IBOutlet var btn_SideMenu: UIButton!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblScreenTitle: UILabel!
    
    //MARK:- ViewLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        self.lblScreenTitle.text = strScreenTitle
        self.lblSubTitle.text = strSubtitle
        //self.lblShoppingGuideDescription.text = strDescription
        getCMSBlock()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Service
    
    func getCMSBlock()
    {
        let kRegiURL = "\(AddhirhamMainURL)cmsBlock.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "static_id" :strId ] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)                        
                        self.wenOfCMS.loadHTMLString( json["CmsBlock"].stringValue, baseURL: nil)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

}

extension ShoppingGuideViewController:UIWebViewDelegate
{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(.zero)
    }
}

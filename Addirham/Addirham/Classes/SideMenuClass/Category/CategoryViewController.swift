//
//  CategoryViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 01/11/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView

class CategoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate
{
    
    //MARK:- Variable Declaration
    
    var arrCategory:[JSON] = []
    var strNoRecordMessage = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet var tblCatagory: UITableView!
    @IBOutlet var txtSeach: CustomTextField!
    @IBOutlet var btn_SideMenu: UIButton!
    @IBOutlet weak var lblTotalCount: UILabel!
    

    //MARK:- ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        txtSeach.delegate = self
        getCategoryList()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tblCatagory.addSubview(refreshControl)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblTotalCount.text = strTotalCartItem
        appDelagte.tabBarController.tabBar.isHidden = false
    }
    //MARK:- Refresh Method
    @objc func refresh() {
        getCategoryList()
        refreshControl.endRefreshing()
    }

    //MARK:- Textfield Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(obj, animated: true)
        return false
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnExpandAction(_ sender: UIButton)
    {
        var dictInner = arrCategory[sender.tag]
        let arrSubCategory = dictInner["child"]
        if arrSubCategory.count == 0
        {
            return
        }
        if dictInner["selected"].stringValue == "1"
        {
            dictInner["selected"] = "0"
        }
        else
        {
            dictInner["selected"] = "1"
        }
        arrCategory[sender.tag] = dictInner
        self.tblCatagory.reloadData()
    }
    
    @IBAction func btnCartAction(_ sender: UIButton)
    {
        goToRootOfTab(index: 2)
    }
    
     //MARK:- TableView Delegate Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrCategory.count == 0
        {
            let lbl = UILabel()
            lbl.text = strNoRecordMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:CategoryCell = self.tblCatagory.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
        let dictInner = arrCategory[indexPath.row]
        cell.lblCategryName.text = dictInner["category"].stringValue
        cell.imgOfProduct.sd_setImage(with: dictInner["image"].url, placeholderImage:#imageLiteral(resourceName: "phone"))
        cell.btnExpandOutlet.tag = indexPath.row
        cell.collectionOfCategory.tag = indexPath.row
        cell.collectionOfCategory.delegate = self
        cell.collectionOfCategory.dataSource = self
        cell.collectionOfCategory.reloadData()
        if dictInner["selected"].stringValue == "1"
        {
            if (dictInner["child"].arrayValue).count == 1
            {
                cell.heightOfCollectionCategory.constant = 70
            }
            else{
                cell.heightOfCollectionCategory.constant = CGFloat(((dictInner["child"].arrayValue).count/2)*70)
                
                cell.collectionOfCategory.contentSize.height
            }
            
            cell.btnExpandOutlet.isSelected = false
        }
        else
        {
            cell.heightOfCollectionCategory.constant = 0
            cell.btnExpandOutlet.isSelected = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    //MARK:- CollectionView Delegate & Datasources
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let dictCategory = arrCategory[collectionView.tag]
        let arrSubCategory = dictCategory["child"]
        return arrSubCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell:SubCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
        let dictCategory = arrCategory[collectionView.tag]
        let arrSubCategory = dictCategory["child"]
        let dictInnerQuantity = arrSubCategory[indexPath.row]
        cell.imgOfSubCategory.sd_setImage(with: dictInnerQuantity["image"].url, placeholderImage:#imageLiteral(resourceName: "phone"))
        cell.lblSubcategoryName.text = dictInnerQuantity["sub-category"].stringValue
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let Width = collectionView.frame.size.width/2
       // let Height:CGFloat = 42.0
        return CGSize(width: Width, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        let dictCategory = arrCategory[collectionView.tag]
        let arrSubCategory = dictCategory["child"]
        obj.dictCategory = arrSubCategory[indexPath.row]
        obj.dict = dictCategory
        obj.isComeFromCategory = true
      //  obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK:- Service
    
    func getCategoryList()
    {
        let kRegiURL = "\(AddhirhamMainURL)categoryListing.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey":HashKey] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        let dictData = json["category_listing"]
                        self.arrCategory = []
                        self.arrCategory = dictData["sucess"].arrayValue
                        for i in 0..<self.arrCategory.count
                        {
                            var dict = self.arrCategory[i]
                            if (dict["child"].arrayValue).count == 0
                            {
                                dict["selected"] = "1"
                            }
                            else
                            {
                                dict["selected"] = "0"
                            }
                            self.arrCategory[i] = dict
                        }
                        self.tblCatagory.reloadData()
                        
                    }
                    else
                    {
                        self.arrCategory = []
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblCatagory.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

}

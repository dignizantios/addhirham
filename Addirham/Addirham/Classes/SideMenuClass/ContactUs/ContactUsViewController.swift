//
//  ContactUsViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 01/11/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView

class ContactUsViewController: UIViewController,NVActivityIndicatorViewable,UITextFieldDelegate,UITextViewDelegate
{
    
    //MARK:- Variable Declaraion
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var txtUserName: CustomTextField!
    @IBOutlet weak var txtUserEmailAddress: CustomTextField!
    @IBOutlet weak var txtMobileNo: CustomTextField!
    @IBOutlet weak var txtViewComment: CustomTextview!
    @IBOutlet weak var lblTxtViewPlaceHolder: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblFaxNo: UILabel!
    @IBOutlet weak var lblEmailAddress: UILabel!
    @IBOutlet var btn_SideMenu: UIButton!

    //MARK:- ViewLife cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        [txtUserName,txtMobileNo,txtUserEmailAddress].forEach({(textfield) in
            textfield?.delegate = self
        })
        txtViewComment.delegate = self
       // let dictLoggedUser:JSON = getLoggedUserDetail()
        if dictLoggedUser["customerId"].stringValue != ""
        {
            self.txtUserName.text = dictLoggedUser["firstname"].stringValue
            self.txtUserEmailAddress.text = dictLoggedUser["email"].stringValue
        }
        if getUserAddressDetail("address_line") != ""
        {          
            self.txtMobileNo.text = getUserAddressDetail("telephone")
        }
        getContactUSList()
        txtViewComment.textContainerInset = UIEdgeInsets(top: 8, left: 10, bottom: 8, right: 10)
        // Do any additional setup after loading the view.
    }
    
    //MARK:- TextView Deleaget
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
           self.lblTxtViewPlaceHolder.isHidden = false
        }
        else
        {
            self.lblTxtViewPlaceHolder.isHidden = true
        }
    }

    //MARK: -Action Zone
    
    @IBAction func btnSubmitAction(_ sender: Any)
    {
        if ((self.txtUserName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
        {
            KSToastView.ks_showToast("Please enter first name", duration: ToastDuration)
        }
       
        else if ((self.txtUserEmailAddress.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
        {
            KSToastView.ks_showToast("Please enter email address", duration: ToastDuration)
        }
        else
        {
            let emailid: String = self.txtUserEmailAddress.text!
            let myStringMatchesRegEx: Bool = isValidEmail(emailAddressString: emailid)
            if myStringMatchesRegEx == false
            {
                KSToastView.ks_showToast("Please enter valid email address", duration: ToastDuration)
            }
            else if (self.txtMobileNo.text?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter mobile number", duration: ToastDuration)
            }
            else if (self.txtMobileNo.text?.characters.count)! < 8
            {
                KSToastView.ks_showToast("Please enter mobile number at least  8 character long", duration: ToastDuration)
            }
            else
            {
                self.view.endEditing(true)
                userContactus()
            }
        }
        
    }
    
    //MARK:- TextFiled Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobileNo
        {
            let limitLength = 10
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= limitLength // Bool
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    //MARK:- Service
    
    func getContactUSList()
    {
        let kRegiURL = "\(AddhirhamMainURL)contectUsFatch.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey
                         ] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        let dictOfContact = json["contectUsFatch"]["sucess"]
                        self.lblAddress.text = dictOfContact["city"].stringValue
                        self.lblMobileNo.text = dictOfContact["telephone"].stringValue
                        self.lblFaxNo.text = dictOfContact["state"].stringValue
                        self.lblEmailAddress.text = dictOfContact["email"].stringValue
                        
                    }
                    else
                    {
                       
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func userContactus()
    {
        let kRegiURL = "\(AddhirhamMainURL)customerContectMail.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "name" :self.txtUserName.text ?? "",
                         "email" :self.txtUserEmailAddress.text ?? "",
                         "comment" :self.txtViewComment.text ?? "",
                         "telephone" :self.txtMobileNo.text ?? "",]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        KSToastView.ks_showToast(json["customerContectMail"]["sucess"].stringValue, duration: ToastDuration)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

}

//
//  OtherAddressesVC.swift
//  Addirham
//
//  Created by Jaydeep on 23/02/18.
//  Copyright © 2018 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import NVActivityIndicatorView
import KSToastView

class OtherAddressesVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var arrAddressList:[JSON] = []
    var strNoRecordMessage = String()

    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblOtherAddress: UITableView!
    
    
    //MARK:- ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print("dictUser \(getLoggedUserDetail())")
        dictLoggedUser = getLoggedUserDetail()
        print("dictUser \(dictLoggedUser)")
        print("customerId ",dictLoggedUser["customerId"].stringValue)
        getUserAddressList()
        appDelagte.tabBarController.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appDelagte.tabBarController.tabBar.isHidden = true
         NotificationCenter.default.addObserver(self, selector: #selector(self.changeAddressList), name: NSNotification.Name(rawValue: "getAddressList"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
     //
    }
    
    //MARK:- Private Method
    
    @objc func changeAddressList()
    {
        dictLoggedUser = getLoggedUserDetail()
        getUserAddressList()
    }

    
    //MARK:- Action Zone
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "changeAddress"), object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDeleteAction(_ sender: UIButton) {
        let alertController = UIAlertController(title: AppName, message: "Are you sure want to delete this address?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
             let dict = self.arrAddressList[sender.tag]
            self.DeleteUserAddressList(strAddressId: dict["customer_address_id"].stringValue)
        }
        alertController.addAction(okAction)
        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action) in }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true)
    }
    
    @IBAction func btnEditAction(_ sender: UIButton)
    {
        let dict = self.arrAddressList[sender.tag]
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "EditShippingAddressVC") as! EditShippingAddressVC
        obj.dictAddress = dict
        obj.isComeFromEdit = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnNewAddressAction(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "EditShippingAddressVC") as! EditShippingAddressVC
        obj.isComeFromEdit = false
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

extension OtherAddressesVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrAddressList.count == 0
        {
            let lbl = UILabel()
            lbl.text = strNoRecordMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrAddressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherAddressCell") as! OtherAddressCell
        
        cell.btnEditOutlet.tag = indexPath.row
        cell.btnDeleteOutlet.tag = indexPath.row
        let dict = arrAddressList[indexPath.row]
        cell.lblAddress.text = dict["address_line"].stringValue
        cell.lblTelephone.text = dict["telephone"].stringValue
        cell.lblZipcode.text = dict["zipcode"].stringValue
        cell.lblCity.text = dict["city"].stringValue
        cell.lblState.text = dict["state"].stringValue
        cell.lblCountry.text = dict["country"].stringValue
        if dict["selected"].stringValue == "1"
        {
            cell.viewAnotherInner.backgroundColor = UIColor.init(red: 170/255, green: 170/255, blue: 170/255      , alpha: 0.3)
        }
        else
        {
            cell.viewAnotherInner.backgroundColor = UIColor.clear
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        for i in 0..<self.arrAddressList.count
        {
            var dict = self.arrAddressList[i]
            dict["selected"] = "0"
            self.arrAddressList[i] = dict
        }
        
        Defaults.removeObject(forKey: "userAddressDetail")
        Defaults.synchronize()
        var dict = self.arrAddressList[indexPath.row]
        guard let rowAdddata = try? dict.rawData() else {return}
        Defaults.setValue(rowAdddata, forKey: "userAddressDetail")
        Defaults.synchronize()
        
        dict["selected"] = "1"
        self.arrAddressList[indexPath.row] = dict
        self.tblOtherAddress.reloadData()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeAddress"), object: nil)
    }
}

extension OtherAddressesVC:NVActivityIndicatorViewable
{
    func getUserAddressList()
    {
        let kRegiURL = "\(AddhirhamMainURL)shippingAddressList.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "custid" :dictLoggedUser["customerId"].stringValue] as [String : Any]
            
            print("parm \(param)")
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.arrAddressList = []
                        self.arrAddressList = json["customer_address_list"]["sucess"].arrayValue
                        for i in 0..<self.arrAddressList.count
                        {
                            var dict = self.arrAddressList[i]
                            if dict["customer_address_id"].stringValue == self.getUserAddressDetail("customer_address_id")
                            {
                                dict["selected"] = "1"
                                Defaults.removeObject(forKey: "userAddressDetail")
                                Defaults.synchronize()
                                guard let rowAdddata = try? dict.rawData() else {return}
                                Defaults.setValue(rowAdddata, forKey: "userAddressDetail")
                                Defaults.synchronize()
                            }
                            else{
                                dict["selected"] = "0"
                            }
                            
                            self.arrAddressList[i] = dict
                        }
                        self.tblOtherAddress.reloadData()
                    }
                    else
                    {
                        self.arrAddressList = []
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblOtherAddress.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func DeleteUserAddressList(strAddressId:String)
    {
        let kRegiURL = "\(AddhirhamMainURL)cutomerAddressDelete.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "custid" :dictLoggedUser["customerId"].stringValue,
                         "addressid":strAddressId] as [String : Any]
            
            print("parm \(param)")
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.getUserAddressList()
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}

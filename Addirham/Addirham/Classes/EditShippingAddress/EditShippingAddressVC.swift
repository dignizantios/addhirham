//
//  EditShippingAddressVC.swift
//  Addirham
//
//  Created by Jaydeep on 23/02/18.
//  Copyright © 2018 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import NVActivityIndicatorView
import KSToastView

class EditShippingAddressVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var isComeFromEdit = Bool()
    var dictAddress:JSON?
    var arrOfCountry:[JSON] = []
    var arrOfRegion:[JSON] = []
    var strCountryId = String()
    var strRegionId = String()
    var StateCode = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet var txtLastName: UITextField!
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtEmailAddress: UITextField!
    @IBOutlet var txtMobileNo: UITextField!
    @IBOutlet var txtAddressLine: UITextField!
    @IBOutlet var txtStateName: UITextField!
    @IBOutlet var txtCityName: UITextField!
    @IBOutlet var txtZipcode: UITextField!
    @IBOutlet var txtCountryName: UITextField!
    
    //MARK:- ViewLifeCyce

    override func viewDidLoad() {
        super.viewDidLoad()

        [txtFirstName,txtLastName,txtEmailAddress,txtMobileNo,txtAddressLine,txtStateName,txtCityName,txtZipcode,txtCountryName].forEach({(textfield) in
            textfield?.delegate = self
        })
        
        dictLoggedUser = getLoggedUserDetail()
        if dictLoggedUser["customerId"].stringValue != ""
        {
            self.txtFirstName.text = dictLoggedUser["firstname"].stringValue
            self.txtLastName.text = dictLoggedUser["lastname"].stringValue
            self.txtEmailAddress.text = dictLoggedUser["email"].stringValue
        }
        
        if isComeFromEdit == true
        {
            self.txtAddressLine.text = dictAddress?["address_line"].stringValue
            self.txtCityName.text = dictAddress?["city"].stringValue
            self.txtZipcode.text = dictAddress?["zipcode"].stringValue
            self.txtMobileNo.text = dictAddress?["telephone"].stringValue
        }
        getCountryList()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "getAddressList"), object: nil)
    }

    //MARK:- Action Zone

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitAction(_ sender: Any) {
        if ((self.txtFirstName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
        {
            KSToastView.ks_showToast("Please enter first name", duration: ToastDuration)
        }
        else if ((self.txtLastName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
        {
            KSToastView.ks_showToast("Please enter last name", duration: ToastDuration)
        }
        else if ((self.txtEmailAddress.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
        {
            KSToastView.ks_showToast("Please enter email address", duration: ToastDuration)
        }
        else
        {
            let emailid: String = self.txtEmailAddress.text!
            let myStringMatchesRegEx: Bool = isValidEmail(emailAddressString: emailid)
            if myStringMatchesRegEx == false
            {
                KSToastView.ks_showToast("Please enter valid email address", duration: ToastDuration)
            }
            else if (self.txtMobileNo.text?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter mobile number", duration: ToastDuration)
            }
            else if (self.txtMobileNo.text?.characters.count)! < 8
            {
                KSToastView.ks_showToast("Please enter mobile number at least  8 character long", duration: ToastDuration)
            }
            else if ((self.txtAddressLine.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter address", duration: ToastDuration)
            }
            else if ((self.txtCityName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter city", duration: ToastDuration)
            }
            else if ((self.txtStateName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter state", duration: ToastDuration)
            }
            else if ((self.txtZipcode.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter zip code", duration: ToastDuration)
            }
            else if ((self.txtCountryName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please select country", duration: ToastDuration)
            }
            else
            {
                self.view.endEditing(true)
                if isComeFromEdit == true
                {
                    userAddressRegistrationUpdate()
                }
                else
                {
                    userAddressRegistration()
                }
            }
        }
        
    }
}

//MARK:- Textfiled delegate method

extension EditShippingAddressVC:UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == self.txtFirstName
        {
            return txtLastName.becomeFirstResponder()
        }
        else if textField == self.txtLastName
        {
            return txtEmailAddress.becomeFirstResponder()
        }
        else if textField == self.txtEmailAddress
        {
            return txtMobileNo.becomeFirstResponder()
        }
        else if textField == self.txtMobileNo
        {
            return txtAddressLine.becomeFirstResponder()
        }
        else if textField == self.txtAddressLine
        {
            return txtStateName.becomeFirstResponder()
        }
            
        else{
            return textField.resignFirstResponder()
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == txtCountryName
        {
            self.view.endEditing(true)
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommomPickerVC") as! CommomPickerVC
            obj.PickerDelegate = self
            obj.arrPicker = arrOfCountry
            obj.pickerType = 0
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true, completion: nil)
            return false
        }
        else if textField == txtStateName
        {
            self.view.endEditing(true)
            if self.txtCountryName.text == ""
            {
                KSToastView.ks_showToast("Please select country", duration: ToastDuration)
            }
            else if arrOfRegion.count == 0
            {
                getRegionList()
            }
            else
            {
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommomPickerVC") as! CommomPickerVC
                obj.PickerDelegate = self
                obj.arrPicker = arrOfRegion
                obj.pickerType = 1
                obj.modalPresentationStyle = .overCurrentContext
                obj.modalTransitionStyle = .crossDissolve
                self.present(obj, animated: true, completion: nil)
            }
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobileNo
        {
            let limitLength = 10
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= limitLength // Bool
        }
        else if textField == txtZipcode
        {
            let limitLength = 6
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= limitLength
        }
        return true
    }
}

//MARK:- Delegate Method

extension EditShippingAddressVC : CommonPickerDelegate
{
    func setName(strCode:String, strName:String,type: Int,strStateCode:String)
    {
        if type == 0
        {
            strCountryId = strCode
            self.txtCountryName.text = strName
            self.txtStateName.text = ""
            getRegionList()
        }
        else
        {
            StateCode = strStateCode
            strRegionId = strCode
            self.txtStateName.text = strName
        }
    }
}

 //MARK:- Service

extension EditShippingAddressVC:NVActivityIndicatorViewable
{
    func getCountryList()
    {
        let kRegiURL = "\(AddhirhamMainURL)countrylist.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.arrOfCountry = []
                        self.arrOfCountry = json["countryid"].arrayValue
                        //if self.getUserAddressDetail("country") != ""
                       // {
                            for i in 0..<self.arrOfCountry.count{
                                let dict = self.arrOfCountry[i]
                                if dict["countryID"].stringValue == self.dictAddress?["country_id"].stringValue
                                {
                                    self.strCountryId = dict["countryID"].stringValue
                                    self.txtCountryName.text = dict["countryName"].stringValue
                                    self.getRegionList()
                                    break
                                }
                            }
                        
                        //}
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func getRegionList()
    {
        let kRegiURL = "\(AddhirhamMainURL)regionList.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "country_code":strCountryId] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.arrOfRegion = []
                        self.arrOfRegion = json["region_code"].arrayValue
                       // if self.getUserAddressDetail("state") != ""
                       // {
                            for i in 0..<self.arrOfRegion.count{
                                let dict = self.arrOfRegion[i]
                                if dict["state_id"].stringValue == self.dictAddress?["state_id"].stringValue
                                {
                                    self.strRegionId = dict["state_id"].stringValue
                                    self.txtStateName.text = dict["name"].stringValue
                                    self.StateCode = dict["state_code"].stringValue
                                    break
                                }
                            }
                        
                        //}
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func userAddressRegistrationUpdate()
    {
        let kRegiURL = "\(AddhirhamMainURL)cutomerAddressUpdate.php"
        let strUserId:String = dictLoggedUser["customerId"].stringValue
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let strUserAddressId = dictAddress?["customer_address_id"].stringValue ?? ""
            
            let param = ["hashkey" :HashKey,
                         "customer_address_id" :strUserAddressId,
                         "cust_id" :strUserId,
                         "firstname" :self.txtFirstName.text ?? "",
                         "lastname" :self.txtLastName.text ?? "",
                         "street" :self.txtAddressLine.text ?? "",
                         "city" :self.txtCityName.text ?? "",
                         "region" :StateCode,
                         "region_id":strRegionId,
                       //  "region" :strRegionId,
                        // "region_id":StateCode, //Changed
                         "postcode" :self.txtZipcode.text ?? "",
                         "country_id" :strCountryId,
                         "telephone" :self.txtMobileNo.text ?? "",
                         "defaultbill" :"true",
                         "defaultship" :"true"]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        if self.dictAddress?["customer_address_id"].stringValue == self.getUserAddressDetail("customer_address_id")
                        {
                            let dictAddress = json["customerAddressUpdate"]["sucess"]
                            print("dictAddress \(dictAddress)")
                            guard let rowdata = try? dictAddress.rawData() else {return}
                            Defaults.setValue(rowdata, forKey: "userAddressDetail")
                            Defaults.synchronize()
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeAddress"), object: nil)
                        }
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getAddressList"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func userAddressRegistration()
    {
        let kRegiURL = "\(AddhirhamMainURL)cutomerAddressCreate.php"
        let strUserId:String = dictLoggedUser["customerId"].stringValue
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "custid" :strUserId,
                         "firstname" :self.txtFirstName.text ?? "",
                         "lastname" :self.txtLastName.text ?? "",
                         "street" :self.txtAddressLine.text ?? "",
                         "city" :self.txtCityName.text ?? "",
                         "region" :StateCode,
                         "postcode" :self.txtZipcode.text ?? "",
                         "country_id" :strCountryId,
                         "telephone" :self.txtMobileNo.text ?? "",
                     //    "region_id":strRegionId,
                         "region" :strRegionId,
                        // "region_id":StateCode, //Changed
                         "defaultbill" :"true",
                         "defaultship" :"true"]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                      /*  let dictAddress = json["customerAddressCreate"]["sucess"]
                        print("dictAddress \(dictAddress)")
                        guard let rowdata = try? dictAddress.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userAddressDetail")
                        Defaults.synchronize()*/
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getAddressList"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}

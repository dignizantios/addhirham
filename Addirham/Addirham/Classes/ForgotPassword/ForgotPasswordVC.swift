//
//  ForgotPasswordVC.swift
//  Addirham
//
//  Created by Jaydeep on 22/02/18.
//  Copyright © 2018 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import NVActivityIndicatorView
import KSToastView

class ForgotPasswordVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var txtEmailAddress: UITextField!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnSendAction(_ sender: Any) {
        if ((self.txtEmailAddress.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
        {
            KSToastView.ks_showToast("Please enter email address", duration: ToastDuration)
        }
        else
        {
            let emailid: String = self.txtEmailAddress.text!
            let myStringMatchesRegEx: Bool = isValidEmail(emailAddressString: emailid)
            if myStringMatchesRegEx == false
            {
                KSToastView.ks_showToast("Please enter valid email address", duration: ToastDuration)
            }
            else
            {
                self.view.endEditing(true)
                UserForgotPassword()
            }
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
       self.dismiss(animated: false, completion: nil)
    }
    
}

//MARK:- Service

extension ForgotPasswordVC:NVActivityIndicatorViewable
{
    func UserForgotPassword()
    {
        let kRegiURL = "\(AddhirhamMainURL)index.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey":HashKey,
                         "email" :self.txtEmailAddress.text ?? ""]
            
            print("parm \(param)")
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        KSToastView.ks_showToast(json["Orderlist"]["sucess"].stringValue, duration: ToastDuration)
                        self.dismiss(animated: false, completion: nil)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}

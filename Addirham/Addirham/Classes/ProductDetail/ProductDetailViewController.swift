//
//  ProductDetailViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 27/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView

class ProductDetailViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable,UITextFieldDelegate
{
    //MARK:- Variable Declaration
    
    var arrOfAdditionalInfo:[JSON] = []
    var dict = JSON()
    var dictProductDetail:JSON?
    var strQuantiy = String()
    var isComeFromCompare = Bool()
    var strProductId = String()
    var timer = Timer()
   // var dictLoggedUser = JSON()
    
    //MARK:- Outlet ZOne
    
    @IBOutlet var scrollOfProduct: UIScrollView!
    @IBOutlet var pageControlOfProduct: UIPageControl!
    @IBOutlet var tblAdditionalInfo: UITableView!
    @IBOutlet var collectionOfQunatity: UICollectionView!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var viewOfRattingProduct: TQStarRatingView!
    @IBOutlet var lblProdutPrice: UILabel!
    @IBOutlet var lblProdutDescription: UILabel!
    @IBOutlet var btnFavoriteOutlet: CustomButton!
    @IBOutlet var btnCompareOutlet: CustomButton!
    @IBOutlet var lblStockStatus: UILabel!
    @IBOutlet var heightOftblAdditionalInfo: NSLayoutConstraint!
    @IBOutlet var txtTag: UITextField!
    @IBOutlet var btnAddOutlet: UIButton!
    @IBOutlet var btnMinusOutlet: UIButton!
    @IBOutlet weak var widthOfCollectionview: NSLayoutConstraint!
    @IBOutlet weak var lblTotalCount: UILabel!
    
    
    //MARK:- ViewLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()

        appDelagte.tabBarController.tabBar.isHidden = true
      //  btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        getProductDetailList()
        [txtTag].forEach({(textfield) in
            textfield?.delegate = self
        })
        print("dict \(dict)")
        if isComeFromCompare == false
        {
            let arrQuntity = dict["arrquantiy"]
            if dict["qty"].intValue <= 5
            {
                self.btnAddOutlet.isHidden = true
                self.btnMinusOutlet.isHidden = true
               // self.widthOfCollectionview.constant = (CGFloat((dict["qty"].intValue) * 35))
            }
            else
            {
                self.btnAddOutlet.isHidden = false
                self.btnMinusOutlet.isHidden = false
                //self.widthOfCollectionview.constant = (CGFloat(5 * 35))
            }
            for i in 0..<arrQuntity.count
            {
                let dict = arrQuntity[i]
                if dict["selected"].stringValue == "1"
                {
                    strQuantiy = dict["quanity_name"].stringValue
                    self.collectionOfQunatity.scrollToItem(at: IndexPath(item: i, section: 0), at: .left, animated: false)
                    break
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lblTotalCount.text = strTotalCartItem
        dictLoggedUser = getLoggedUserDetail()
        tblAdditionalInfo.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        timer.invalidate()
        tblAdditionalInfo.removeObserver(self, forKeyPath: "contentSize")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ListRefresh"), object: nil)
    }


    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Setup Quantity Array
    
    func SetupQuantityArray(qnt:Int) ->  [JSON]
    {
        var arrOfQuantity:[JSON] = []
        for i in 0..<qnt
        {
            var dict = [String:String]()
            dict["quanity_name"] = String(i+1)
            dict["selected"] = "0"
            arrOfQuantity.append(JSON(dict))
        }
        return arrOfQuantity
    }
    
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblAdditionalInfo.contentSize.height)")
            self.heightOftblAdditionalInfo.constant = tblAdditionalInfo.contentSize.height
            //let finalHeight = self.heightOftblCommon.constant + 100
            // print("finalHeight:= \(finalHeight)")
            
        }
    }

    //MARK: - textfield delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    
    //MARK:- Private Zone
    
    func setupProductDetail()
    {
        self.arrOfAdditionalInfo = []
        var dict = [String:String]()
        dict["title"] = "Brand"
        dict["name"] = self.dictProductDetail?["brand"].stringValue ?? ""
        self.arrOfAdditionalInfo.append(JSON(dict))
        dict = [String:String]()
        dict["title"] = "Color"
        dict["name"] = self.dictProductDetail?["color"].stringValue ?? ""
        self.arrOfAdditionalInfo.append(JSON(dict))
        dict = [String:String]()
        dict["title"] = "Size"
        dict["name"] = self.dictProductDetail?["size"].stringValue ?? ""
        self.arrOfAdditionalInfo.append(JSON(dict))
       /* dict = [String:String]()
        dict["title"] = "Image Galary"
        dict["name"] = self.dictProductDetail?["image_gallary"].stringValue ?? ""
        self.arrOfAdditionalInfo.append(JSON(dict))*/
        self.tblAdditionalInfo.reloadData()
        setUpImageSlider()
        self.lblProductName.text = self.dictProductDetail?["name"].stringValue ?? ""
        self.lblProdutPrice.text = "OMR \(self.dictProductDetail?["price"].stringValue ?? "0")"
        self.viewOfRattingProduct.setScore((self.dictProductDetail?["reviews"].floatValue ?? 0)/5, withAnimation: false)
        self.lblProdutDescription.text = self.dictProductDetail?["description"].stringValue ?? ""
      //  self.collectionOfQunatity.scrollToItem(at: IndexPath(item: (self.dictProductDetail["qty"].intValue), section: 0), at: .left, animated: false)
        if self.dictProductDetail?["avalable"].boolValue ?? false == true
        {
            self.lblStockStatus.text = "In Stock"
        }
        else
        {
            self.lblStockStatus.text = "Out of Stock"
        }
        
        if self.dictProductDetail?["wish_list"].boolValue ?? false == false
        {
            self.btnFavoriteOutlet.isSelected = false
            self.btnFavoriteOutlet.backgroundColor = UIColor.white
        }
        else
        {
            self.btnFavoriteOutlet.isSelected = true
            self.btnFavoriteOutlet.backgroundColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1)
        }
        
        for i in 0..<strComapareID.count
        {
            if strComapareID[i] == dictProductDetail?["product_id"].stringValue
            {
                self.btnCompareOutlet.isSelected = true
                self.btnCompareOutlet.backgroundColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1)
                break
            }
        }
        
        if isComeFromCompare == true
        {
            isComeFromCompare = false
            let arrQnt = self.SetupQuantityArray(qnt:dictProductDetail?["qty"].intValue ?? 0)
            self.dict["arrquantiy"] = JSON(arrQnt)
            self.collectionOfQunatity.reloadData()
            
            if dictProductDetail?["qty"].intValue ?? 0 <= 5
            {
                self.btnAddOutlet.isHidden = true
                self.btnMinusOutlet.isHidden = true
            }
           
        }
    }
    
    func setUpImageSlider()
    {
        let arrOfSlider = dictProductDetail?["image"].arrayValue ?? []
        self.pageControlOfProduct.currentPage = 0
        self.pageControlOfProduct.numberOfPages = arrOfSlider.count
       
        var x = CGFloat()
        x = 0
        
        for i in 0..<arrOfSlider.count
        {
            let imgOfPromotion = UIImageView()
            imgOfPromotion.contentMode = .scaleAspectFit
          //  imgOfPromotion.clipsToBounds = true
            imgOfPromotion.frame = CGRect(x: x, y: 10, width:self.scrollOfProduct.frame.size.width, height: 230)
            x += self.scrollOfProduct.frame.size.width
            let strImage = arrOfSlider[i].stringValue
            let urlImage:URL = URL(string: strImage)!
            imgOfPromotion.sd_setImage(with: urlImage as URL, placeholderImage:#imageLiteral(resourceName: "phone"))
            //imgOfPromotion.layer.cornerRadius = 5
            self.scrollOfProduct.addSubview(imgOfPromotion)
        }
        
        self.scrollOfProduct.contentSize = CGSize(width:x, height: scrollOfProduct.frame.size.height)
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    
    @objc func update()
    {
        var scrollxpos = scrollOfProduct.contentOffset.x
        
        var visibleRect = CGRect()
        
        if scrollxpos + scrollOfProduct.frame.size.width == scrollOfProduct.contentSize.width
        {
            scrollxpos = 0
            visibleRect = CGRect(x: scrollxpos, y: scrollOfProduct.frame.origin.y, width: scrollOfProduct.frame.size.width, height: scrollOfProduct.frame.size.height)
            let pageNumber = round(scrollxpos / scrollOfProduct.frame.size.width)
            pageControlOfProduct.currentPage = Int(pageNumber)
        }
        else
        {
            visibleRect = CGRect(x: (scrollxpos +  scrollOfProduct.frame.size.width), y: scrollOfProduct.frame.origin.y, width: scrollOfProduct.frame.size.width, height: scrollOfProduct.frame.size.height)
            let pageNumber = round((scrollxpos +  scrollOfProduct.frame.size.width)/scrollOfProduct.frame.size.width)
            pageControlOfProduct.currentPage = Int(pageNumber)
        }
        
        scrollOfProduct.scrollRectToVisible(visibleRect, animated: true)
        
    }
    
  
    
    //MARK:- Collection View delagte

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if isComeFromCompare == true
        {
            return 0
        }
        let arrQuntity = dict["arrquantiy"]
        return arrQuntity.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:QuantityCell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuantityCell", for: indexPath) as! QuantityCell
        let arrQuntity = dict["arrquantiy"]
        let dictInnerQuantity = arrQuntity[indexPath.row]
        cell.lblQuantity.text = dictInnerQuantity["quanity_name"].stringValue
        cell.lblQuantity.cornerRadius = cell.lblQuantity.frame.size.width/2
        cell.lblQuantity.clipsToBounds = true
        if dictInnerQuantity["selected"].stringValue == "1"
        {
            strQuantiy = dictInnerQuantity["quanity_name"].stringValue
            cell.lblQuantity.textColor = UIColor.white
            cell.lblQuantity.backgroundColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1)
        }
        else
        {
            cell.lblQuantity.textColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1)
            cell.lblQuantity.backgroundColor = UIColor.white
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = collectionView.frame.size.height
        if dictProductDetail?["qty"].intValue ?? 0 <= 5
        {
            return CGSize(width: 30, height: height)
        }
        let width = collectionView.frame.size.width/5
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        var arrQuntity = dict["arrquantiy"]
        for i in 0..<arrQuntity.count{
            var dictInner = arrQuntity[i]
            dictInner["selected"] = "0"
            arrQuntity[i] = dictInner
        }
        dict["arrquantiy"] = arrQuntity
        var dictInnerQuantity = arrQuntity[indexPath.row]
        dictInnerQuantity["selected"] = "1"
        arrQuntity[indexPath.row] = dictInnerQuantity
        dict["arrquantiy"] = arrQuntity
        
        self.collectionOfQunatity.reloadData()
    }
    
    //MARK:- TableView Delegate Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfAdditionalInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AdditionalInfoCell = self.tblAdditionalInfo.dequeueReusableCell(withIdentifier: "AdditionalInfoCell") as! AdditionalInfoCell
        let dictData = arrOfAdditionalInfo[indexPath.row]
        cell.lblTitleName.text = dictData["title"].stringValue
        cell.lblInfoName.text = dictData["name"].stringValue
        if indexPath.row == 0
        {
            cell.topOfStackView.constant = 1
        }
        else
        {
            cell.topOfStackView.constant = 0
        }
        cell.viewOfSep.isHidden = false
       /* if indexPath.row == arrOfAdditionalInfo.count-1
        {
            cell.viewOfSep.isHidden = false
        }
        else
        {
           cell.viewOfSep.isHidden = true
        }*/
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMinusAction(_ sender: Any) {
//        let currentIndex:Int = dict["current_index"].intValue
//        if currentIndex == 4
//        {
//            return
//        }
//        self.collectionOfQunatity.scrollToItem(at: IndexPath(item: currentIndex-1, section: 0), at:.left, animated: true)
//        dict["current_index"] = JSON(currentIndex-1)
        print("Visible Cell \(collectionOfQunatity.indexPathsForVisibleItems)")
        let arrOfIndexpath:[IndexPath] = collectionOfQunatity.indexPathsForVisibleItems
        let sortedArray = arrOfIndexpath.sorted {$0.row < $1.row}
        print("sortedArray \(arrOfIndexpath.sorted {$0.row < $1.row})")
        
        let lastIndexPath = sortedArray[0]
        if lastIndexPath.row == 0
        {
            return
        }
        collectionOfQunatity.scrollToItem(at: IndexPath(item: (lastIndexPath.row) - 1, section: 0), at:.left, animated: true)
    }
    @IBAction func btnAddAction(_ sender: Any) {
       
//        let currentIndex:Int = dict["current_index"].intValue
        let arrQuntity = dict["arrquantiy"]
//        if currentIndex == (arrQuntity.count - 1)
//        {
//            return
//        }
//        self.collectionOfQunatity.scrollToItem(at: IndexPath(item: currentIndex+1, section: 0), at:.right, animated: true)
//        dict["current_index"] = JSON(currentIndex+1)
        
        let arrOfIndexpath:[IndexPath] = collectionOfQunatity.indexPathsForVisibleItems
        let sortedArray = arrOfIndexpath.sorted {$0.row < $1.row}
        print("sortedArray \(arrOfIndexpath.sorted {$0.row < $1.row})")
        
        if sortedArray.count == 6
        {
            collectionOfQunatity.scrollToItem(at: sortedArray[5], at:.right, animated: true)
        }
        else
        {
            let lastIndexPath = sortedArray[4]
            if (lastIndexPath.row) + 1 == arrQuntity.count
            {
                return
            }
            collectionOfQunatity.scrollToItem(at: IndexPath(item: (lastIndexPath.row) + 1, section: 0), at:.right, animated: true)
        }
    }
    @IBAction func btnFavoriteAction(_ sender: UIButton)
    {
        if dictLoggedUser["customerId"].stringValue == ""
        {
            let alertController = UIAlertController(title: AppName, message: "You are not access this feature because you are not login!!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Login", style: .default) { (action) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let obj = storyboard.instantiateViewController(withIdentifier: "ShippingAddressViewController") as! ShippingAddressViewController
               // obj.modalPresentationStyle = .overCurrentContext
             //   obj.modalTransitionStyle = .crossDissolve
                self.present(obj, animated: true, completion: nil)
            }
            alertController.addAction(okAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true)
        }
        else
        {
            if self.dictProductDetail?["wish_list"].boolValue ?? false == true
            {
                KSToastView.ks_showToast("Product alredy added to wishlist.", duration: ToastDuration)
                return
            }
            if isComeFromCompare == true
            {
               addToWishList(strProductID:strProductId,strCustId:dictLoggedUser["customerId"].stringValue)
            }
            else
            {
                addToWishList(strProductID:dict["product_id"].stringValue,strCustId:dictLoggedUser["customerId"].stringValue)
            }
        
        }
    }
    @IBAction func btnComapareAction(_ sender: Any)
    {
        if dictProductDetail?["is_compare"].stringValue ?? "0" == "1"
        {
            for i in 0..<strComapareID.count
            {
                if strComapareID[i] == dictProductDetail?["product_id"].stringValue ?? "0"
                {
                    strComapareID.remove(at: i)
                    dictProductDetail?["is_compare"] = "0"
                    self.btnCompareOutlet.isSelected = false
                    self.btnCompareOutlet.backgroundColor = UIColor.white
                    break
                }
            }
            return
        }
        if strComapareID.count == 2
        {
            KSToastView.ks_showToast("You can compare only two items.", duration: ToastDuration)
            return
        }
        if isComeFromCompare == true
        {
            strComapareID.append(strProductId)
        }
        else{
          strComapareID.append(dict["product_id"].stringValue)
        }
        
        if strComapareID[0] == "0"
        {
            strComapareID.remove(at: 0)
        }
        dictProductDetail?["is_compare"] = "1"
        self.btnCompareOutlet.isSelected = true
        self.btnCompareOutlet.backgroundColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ListRefresh"), object: nil)
    }
    
    @IBAction func btnAddTagAction(_ sender: Any)
    {
        if dictLoggedUser["customerId"].stringValue == ""
        {
            KSToastView.ks_showToast("Please fisrt login", duration: ToastDuration)
            return
        }
        if ((self.txtTag.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
        {
            KSToastView.ks_showToast("Please enter tag", duration: ToastDuration)
        }
        else
        {
            self.view.endEditing(true)
            AddTag()
        }
    }
    @IBAction func btnAddToCartAction(_ sender: Any)
    {
        if strQuantiy == ""
        {
            KSToastView.ks_showToast("Please select at least one quantity", duration: ToastDuration)
        }
        else
        {
            addToCartProduct()
        }
     
    }
    @IBAction func btnSearchAction(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnCartAction(_ sender: Any) {
        self.goToRootOfTab(index: 2)
    }
    @IBAction func btnBuyNowAction(_ sender: Any)
    {
        var isSelected = Bool()
        var strQunty = String()
        var arrQuntity = dict["arrquantiy"]
        for i in 0..<arrQuntity.count
        {
            var dictInner = arrQuntity[i]
            if dictInner["selected"].stringValue == "1"
            {
                isSelected = true
                strQunty = dictInner["quanity_name"].stringValue
                break
            }
        }
        if isSelected == false
        {
            KSToastView.ks_showToast("Please select at least one quantity", duration: ToastDuration)
        }
        else
        {
            if isComeFromCompare == true
            {
                addToCartProduct(strProductID: strProductId, strQuantiy: strQunty)
            }
            else{
               addToCartProduct(strProductID: dict["product_id"].stringValue, strQuantiy: strQunty)
            }
            
        }
    }
    
    @IBAction func btnShareAction(_ sender: Any) {
       // let strMsg = "Hey check out my app at: "
       // let link = "https://itunes.apple.com/us/app/hand2home/id1347042010?ls=1&mt=8"
    
        let activityVC = UIActivityViewController(activityItems: [self.dictProductDetail?["product_url"].stringValue ?? ""], applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)
    }
    
    //MARK:- Service
    
    func getProductDetailList()
    {
        
        let kRegiURL = "\(AddhirhamMainURL)productView.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param:[String:String] = [:]
            if isComeFromCompare == true
            {
                 param = ["hashkey" :HashKey,
                          "product_id" :strProductId,
                          "custid":dictLoggedUser["customerId"].stringValue]
            }
            else{
                param = ["hashkey" :HashKey,
                         "product_id" :dict["product_id"].stringValue,
                         "custid":dictLoggedUser["customerId"].stringValue]
            }
            
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)                        
                        var arrOfProductDetail:[JSON] = []
                        arrOfProductDetail = json["product"].arrayValue
                        self.dictProductDetail = arrOfProductDetail[0]
                        for i in 0..<strComapareID.count
                        {
                            if strComapareID[i] == self.dictProductDetail?["product_id"].stringValue ?? ""
                            {
                                self.dictProductDetail?["is_compare"] = "1"
                                break
                            }
                        }
                        self.setupProductDetail()
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func AddTag()
    {
        let kRegiURL = "\(AddhirhamMainURL)addTag.pnp"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param:[String:String] = [:]
            if isComeFromCompare == true
            {
                param = ["hashkey" :HashKey,
                         "product_id" :strProductId,
                         "custid":dictLoggedUser["customerId"].stringValue,
                         "add_tag":self.txtTag.text ?? "",
                         "store":"1"]
            }
            else{
                param = ["hashkey" :HashKey,
                         "product_id" :dict["product_id"].stringValue,
                         "custid":dictLoggedUser["customerId"].stringValue,
                         "add_tag":self.txtTag.text ?? "",
                         "store":"1"]
            }
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        KSToastView.ks_showToast("Tag add successfully", duration: ToastDuration)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func addToCartProduct()
    {        
        let kRegiURL = "\(AddhirhamMainURL)addtoCart.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param:[String:String] = [:]
            if isComeFromCompare == true{
                 param = ["hashkey" :HashKey,
                          "prodid" :strProductId,
                          "custid":dictLoggedUser["customerId"].stringValue,
                          "prodqty":strQuantiy,
                          "quoteId":getCartDetail("quote_id")]
            }
            else
            {
                 param = ["hashkey" :HashKey,
                          "prodid" :dict["product_id"].stringValue,
                          "custid":dictLoggedUser["customerId"].stringValue,
                          "prodqty":strQuantiy,
                          "quoteId":getCartDetail("quote_id")]
            }
           
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        guard let rowdata = try? json.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "cartDetail")
                        Defaults.synchronize()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                        strTotalCartItem = json["count"].stringValue
                        Defaults.setValue(strTotalCartItem, forKey: "CartItem")
                        Defaults.synchronize()
                        self.lblTotalCount.text = strTotalCartItem
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func addToWishList(strProductID:String,strCustId:String)
    {
        let kRegiURL = "\(AddhirhamMainURL)addToWhishlist.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "product_id" :strProductID,
                         "custid":strCustId] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ListRefresh"), object: nil)
                        KSToastView.ks_showToast(json["AddToWhishlist"]["sucess"].stringValue, duration: ToastDuration)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func addToCartProduct(strProductID:String,strQuantiy:String)
    {
        let kRegiURL = "\(AddhirhamMainURL)addtoCart.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "prodid" :strProductID,
                         "custid":dictLoggedUser["customerId"].stringValue,
                         "prodqty":strQuantiy,
                         "quoteId":getCartDetail("quote_id")] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        guard let rowdata = try? json.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "cartDetail")
                        Defaults.synchronize()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                        self.goToRootOfTab(index: 2)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}

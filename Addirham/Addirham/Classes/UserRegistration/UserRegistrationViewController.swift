//
//  UserRegistrationViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 03/11/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import NVActivityIndicatorView
import KSToastView

class UserRegistrationViewController: UIViewController,UITextFieldDelegate,NVActivityIndicatorViewable {
    
    //MARK:- Outlet ZOne
    
    @IBOutlet var txtEmailAddress: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtLastName: UITextField!
    @IBOutlet var btn_SideMenu: UIButton!
    
    
    //MARK:- ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [txtEmailAddress,txtPassword,txtFirstName,txtLastName].forEach({(textfield) in
            textfield?.delegate = self
        })
        btn_SideMenu.isHidden = true
       // btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RedirectToAddrees"), object: nil)
    }
    
   
    
    
    //MARK: - textfield delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == self.txtEmailAddress
        {
            return txtPassword.becomeFirstResponder()
        }
        else if textField == self.txtPassword
        {
            return txtFirstName.becomeFirstResponder()
        }
        else if textField == self.txtFirstName
        {
            return txtLastName.becomeFirstResponder()
        }
        else if textField == self.txtLastName
        {
            return txtLastName.resignFirstResponder()
        }
        return textField.resignFirstResponder()
    }
    
    //MARK:- ACtion Zone
   
    @IBAction func btnRegisterAction(_ sender: Any)
    {
        if ((self.txtEmailAddress.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
        {
            KSToastView.ks_showToast("Please enter email address", duration: ToastDuration)
        }
        else
        {
            let emailid: String = self.txtEmailAddress.text!
            let myStringMatchesRegEx: Bool = isValidEmail(emailAddressString: emailid)
            if myStringMatchesRegEx == false
            {
                KSToastView.ks_showToast("Please enter valid email address", duration: ToastDuration)
            }
            else if (self.txtPassword.text?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter password", duration: ToastDuration)
            }
            else if ((self.txtFirstName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter firstname", duration: ToastDuration)
            }
            else if ((self.txtLastName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter lastname", duration: ToastDuration)
            }
            else
            {
                self.view.endEditing(true)
                UserRegistration()
            }
        }
    }
    
    @IBAction func btnCheckoutAsGuestAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RedirectToAddrees"), object: nil)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Service
    
    func UserRegistration()
    {
        let kRegiURL = "\(AddhirhamMainURL)customer.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "email" :self.txtEmailAddress.text ?? "",
                         "password" :self.txtPassword.text ?? "",
                         "firstName" :self.txtFirstName.text ?? "",
                         "lastName" :self.txtLastName.text ?? "",
                         "type":"register"]
            
            print("parm \(param)")
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        // print(json)
                        self.dismiss(animated: true, completion: nil)
                        let data = json["message"]
                        KSToastView.ks_showToast(data["message"].stringValue, duration: ToastDuration)
                        
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                    
                }
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}

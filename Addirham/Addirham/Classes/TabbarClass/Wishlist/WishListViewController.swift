//
//  WishListViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 17/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView

class WishListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,NVActivityIndicatorViewable,UITextFieldDelegate
{
    //MARK:- Varible Declaration
    
    var arrOfWishList:[JSON] = []
    //var dictLoggedUser = JSON()
    var strNoRecordMessage = String()
    
     //MARK:- Outlet Zone
    @IBOutlet weak var lblTotalCount: UILabel!  
    
    @IBOutlet var txtSearch: CustomTextField!
    @IBOutlet var tblWishList: UITableView!
    @IBOutlet var lblUserNameList: UILabel!
    @IBOutlet var lblTotalItemWishList: UILabel!
    @IBOutlet var btn_SideMenu: UIButton!
 
    //MARK:- ViewLife cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
         btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        txtSearch.delegate = self
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tblWishList.addSubview(refreshControl)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        appDelagte.tabBarController.tabBar.isHidden = false
        dictLoggedUser = getLoggedUserDetail()
        print("dictUser \(dictLoggedUser)")
        print("customerId ",dictLoggedUser["customerId"].stringValue)
        self.lblUserNameList.text = "\(dictLoggedUser["firstname"].stringValue) \(dictLoggedUser["lastname"].stringValue) Wish List"
        if dictLoggedUser["customerId"].stringValue != ""
        {
            getUserWishList()
        }
        else
        {
            self.lblUserNameList.text = "User, Wish List"
            self.lblTotalItemWishList.text = "0 Item, in Your Wish List"
            strNoRecordMessage = "Please login first"
            self.arrOfWishList = []
            self.tblWishList.reloadData()
        }
        self.lblTotalCount.text = strTotalCartItem
    }
    
    //MARK:- Refresh Method
    @objc func refresh() {
        if dictLoggedUser["customerId"].stringValue != ""
        {
            getUserWishList()
        }
        refreshControl.endRefreshing()
    }

    
    //MARK:- Setup Quantity Array
    
    func SetupQuantityArray(qnt:Int) ->  [JSON]
    {
        var arrOfQuantity:[JSON] = []
        for i in 0..<qnt
        {
            var dict = [String:String]()
            dict["quanity_name"] = String(i+1)
            dict["selected"] = "0"
            arrOfQuantity.append(JSON(dict))
        }
        return arrOfQuantity
    }
    
    //MARK:- Textfield Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(obj, animated: true)
        return false
    }

    //MARK:- Action Zone
    
    @IBAction func btnAddCartAction(_ sender: UIButton)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        obj.dict = arrOfWishList[sender.tag]
      //  obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnCartAction(_ sender: UIButton)
    {
        goToRootOfTab(index: 2)
    }

    //MARK:- TableView delagte Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrOfWishList.count == 0
        {
            let lbl = UILabel()
            lbl.text = strNoRecordMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrOfWishList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:WishListCell = self.tblWishList.dequeueReusableCell(withIdentifier: "WishListCell") as! WishListCell
       
        let dict = arrOfWishList[indexPath.row]
        cell.lblProduName.text = dict["name"].stringValue
        cell.imgOfProduct.sd_setImage(with: dict["image"].url, placeholderImage:#imageLiteral(resourceName: "phone"))
        cell.lblProductPrice.text = "OMR \(dict["price"].stringValue)"
        cell.viewOfRatting.isHidden = true
        cell.viewOfRatting.setScore((dict["review"].floatValue/5), withAnimation: false)
        cell.lblStockStatus.text = dict["stoke"].stringValue
        cell.lblQuantity.text = "Quantity   : \(String(dict["qty"].intValue))"
        cell.btnAddCartOutlet.tag = indexPath.row
        if dict["qty"].intValue <= 0
        {
            cell.btnAddCartOutlet.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        obj.dict = arrOfWishList[indexPath.row]
       // obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:- Service
    
    func getUserWishList()
    {
        let kRegiURL = "\(AddhirhamMainURL)showWhishlist.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "custid" :dictLoggedUser["customerId"].stringValue] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.arrOfWishList = []
                        self.arrOfWishList = json["AddToWhishlist"]["sucess"].arrayValue
                        for i in 0..<self.arrOfWishList.count
                        {
                            var dict = self.arrOfWishList[i]
                            let arrQnt = self.SetupQuantityArray(qnt:dict["qty"].intValue)
                            dict["arrquantiy"] = JSON(arrQnt)
                            self.arrOfWishList[i] = dict
                        }
                        self.tblWishList.reloadData()
                        if json["item_count"].intValue > 1
                        {
                            self.lblTotalItemWishList.text = "\(json["item_count"].stringValue) Items, in Your Wish List"
                        }
                        else{
                            self.lblTotalItemWishList.text = "\(json["item_count"].stringValue) Item, in Your Wish List"
                        }                       
                        
                    }
                    else
                    {
                        self.lblTotalItemWishList.text = "0 Item, in Your Wish List"
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblWishList.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}

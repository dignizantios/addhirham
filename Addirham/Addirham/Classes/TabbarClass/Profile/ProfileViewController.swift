//
//  ProfileViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 17/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import NVActivityIndicatorView
import KSToastView

class ProfileViewController: UIViewController,UITextFieldDelegate,NVActivityIndicatorViewable,UITabBarControllerDelegate
{
    
    //MARK:- Variable Declarationclass
    
    //var dictLoggedUser = JSON()
    var arrOfCountry:[JSON] = []
    var arrOfRegion:[JSON] = []
    var strCountryId = String()
    var strRegionId = String()
    var dictUserInfo:JSON!
    var dictUserAddressInfo:JSON!
    var StateCode = String()
    var picker = UIImagePickerController()
    var isComeFromPicker = Bool()
     //MARK:- Outlet Zone
    
    @IBOutlet weak var heightOfChangeBtn: NSLayoutConstraint!
    @IBOutlet weak var heightOfProfilebtn: NSLayoutConstraint!
    @IBOutlet var btn_SideMenu: UIButton!
    @IBOutlet var lblMainTitle: UILabel!
    @IBOutlet var lblUserEmail: UILabel!
    @IBOutlet var lblUserFullName: UILabel!
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtLastName: UITextField!
    @IBOutlet var txtEMailAddress: UITextField!
    @IBOutlet var txtCurrentPassword: UITextField!
    @IBOutlet var txtNewPassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    @IBOutlet var txtMobileNo: UITextField!
    @IBOutlet var txtAddressLine: UITextField!
    @IBOutlet var txtStateName: UITextField!
    @IBOutlet var txtCityName: UITextField!
    @IBOutlet var txtZipcode: UITextField!
    @IBOutlet var txtCountryName: UITextField!
    @IBOutlet weak var btnProfileOutlet: UIButton!
    @IBOutlet weak var imgProfile: CustomImageView!
    
    //MARK:- ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker.delegate = self
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.height/2
        self.imgProfile.clipsToBounds = true
        
         btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool)
    {
        if isComeFromPicker == false
        {
            [txtFirstName,txtLastName,txtEMailAddress,txtCurrentPassword,txtNewPassword,txtConfirmPassword,txtMobileNo,txtAddressLine,txtStateName,txtCityName,txtZipcode,txtCountryName].forEach({(textfield) in
                textfield?.delegate = self
                textfield?.isUserInteractionEnabled = false
                
            })
            btnProfileOutlet.isUserInteractionEnabled = false
            dictLoggedUser = getLoggedUserDetail()
            if  let str = Defaults.value(forKey: "userProfilePic") as? String
            {
                self.imgProfile.sd_setImage(with: URL(string: str), placeholderImage: #imageLiteral(resourceName: "user_placeholder_register_screen"), options: .lowPriority, completed: nil)
            }
            else
            {
                self.imgProfile.image = #imageLiteral(resourceName: "user_placeholder_register_screen")
            }
            getProfileDetail()
            self.heightOfChangeBtn.constant = 0
            self.heightOfProfilebtn.constant = 0
        }
        
       /* if getUserAddressDetail("address_line") != ""
        {
            self.txtAddressLine.text = getUserAddressDetail("address_line")
            self.txtCityName.text = getUserAddressDetail("city")
            self.txtZipcode.text = getUserAddressDetail("zipcode")
            self.txtMobileNo.text = getUserAddressDetail("telephone")
        }*/
       
    }
    
    
    //MARK:- Action Zone
    
    @IBAction func btnSubmitAction(_ sender: UIButton)
    {
        if sender.tag == 1
        {
            if ((self.txtFirstName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter first name", duration: ToastDuration)
            }
            else if ((self.txtLastName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter last name", duration: ToastDuration)
            }
            else if (self.txtMobileNo.text?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter mobile number", duration: ToastDuration)
            }
            else if (self.txtMobileNo.text?.characters.count)! < 8
            {
                KSToastView.ks_showToast("Please enter mobile number at least  8 character long", duration: ToastDuration)
            }
            else if ((self.txtAddressLine.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter address", duration: ToastDuration)
            }
            else if ((self.txtCityName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter city", duration: ToastDuration)
            }
            else if ((self.txtStateName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter state", duration: ToastDuration)
            }
            else if ((self.txtZipcode.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter zip code", duration: ToastDuration)
            }
            else if ((self.txtCountryName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                KSToastView.ks_showToast("Please select country", duration: ToastDuration)
            }
            else
            {
                self.view.endEditing(true)
                if getUserAddressDetail("customer_address_id") != ""
                {
                     userAddressRegistrationUpdate()
                }
                else
                {
                    userAddressRegistration()
                }
               
            }
        }
        else
        {
            if (self.txtCurrentPassword.text?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter current password", duration: ToastDuration)
            }
            else if (self.txtNewPassword.text?.isEmpty)!
            {
                KSToastView.ks_showToast("Please enter new password", duration: ToastDuration)
            }
            else if !(self.txtNewPassword.text == "")
            {
                if (self.txtNewPassword.text?.count)! < 6
                {
                    KSToastView.ks_showToast("Password must be 6 character long", duration: ToastDuration)
                }
                else if self.txtConfirmPassword.text == ""
                {
                    KSToastView.ks_showToast("Please enter confirm password", duration: ToastDuration)
                }
                else
                {
                    if(!(self.txtNewPassword.text == self.txtConfirmPassword.text))
                    {
                        KSToastView.ks_showToast("Password and confirm password must be same", duration: ToastDuration)
                    }
                    else
                    {
                        self.view.endEditing(true)
                        changePassword()
                    }
                }
            }
            
        }
    }
   
    @IBAction func btnEditProfile(_ sender: UIButton)
    {
        if sender.tag == 1
        {
            [txtFirstName,txtLastName,txtMobileNo,txtAddressLine,txtStateName,txtCityName,txtZipcode,txtCountryName].forEach({(textfield) in
                
                textfield?.isUserInteractionEnabled = true
                
            })
            [txtCurrentPassword,txtNewPassword,txtConfirmPassword].forEach({(textfield) in
                textfield?.text = ""
                textfield?.isUserInteractionEnabled = false
            })
            self.heightOfChangeBtn.constant = 0
            self.heightOfProfilebtn.constant = 50
            self.btnProfileOutlet.isUserInteractionEnabled = true
        }
        else
        {
            [txtFirstName,txtLastName,txtMobileNo,txtAddressLine,txtStateName,txtCityName,txtZipcode,txtCountryName].forEach({(textfield) in
                self.txtFirstName.text = self.dictUserInfo["firstname"].stringValue
                self.txtLastName.text = self.dictUserInfo["lastname"].stringValue
                self.txtEMailAddress.text = self.dictUserInfo["email"].stringValue
                
                self.txtAddressLine.text = self.dictUserAddressInfo["address_line"].stringValue
                self.txtCityName.text = self.dictUserAddressInfo["city"].stringValue
                self.txtZipcode.text = self.dictUserAddressInfo["zipcode"].stringValue
                self.txtMobileNo.text = self.dictUserAddressInfo["telephone"].stringValue
                self.txtCountryName.text = self.dictUserAddressInfo["country"].stringValue
                self.txtStateName.text = self.dictUserAddressInfo["state"].stringValue
                textfield?.isUserInteractionEnabled = false
            })
            [txtCurrentPassword,txtNewPassword,txtConfirmPassword].forEach({(textfield) in
                
                textfield?.isUserInteractionEnabled = true
            })
            self.heightOfChangeBtn.constant = 50
            self.heightOfProfilebtn.constant = 0
            self.btnProfileOutlet.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func btnSelectProfileAction(_ sender: Any) {
        ShowChooseImageOptions(picker : picker)
    }
    //MARK: - textfield delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == self.txtFirstName
        {
            return txtLastName.becomeFirstResponder()
        }
        else if textField == self.txtLastName
        {
            return txtMobileNo.becomeFirstResponder()
        }
        else if textField == self.txtMobileNo
        {
            return txtAddressLine.becomeFirstResponder()
        }
        else if textField == self.txtAddressLine
        {
            return txtStateName.becomeFirstResponder()
        }
        else if textField == self.txtCurrentPassword
        {
            return txtNewPassword.becomeFirstResponder()
        }
        else if textField == self.txtNewPassword
        {
            return txtConfirmPassword.becomeFirstResponder()
        }
        else if textField == self.txtConfirmPassword
        {
            return txtConfirmPassword.resignFirstResponder()
        }
        else{
            return textField.resignFirstResponder()
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == txtCountryName
        {
            self.view.endEditing(true)
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommomPickerVC") as! CommomPickerVC
            obj.PickerDelegate = self
            obj.arrPicker = arrOfCountry
            obj.pickerType = 0
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true, completion: nil)
            return false
        }
        else if textField == txtStateName
        {
            self.view.endEditing(true)
            if self.txtCountryName.text == ""
            {
                KSToastView.ks_showToast("Please select country", duration: ToastDuration)
            }
            else if arrOfRegion.count == 0
            {
                getRegionList()
            }
            else
            {
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommomPickerVC") as! CommomPickerVC
                obj.PickerDelegate = self
                obj.arrPicker = arrOfRegion
                obj.pickerType = 1
                obj.modalPresentationStyle = .overCurrentContext
                obj.modalTransitionStyle = .crossDissolve
                self.present(obj, animated: true, completion: nil)
            }
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobileNo
        {
            let limitLength = 10
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= limitLength // Bool
        }
        else if textField == txtZipcode
        {
            let limitLength = 6
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= limitLength
        }
        return true
    }
    
    //MARK:- Service
    
    func userAddressRegistrationUpdate()
    {
        let kRegiURL = "\(AddhirhamMainURL)cutomerAddressUpdate.php"
        let strUserId:String = dictLoggedUser["customerId"].stringValue

        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "customer_address_id" : getUserAddressDetail("customer_address_id"),
                         "cust_id:" :strUserId,
                         "firstname" :self.txtFirstName.text ?? "",
                         "lastname" :self.txtLastName.text ?? "",
                         "street" :self.txtAddressLine.text ?? "",
                         "city" :self.txtCityName.text ?? "",
                         "region" :StateCode,
                         "region_id":strRegionId,
                         //"region" :strRegionId,
                       //  "region_id":StateCode, // chnaged
                         "postcode" :self.txtZipcode.text ?? "",
                         "country_id" :strCountryId,
                         "telephone" :self.txtMobileNo.text ?? "",
                         "defaultbill" :"true",
                         "defaultship" :"true"]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.dictUserAddressInfo = json["customerAddressUpdate"]["sucess"]
                        print("dictAddress \(self.dictUserAddressInfo)")
                        guard let rowdata = try? self.dictUserAddressInfo.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userAddressDetail")
                        Defaults.synchronize()
                        [self.txtFirstName,self.txtLastName,self.txtMobileNo,self.txtAddressLine,self.txtStateName,self.txtCityName,self.txtZipcode,self.txtCountryName].forEach({(textfield) in
                            self.txtFirstName.text = self.dictUserInfo["firstname"].stringValue
                            self.txtLastName.text = self.dictUserInfo["lastname"].stringValue
                            self.txtEMailAddress.text = self.dictUserInfo["email"].stringValue
                            
                            self.txtAddressLine.text = self.dictUserAddressInfo["address_line"].stringValue
                            self.txtCityName.text = self.dictUserAddressInfo["city"].stringValue
                            self.txtZipcode.text = self.dictUserAddressInfo["zipcode"].stringValue
                            self.txtMobileNo.text = self.dictUserAddressInfo["telephone"].stringValue
                            self.txtCountryName.text = self.dictUserAddressInfo["country"].stringValue
                            self.txtStateName.text = self.dictUserAddressInfo["state"].stringValue
                            textfield?.isUserInteractionEnabled = false
                        })
                        self.heightOfProfilebtn.constant = 0
                        self.btnProfileOutlet.isUserInteractionEnabled = false
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func userAddressRegistration()
    {
        let kRegiURL = "\(AddhirhamMainURL)cutomerAddressCreate.php"
        let strUserId:String = dictLoggedUser["customerId"].stringValue
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "custid" :strUserId,
                         "firstname" :self.txtFirstName.text ?? "",
                         "lastname" :self.txtLastName.text ?? "",
                         "street" :self.txtAddressLine.text ?? "",
                         "city" :self.txtCityName.text ?? "",
                         "region" :StateCode,
                         "postcode" :self.txtZipcode.text ?? "",
                         "country_id" :strCountryId,
                         "telephone" :self.txtMobileNo.text ?? "",
                         "region_id":strRegionId,
                       //  "region" :strRegionId,
                      //   "region_id":StateCode, // chnaged
                         "defaultbill" :"true",
                         "defaultship" :"true"]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.dictUserAddressInfo = json["customerAddressCreate"]["sucess"]
                        print("dictAddress \(self.dictUserAddressInfo)")
                        guard let rowdata = try? self.dictUserAddressInfo.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userAddressDetail")
                        Defaults.synchronize()
                        
                        [self.txtFirstName,self.txtLastName,self.txtMobileNo,self.txtAddressLine,self.txtStateName,self.txtCityName,self.txtZipcode,self.txtCountryName].forEach({(textfield) in
                            self.txtFirstName.text = self.dictUserInfo["firstname"].stringValue
                            self.txtLastName.text = self.dictUserInfo["lastname"].stringValue
                            self.txtEMailAddress.text = self.dictUserInfo["email"].stringValue
                            
                            self.txtAddressLine.text = self.dictUserAddressInfo["address_line"].stringValue
                            self.txtCityName.text = self.dictUserAddressInfo["city"].stringValue
                            self.txtZipcode.text = self.dictUserAddressInfo["zipcode"].stringValue
                            self.txtMobileNo.text = self.dictUserAddressInfo["telephone"].stringValue
                            self.txtCountryName.text = self.dictUserAddressInfo["country"].stringValue
                            self.txtStateName.text = self.dictUserAddressInfo["state"].stringValue
                            textfield?.isUserInteractionEnabled = false
                        })
                        self.heightOfProfilebtn.constant = 0
                        self.btnProfileOutlet.isUserInteractionEnabled = false
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func getProfileDetail()
    {        
        let kRegiURL = "\(AddhirhamMainURL)dasboard.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "email" :dictLoggedUser["email"].stringValue,
                         "id":dictLoggedUser["customerId"].stringValue] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        let dict = json["dashborad"]
                        self.dictUserInfo = dict["info"]
                        self.dictUserAddressInfo = dict["billing"]
                        self.txtFirstName.text = self.dictUserInfo["firstname"].stringValue
                        self.txtLastName.text = self.dictUserInfo["lastname"].stringValue
                        self.txtEMailAddress.text = self.dictUserInfo["email"].stringValue
                        self.lblMainTitle.text = "Hello, \(self.dictUserInfo["firstname"].stringValue)!"
                        self.lblUserFullName.text = "\(self.dictUserInfo["firstname"].stringValue) \(self.dictUserInfo["lastname"].stringValue)"
                        self.lblUserEmail.text = self.dictUserInfo["email"].stringValue
                        self.txtAddressLine.text = self.dictUserAddressInfo["address_line"].stringValue
                        self.txtCityName.text = self.dictUserAddressInfo["city"].stringValue
                        self.txtZipcode.text = self.dictUserAddressInfo["zipcode"].stringValue
                        self.txtMobileNo.text = self.dictUserAddressInfo["telephone"].stringValue
                        guard let rowAdddata = try? self.dictUserAddressInfo.rawData() else {return}
                        Defaults.setValue(rowAdddata, forKey: "userAddressDetail")
                        Defaults.synchronize()
                        self.getCountryList()
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

    func changePassword()
    {
        let kRegiURL = "\(AddhirhamMainURL)changePassword.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "username" :"\(self.txtFirstName.text ?? "") \(self.txtLastName.text ?? "")",
                         "cust_id":dictLoggedUser["customerId"].stringValue,
                         "old_pass":self.txtCurrentPassword.text ?? "",
                         "new_pass":self.txtNewPassword.text ?? ""] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                         KSToastView.ks_showToast(json["changePassword"]["sucess"].stringValue, duration: ToastDuration)
                        [self.txtCurrentPassword,self.txtNewPassword,self.txtConfirmPassword].forEach({(textfield) in
                            textfield?.text = ""
                            textfield?.isUserInteractionEnabled = false
                        })
                        self.heightOfChangeBtn.constant = 0
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func getCountryList()
    {
        let kRegiURL = "\(AddhirhamMainURL)countrylist.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.arrOfCountry = []
                        self.arrOfCountry = json["countryid"].arrayValue
                        if self.getUserAddressDetail("country") != ""
                        {
                            for i in 0..<self.arrOfCountry.count{
                                let dict = self.arrOfCountry[i]
                                if dict["countryID"].stringValue == self.getUserAddressDetail("country_id")
                                {
                                    self.strCountryId = dict["countryID"].stringValue
                                    self.txtCountryName.text = dict["countryName"].stringValue
                                    self.getRegionList()
                                    break
                                }
                            }
                            
                        }
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func getRegionList()
    {
        let kRegiURL = "\(AddhirhamMainURL)regionList.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "country_code":strCountryId] as [String : Any]
            
            print("parm \(param)")
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.txtStateName.text = ""
                        self.strRegionId = ""
                        self.arrOfRegion = []
                        self.arrOfRegion = json["region_code"].arrayValue
                        if self.getUserAddressDetail("state") != ""
                        {
                            for i in 0..<self.arrOfRegion.count{
                                let dict = self.arrOfRegion[i]
                                if dict["state_id"].stringValue == self.getUserAddressDetail("state_id")
                                {
                                    self.strRegionId = dict["state_id"].stringValue
                                    self.txtStateName.text = dict["name"].stringValue
                                    self.StateCode = dict["state_code"].stringValue
                                    break
                                }
                            }
                            
                        }
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func userUploadProfilePic()
    {
        let kRegiURL = "\(AddhirhamMainURL)uploaddp.php"
        print("URL:- \(kRegiURL)")
        
        var imgData = Data()
        if let img = self.imgProfile.image
        {
            imgData = UIImageJPEGRepresentation(img, 0.7)!
        }
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
           let strBase64 = imgData.base64EncodedString(options: .lineLength64Characters)
          // print(strBase64)
           let param = ["hashkey":HashKey,
                        "cust_id":dictLoggedUser["customerId"].stringValue,
                        "img_name":"\(dictLoggedUser["firstname"].stringValue).jpg",
                        "img_string":strBase64]
            
            print("param \(param)")
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.imgProfile.sd_setImage(with: json["Upload profile"]["sucess"].url, placeholderImage: #imageLiteral(resourceName: "user_placeholder_register_screen"), options: .lowPriority, completed: nil)
                        
                        Defaults.setValue(json["Upload profile"]["sucess"].stringValue, forKey: "userProfilePic")
                        Defaults.synchronize()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

}

extension ProfileViewController : CommonPickerDelegate
{
    func setName(strCode:String, strName:String,type: Int,strStateCode:String)
    {
        if type == 0
        {
            strCountryId = strCode
            self.txtCountryName.text = strName
            self.txtStateName.text = ""
            getRegionList()
        }
        else
        {            
            StateCode = strStateCode
            strRegionId = strCode
            self.txtStateName.text = strName
        }
    }
    
}

//MARK: - ImagePicker Methods

extension ProfileViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let selectedImage = info[UIImagePickerControllerEditedImage]
        imgProfile.image = selectedImage as? UIImage
        isComeFromPicker = true
        userUploadProfilePic()
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        isComeFromPicker = true
        self.dismiss(animated: true, completion: nil)
    }
}

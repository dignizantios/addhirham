//
//  MyCartViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 17/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView

class MyCartViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,NVActivityIndicatorViewable
{
    
    //MARK:- Varible Declaration
    
    var arrOfCartList:[JSON] = []
 //   var dictLoggedUser = JSON()
    var strQuoteId = String()
    var strNoRecordMessage = String()
    var isAllProductAvailabel = Bool()
    
    //MARK:- Outlet Zone
    
    @IBOutlet var tblCart: UITableView!
    @IBOutlet var btnTotalPrice: UIButton!
    @IBOutlet var btnContinueOutlet: UIButton!
    @IBOutlet var btn_SideMenu: UIButton!
    @IBOutlet weak var lblTotalCount: UILabel!
    
    //MARK:- ViewLifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tblCart.addSubview(refreshControl)
       // self.btnContinueOutlet.roundButtonCorners([.bottomRight,.topRight], radius: 5)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
         NotificationCenter.default.addObserver(self, selector: #selector(self.redirectToShippingAddress), name: NSNotification.Name(rawValue: "RedirectToShipAddrees"), object: nil)
        
        self.lblTotalCount.text = strTotalCartItem
        appDelagte.tabBarController.tabBar.isHidden = false
        print("dictUser \(getLoggedUserDetail())")
        dictLoggedUser = getLoggedUserDetail()
        print("dictUser \(dictLoggedUser)")
        print("customerId ",dictLoggedUser["customerId"].stringValue)
       // self.view.addSubview(self.tblCart)
       
        /*if dictLoggedUser["customerId"].stringValue != ""
        {
            getUserCartList()
        }
        else*/ if getCartDetail("quote_id") != "0"
        {
            getGuestUserCartList()
        }
        else
        {
            self.btnTotalPrice.setTitle("0.0", for: .normal)
            strNoRecordMessage = "No record found"
            self.arrOfCartList = []
            self.tblCart.reloadData()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
      
    }
    
    //MARK:- Private Method
    
    @objc func redirectToShippingAddress()
    {
        startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
        self.perform(#selector(redirect), with: nil, afterDelay: 1)
    }
    
    @objc func redirect() {
        stopAnimating()
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        obj.isComeFromGuestUser = true
        self.navigationController?.pushViewController(obj, animated: true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RedirectToShipAddrees"), object: nil)
    }
    
    //MARK:- Refresh Method
    @objc func refresh() {
        if getCartDetail("quote_id") != "0"
        {
            getGuestUserCartList()
        }
        else
        {
            self.btnTotalPrice.setTitle("0.0", for: .normal)
            strNoRecordMessage = "No record found"
            self.arrOfCartList = []
            self.tblCart.reloadData()
        }
        refreshControl.endRefreshing()
    }

    
    
    
    //MARK:- TableView delagte Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrOfCartList.count == 0
        {
            let lbl = UILabel()
            lbl.text = strNoRecordMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrOfCartList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CartCell = self.tblCart.dequeueReusableCell(withIdentifier: "CartCell") as! CartCell
       // cell.viewOfDelEdit.roundCorners([.topRight,.bottomRight], radius: 5)
        let dict = arrOfCartList[indexPath.row]
        cell.lblProductName.text = dict["name"].stringValue
       // cell.lblSellerName.text = "Seller : \(dict["seller_name"].stringValue)"
      //  cell.btnAddOutlet.isHidden =
        /* let strImage = dict["image"].stringValue
         let urlImage:URL = URL(string: strImage)!*/
        
        cell.imgProduct.sd_setImage(with: dict["image"].url, placeholderImage:#imageLiteral(resourceName: "phone"))
     //   cell.lblOldPrice.text = "OMR \(dict["special_price"].stringValue)"
      //  cell.lblNewPrice.text = "OMR \(dict["price"].stringValue)"
        cell.lblTotalQuantity.text = dict["qty"].stringValue
        cell.btnAddOutlet.tag = indexPath.row
        cell.btnMinusOutlet.tag = indexPath.row
        cell.btnDeleteOutlet.tag = indexPath.row
        cell.btnEditOutlet.tag = indexPath.row
        cell.btnEditOutlet.setImage(#imageLiteral(resourceName: "true_mark_white"), for: .normal)
        cell.btnEditOutlet.setImage(#imageLiteral(resourceName: "ic_item_item_card_screen"), for: .selected)
        if dict["is_edit"].stringValue == "0"
        {
            cell.btnAddOutlet.isHidden = true
            cell.btnMinusOutlet.isHidden = true
            cell.btnEditOutlet.isSelected = true
           // cell.trailingOfMainView.constant = 9
        }
        else
        {
            cell.btnEditOutlet.isSelected = false
            //cell.trailingOfMainView.constant = -89
            cell.btnAddOutlet.isHidden = false
            cell.btnMinusOutlet.isHidden = false
        }
        
        if dict["stock_status"].stringValue == "1"
        {
            cell.lblOutOfStock.text = "In stock"
            cell.viewOfEdit.isHidden = false
        }
        else{
            cell.lblOutOfStock.text = "Out of stock"
            cell.viewOfEdit.isHidden = true
        }
        if dict["special_price"].intValue > 0
        {
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "OMR \(dict["price"].stringValue)")
            attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
         //   cell.lblOldPrice.attributedText = attributeString
         //   cell.lblNewPrice.text  = "OMR \(dict["special_price"].stringValue)"
            cell.lblOldPrice.attributedText = attributeString
            cell.lblNewPrice.text  = "OMR \(dict["special_price"].stringValue)"
        }
        else{
            cell.lblNewPrice.text  = "OMR \(dict["price"].stringValue)"
            cell.lblOldPrice.text = ""
            cell.lblOldPrice.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnContinueAction(_ sender: Any)
    {
        if arrOfCartList.count == 0
        {
            KSToastView.ks_showToast("Please add at least one item in cart", duration: ToastDuration)
        }
        else
        {
            if isAllProductAvailabel == false{
                KSToastView.ks_showToast("Some products are not available, so please remove from your cart and then after continue", duration: ToastDuration)
                return
            }
            if dictLoggedUser["customerId"].stringValue == ""
            {
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "ShippingAddressViewController") as! ShippingAddressViewController
              // obj.modalPresentationStyle = .overCurrentContext
              //  obj.modalTransitionStyle = .crossDissolve
                self.present(obj, animated: true, completion: nil)
            }
            else
            {
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RedirectToShipAddrees"), object: nil)
                let method = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
                self.navigationController?.pushViewController(method, animated: true)
            }
        }
    }
    @IBAction func btnAddAction(_ sender: UIButton)
    {
        var dict = arrOfCartList[sender.tag]
        let add = dict["qty"].intValue + 1
        dict["qty"] = JSON(String(add))
        print("dict \(dict)")
        arrOfCartList[sender.tag] = dict
         print("arrOfCartList \(arrOfCartList)")
        //self.tblCart.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
        self.tblCart.reloadData()
    }
    @IBAction func btnMinusAction(_ sender: UIButton)
    {
        var dict = arrOfCartList[sender.tag]
        if dict["qty"].intValue <= 1
        {
            return
        }
        let minus = dict["qty"].intValue - 1
        dict["qty"] = JSON(String(minus))
        print("dict \(dict)")
        arrOfCartList[sender.tag] = dict
        print("arrOfCartList \(arrOfCartList)")
        //self.tblCart.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
        self.tblCart.reloadData()
    }
    
    @IBAction func btnFilterAction(_ sender: UIButton)
    {
        let obj:FilterViewController = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        obj.modalPresentationStyle = .overCurrentContext        
        self.present(obj, animated: false, completion: nil)
    }
    
    @IBAction func btnDeleteAction(_ sender: UIButton)
    {
        let alertController = UIAlertController(title: AppName, message: "Are you sure want to delete item from cart?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            self.isAllProductAvailabel = false
            let dict = self.arrOfCartList[sender.tag]
            self.deleteCartProduct(strCartId: dict["product_id"].stringValue)
            
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
       
    }
    @IBAction func btnEditAction(_ sender: UIButton)
    {
        var dict = arrOfCartList[sender.tag]
        if dict["is_edit"].stringValue == "0"
        {
            dict["is_edit"] = "1"
            arrOfCartList[sender.tag] = dict
            self.tblCart.reloadData()
        }
        else{
             self.isAllProductAvailabel = false
            dict["is_edit"] = "0"
            arrOfCartList[sender.tag] = dict
            editCartProduct(strCartId: dict["product_id"].stringValue, strQty: dict["qty"].stringValue)
        }
        
    }
    
    //MARK:- Service
    
    func getUserCartList()
    {
        let kRegiURL = "\(AddhirhamMainURL)customerCartInfo.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "customer_id" :dictLoggedUser["customerId"].stringValue] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        
                        self.arrOfCartList = []
                        self.arrOfCartList = json["cart_info"]["product"].arrayValue
                        self.tblCart.reloadData()
                        self.btnTotalPrice.setTitle("\(json["cart_info"]["global_currency_code"].stringValue) \(json["cart_info"]["grand_total"].stringValue)", for: .normal)
                    }
                    else
                    {
                        self.arrOfCartList = []
                        self.btnTotalPrice.setTitle("0.0", for: .normal)
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblCart.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func getGuestUserCartList()
    {
        let kRegiURL = "\(AddhirhamMainURL)guestCartInfo.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "quote_id":getCartDetail("quote_id")] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                refreshControl.endRefreshing()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        strTotalCartItem = json["cart_info"]["items_count"].stringValue
                        Defaults.setValue(strTotalCartItem, forKey: "CartItem")
                        Defaults.synchronize()
                        self.lblTotalCount.text = strTotalCartItem
                        self.arrOfCartList = []
                        self.arrOfCartList = json["cart_info"]["product"].arrayValue
                        for i in 0..<self.arrOfCartList.count
                        {
                            var dict = self.arrOfCartList[i]
                            dict["is_edit"] = "0"
                            self.arrOfCartList[i] = dict
                        }
                        if self.arrOfCartList.count == 0
                        {
                            self.strNoRecordMessage = "Cart is empty"
                        }
                        var flg = Bool()
                        for i in 0..<self.arrOfCartList.count
                        {
                            var dict = self.arrOfCartList[i]
                            if dict["stock_status"].stringValue == "0"
                            {
                                flg = true
                            }
                        }
                        if flg == true
                        {
                           self.isAllProductAvailabel = false
                        }
                        else{
                            self.isAllProductAvailabel = true
                        }
                        self.tblCart.reloadData()
                        self.btnTotalPrice.setTitle("\(json["cart_info"]["global_currency_code"].stringValue) \(json["cart_info"]["grand_total"].stringValue)", for: .normal)
                    }
                    else
                    {
                        self.arrOfCartList = []
                        self.btnTotalPrice.setTitle("0.000", for: .normal)
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblCart.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func deleteCartProduct(strCartId:String)
    {
        let kRegiURL = "\(AddhirhamMainURL)deleteCartProduct.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "quote_id":getCartDetail("quote_id"),
                         "product_id":strCartId] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        strTotalCartItem = json["cart_info"]["items_count"].stringValue
                        Defaults.setValue(strTotalCartItem, forKey: "CartItem")
                        Defaults.synchronize()
                        self.arrOfCartList = []
                        self.arrOfCartList = json["cart_info"]["product"].arrayValue
                        for i in 0..<self.arrOfCartList.count
                        {
                            var dict = self.arrOfCartList[i]
                            dict["is_edit"] = "0"
                            self.arrOfCartList[i] = dict
                        }
                        if self.arrOfCartList.count == 0
                        {
                            self.strNoRecordMessage = "Cart is empty"
                        }
                        var flg = Bool()
                        for i in 0..<self.arrOfCartList.count
                        {
                            var dict = self.arrOfCartList[i]
                            if dict["stock_status"].stringValue == "0"
                            {
                                flg = true
                            }
                        }
                        if flg == true
                        {
                            self.isAllProductAvailabel = false
                        }
                        else{
                            self.isAllProductAvailabel = true
                        }
                        self.tblCart.reloadData()
                        self.btnTotalPrice.setTitle("\(json["cart_info"]["global_currency_code"].stringValue) \(json["cart_info"]["grand_total"].stringValue)", for: .normal)
                    }
                    else
                    {
                        self.btnTotalPrice.setTitle("0.0", for: .normal)
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblCart.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func editCartProduct(strCartId:String,strQty:String)
    {
        let kRegiURL = "\(AddhirhamMainURL)updateCartProduct.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "quote_id":getCartDetail("quote_id"),
                         "product_id":strCartId,
                         "qty":strQty] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        strTotalCartItem = json["cart_info"]["items_count"].stringValue
                        Defaults.setValue(strTotalCartItem, forKey: "CartItem")
                        Defaults.synchronize()
                        self.arrOfCartList = []
                        self.arrOfCartList = json["cart_info"]["product"].arrayValue
                        for i in 0..<self.arrOfCartList.count
                        {
                            var dict = self.arrOfCartList[i]
                            dict["is_edit"] = "0"
                            self.arrOfCartList[i] = dict
                        }
                        var flg = Bool()
                        for i in 0..<self.arrOfCartList.count
                        {
                            var dict = self.arrOfCartList[i]
                            if dict["stock_status"].stringValue == "0"
                            {
                                flg = true
                            }
                        }
                        if flg == true
                        {
                            self.isAllProductAvailabel = false
                        }
                        else{
                            self.isAllProductAvailabel = true
                        }
                        self.tblCart.reloadData()
                        self.btnTotalPrice.setTitle("\(json["cart_info"]["global_currency_code"].stringValue) \(json["cart_info"]["grand_total"].stringValue)", for: .normal)
                    }
                    else
                    {
                        self.btnTotalPrice.setTitle("0.0", for: .normal)
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblCart.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}


//
//  TrackOrderViewController.swift
//  Addirham
//
//  Created by Haresh on 11/13/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView

class TrackOrderViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable
{
    //MARK:- Variable Declaration
    
    var arrOfTrackOrder:[JSON] = []
    var dict:JSON!
    var strStatus = Int()
    var strProductId = String()
    var strNoRecordMessage = String()
    
    //MARK:- Outlet Zone
    @IBOutlet weak var imgOfProduct: CustomImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var tblOfTrackOrder: UITableView!
    @IBOutlet weak var lblTotalCount: UILabel!
    @IBOutlet weak var btnCancelOutlet: UIButton!
    
    //MARK:- ViewLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        strStatus = 0
        getTrackOrder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblTotalCount.text = strTotalCartItem
        appDelagte.tabBarController.tabBar.isHidden = true
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCartAction(_ sender: Any)
    {
        goToRootOfTab(index: 2)
    }
    @IBAction func btnCancelOrderAction(_ sender: Any)
    {
        if strStatus > 1
        {
            KSToastView.ks_showToast("You can not cancel your order", duration: ToastDuration)
        }
        else
        {
            cancelOrder()
        }
    }
    //MARK:- Table view Life Cycle
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrOfTrackOrder.count == 0
        {
            let lbl = UILabel()
            lbl.text = strNoRecordMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrOfTrackOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell:TrackHeaderCell = self.tblOfTrackOrder.dequeueReusableCell(withIdentifier: "TrackHeaderCell") as! TrackHeaderCell
            let dict = arrOfTrackOrder[indexPath.row]
            cell.imgOfHeader.sd_setImage(with:  dict["image"].url, placeholderImage:#imageLiteral(resourceName: "phone"))
            cell.lblProductName.text =  dict["name"].stringValue
            cell.lblOrderNo.text = "Order #\( dict["order#"].stringValue)"
            return cell
        }
        let cell:TrackOrderCell = self.tblOfTrackOrder.dequeueReusableCell(withIdentifier: "TrackOrderCell") as! TrackOrderCell
        let dict = arrOfTrackOrder[indexPath.row]
        cell.btnProgressName.setTitle(dict["name"].stringValue, for: .normal)
        cell.btnProgressName.setTitle(dict["name"].stringValue, for: .selected)
        cell.lblDescription.text = dict["description"].stringValue
       if indexPath.row == 1
        {
            cell.btnUpparSepOutlet.isHidden = true
        }
        else
        {
            cell.btnUpparSepOutlet.isHidden = false
        }
        if indexPath.row == (arrOfTrackOrder.count - 1)
        {
            cell.btnOfBootomLineOutlet.backgroundColor = UIColor.clear
        }
        else
        {
            if indexPath.row-1 < dict["status"].intValue
            {
                cell.btnOfBootomLineOutlet.backgroundColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1.0)
                cell.btnUpparSepOutlet.backgroundColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1.0)
            }
            else
            {
                cell.btnOfBootomLineOutlet.backgroundColor = UIColor.init(red: 50/255, green: 50/255, blue: 50/255, alpha: 1.0)
                cell.btnUpparSepOutlet.backgroundColor = UIColor.init(red: 50/255, green: 50/255, blue: 50/255, alpha: 1.0)
            }
            
        }
        if indexPath.row-1 <= dict["status"].intValue
        {
            cell.btnImgOfProgress.setImage(UIImage.init(named: dict["selected_image"].stringValue), for: .normal)
            cell.viewOfProgressview.backgroundColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1.0)
            cell.btnProgressName.isSelected = true
        }
        else
        {
            cell.btnImgOfProgress.setImage(UIImage.init(named: dict["image"].stringValue), for: .normal)
            cell.viewOfProgressview.backgroundColor = UIColor.init(red: 50/255, green: 50/255, blue: 50/255, alpha: 1.0)
             cell.btnProgressName.isSelected = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK:- Service
    
    func getTrackOrder()
    {
        let kRegiURL = "\(AddhirhamMainURL)orderTrack.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "order_id" :dict["order_id"].stringValue] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.arrOfTrackOrder = []
                      
                        if (json["orderstatus"]["status"].stringValue).lowercased() == "pending" || (json["orderstatus"]["status"].stringValue).lowercased() == "pending_payment"
                        {
                            self.strStatus = 0
                            self.btnCancelOutlet.isHidden = false
                        }
                        else if (json["orderstatus"]["status"].stringValue).lowercased() == "processing"
                        {
                            self.strStatus = 1
                            self.btnCancelOutlet.isHidden = true
                        }
                        else
                        {
                           self.strStatus = 2
                           self.btnCancelOutlet.isHidden = true
                        }
                        
                        self.arrOfTrackOrder.append(json["product_info"])
                        
                        var dict = [String:String]()
                        dict["name"] = "Order Placed"
                        dict["description"] = "The order has been placed and we are preparing your order."
                        dict["selected_image"] = "ic_order_placed_selected_tracking_details"
                        dict["image"] = "ic_order_placed_unselected_tracking_details"
                        dict["status"] = String(self.strStatus)
                        self.arrOfTrackOrder.append(JSON(dict))
                        
                        dict = [String:String]()
                        dict["name"] = "Shipped"
                        dict["description"] = "The order is shipped. It is now processing and will reach you on time."
                        dict["selected_image"] = "ic_order_out_shipped_selected_tracking_details"
                        dict["image"] = "ic_order_out_shipped_unselected_tracking_details"
                        dict["status"] = String(self.strStatus)
                        self.arrOfTrackOrder.append(JSON(dict))
                        
                      /*  dict = [String:String]()
                        dict["name"] = "Processing Order"
                        dict["description"] = "The order is on the way. it is being transported to your location."
                        dict["selected_image"] = "ic_order_proccessing_selected_tracking_details"
                        dict["image"] = "ic_order_proccessing_unselected_tracking_details"
                        dict["status"] = strStatus
                        self.arrOfTrackOrder.append(JSON(dict))
                        
                        dict = [String:String]()
                        dict["name"] = "Out for Delivery"
                        dict["description"] = "The order out of delivery. it will reach you before the end of the day."
                        dict["selected_image"] = "ic_order_out_for_delivery_selected_tracking_details"
                        dict["image"] = "ic_order_out_for_delivery_unselected_tracking_details"
                        dict["status"] = strStatus
                        self.arrOfTrackOrder.append(JSON(dict))*/
                        
                        dict = [String:String]()
                        dict["name"] = "Order delivered"
                        dict["description"] = "The order was delivered."
                        dict["selected_image"] = "ic_order_delivered_selected_tracking_details"
                        dict["image"] = "ic_order_delivered_unselected_tracking_details"
                        dict["status"] = String(self.strStatus)
                        self.arrOfTrackOrder.append(JSON(dict))
                      
                        self.strProductId = json["product_info"]["order#"].stringValue
                        
                       /* self.imgOfProduct.sd_setImage(with: json["product_info"]["image"].url, placeholderImage:#imageLiteral(resourceName: "phone"))
                        self.lblProductName.text = json["product_info"]["name"].stringValue
                        
                        self.strProductId = json["product_info"]["order#"].stringValue
                        self.lblOrderNo.text = "Order #\(json["product_info"]["order#"].stringValue)"*/
                        
                        print("arrOfTrackOrder \(self.arrOfTrackOrder)")
                        self.tblOfTrackOrder.reloadData()
                        /*self.arrOfProductList = []
                        self.arrOfProductList = json["categoryProduct"].arrayValue
                        for i in 0..<self.arrOfProductList.count
                        {
                            var dict = self.arrOfProductList[i]
                            let arrQnt = self.SetupQuantityArray(qnt:dict["qty"].intValue)
                            dict["arrquantiy"] = JSON(arrQnt)
                            dict["current_index"] = "4"
                            self.arrOfProductList[i] = dict
                        }
                        // print("arrOfProductList:- \(self.arrOfProductList)")
                        self.tblProductList.reloadData()
                        
                        self.dictFilter = json["filter"]*/
                    }
                    else
                    {
                      /*  strNoRecordMessage = json["message"].stringValue
                        self.tblProductList.reloadData()*/
                        
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func cancelOrder()
    {
        let kRegiURL = "\(AddhirhamMainURL)cancleOrder.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "order#" :strProductId] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        KSToastView.ks_showToast(json["ordercancle"]["code"].stringValue, duration: ToastDuration)
                       
                    }
                    else
                    {
                         KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    

  
}

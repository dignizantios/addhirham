//
//  WebPaymentVC.swift
//  Addirham
//
//  Created by Jaydeep on 06/03/18.
//  Copyright © 2018 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import NVActivityIndicatorView
import KSToastView
import WebKit

class WebPaymentVC: UIViewController,NVActivityIndicatorViewable {
    
    //MARK:- Variable declration
    
    var dict:JSON?
    var scriptName:String = "AndroidFunction"
    var webViewOfPayment:WKWebView?
    var strOrderPlace = String()
    
    //MARK:- Outlet ZOme
    
    @IBOutlet weak var viewOfPayment: UIView!
    
    //MARK:- ViewLifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelagte.tabBarController.tabBar.isHidden = true
        
        // var scriptName:String = "AndroidFunction" which is used in javascript
        //self.webViewOfPayment.navigationDelegate = self
        let contentController = WKUserContentController()
        // let script = "webkit.messageHandlers.\(self.scriptName).postMessage(document.URL)"
        let userScript = WKUserScript(source: self.scriptName, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        contentController.addUserScript(userScript)
        contentController.add(self, name: self.scriptName)
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        self.webViewOfPayment = WKWebView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height-44), configuration: config)
        // self.webViewOfPayment.delegate = self
        self.viewOfPayment.addSubview(self.webViewOfPayment!)
        
        var urlRequest = URLRequest(url: URL(string: dict?["url"].stringValue ?? AddirhamPaymentURL)!)
        
        /*below code was not a correct one.*/
        
        //        var urlRequest = URLRequest(url: URL(string: String(format: "https://www.addirham.com/ipay/ipay.php?grandtotal=%@&orderid=%@&mobile=mobile",(dict?["grandtotal"].stringValue)!,(dict?["orderid"].stringValue)!))!)
        //        urlRequest.httpMethod = "POST"
        //        let query :[String:String] = ["grandtotal":dict?["grandtotal"].stringValue ?? "0",
        //                                      "orderid":dict?["orderid"].stringValue ?? "0",
        //                                      "mobile":"mobile"]
        //        let jsonData = try? JSONSerialization.data(withJSONObject: query, options: [])
        //        urlRequest.httpBody = jsonData
        //        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        /*changed to this*/
        
        var data = Data()
        var parameter = String()
        parameter = String(format:"grandtotal=%@&orderid=%@&mobile=mobile",(dict?["grandtotal"].stringValue)!,strOrderPlace)
        urlRequest.httpMethod = "POST"
        
        urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("no-cache", forHTTPHeaderField: "Cache-Control")
        data = parameter.data(using: .utf8)!
        urlRequest.httpBody = data
        
        self.webViewOfPayment?.navigationDelegate=self
        self.webViewOfPayment?.load(urlRequest)
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Service
    
    func orderConfirm()
    {
        let kRegiURL = "\(AddhirhamMainURL)orderconfirm.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "quote_id" :getCartDetail("quote_id"),
                         "flag" :"1",
                         "order_id" :dict?["orderid"].stringValue ?? "0"] as [String : Any]
            
            print("parm \(param)")
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                 //   KSToastView.ks_showToast("Called API Order Confirm", duration: ToastDuration)
                    if json["code"].stringValue == "101" //json["code"].stringValue == "100" || json["code"].stringValue == "101"
                    {
                        //print(json)
                     //   KSToastView.ks_showToast(json["order"]["sucess"].stringValue, duration: ToastDuration)
                        guard let userDetail = UserDefaults.standard.value(forKey: "cartDetail") as? Data else { return }
                        var data = JSON(userDetail)
                        data["quote_id"] = "0"
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "cartDetail")
                        strTotalCartItem = "0"
                        Defaults.setValue(strTotalCartItem, forKey: "CartItem")
                        Defaults.synchronize()
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SuccessFailureVC") as! SuccessFailureVC
                        obj.isPaymentSucceess = true
                        self.navigationController?.pushViewController(obj, animated: true)
                        
                    }
                    else
                    {
                         KSToastView.ks_showToast("Your Order is not sucessfully placed", duration: ToastDuration)
                        guard let userDetail = UserDefaults.standard.value(forKey: "cartDetail") as? Data else { return }
                        var data = JSON(userDetail)
                        data["quote_id"] = "0"
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "cartDetail")
                        strTotalCartItem = "0"
                        Defaults.setValue(strTotalCartItem, forKey: "CartItem")
                        Defaults.synchronize()
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SuccessFailureVC") as! SuccessFailureVC
                        obj.isPaymentSucceess = false
                        self.navigationController?.pushViewController(obj, animated: true)
                        //  KSToastView.ks_showToast("this Response contaibn order confirm", duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

    //MARK:- Action Zone
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        appDelagte.tabBarController.tabBar.isHidden = false
        goToRootOfTab(index: 0)
    }
    
}


//MARK:- webview delegate methods

extension WebPaymentVC:WKNavigationDelegate
{
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
    {
        print("didFinishurl: ", webView.url?.absoluteString as Any)
        if webView.url?.absoluteString == "http://www.addirham.com/mobile/mobile_payment_success.php"
        {
            orderConfirm()
          /*  let obj = self.storyboard?.instantiateViewController(withIdentifier: "SuccessFailureVC") as! SuccessFailureVC
            obj.isPaymentSucceess = true
            self.navigationController?.pushViewController(obj, animated: true)*/
        }
        if webView.url?.absoluteString == "http://www.addirham.com/mobile/mobile_payment_failure.php"
        {
            guard let userDetail = UserDefaults.standard.value(forKey: "cartDetail") as? Data else { return }
            var data = JSON(userDetail)
            data["quote_id"] = "0"
            guard let rowdata = try? data.rawData() else {return}
            Defaults.setValue(rowdata, forKey: "cartDetail")
            strTotalCartItem = "0"
            Defaults.setValue(strTotalCartItem, forKey: "CartItem")
            Defaults.synchronize()
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "SuccessFailureVC") as! SuccessFailureVC
            obj.isPaymentSucceess = false
            self.navigationController?.pushViewController(obj, animated: true)
        }
    //    KSToastView.ks_showToast("url \(webView.url?.absoluteString)", duration: 5)
        
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation, withError error: Error) {
        print("webView:\(webView) didFailNavigation:\(navigation) withError:\(error)")
        print("didfailurl: ", webView.url?.absoluteString as Any)
        
        guard let userDetail = UserDefaults.standard.value(forKey: "cartDetail") as? Data else { return }
        var data = JSON(userDetail)
        data["quote_id"] = "0"
        guard let rowdata = try? data.rawData() else {return}
        Defaults.setValue(rowdata, forKey: "cartDetail")
        strTotalCartItem = "0"
        Defaults.setValue(strTotalCartItem, forKey: "CartItem")
        Defaults.synchronize()
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SuccessFailureVC") as! SuccessFailureVC
        obj.isPaymentSucceess = false
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!)
    {
        print("didstarturl: ", webView.url?.absoluteString as Any)
        
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping ((WKNavigationActionPolicy) -> Void)) {
        print("webView:\(webView) decidePolicyForNavigationAction:\(navigationAction) decisionHandler:\(decisionHandler)")
        print("url: ", webView.url?.absoluteString as Any)
        
        if let url = webView.url {
            print(url.absoluteString)
        }
        decisionHandler(.allow)
    }
}

extension WebPaymentVC:WKScriptMessageHandler
{

    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("Print \(message.body)")
        //KSToastView.ks_showToast("Called Webview", duration: ToastDuration)
        if message.name == scriptName {
            if let url = message.body as? String {
                //if url.contains("http://money.cnn.com/") {
                print("!!! \(url)")
                //orderConfirm()
                // }
            }
        }
        else
        {
        //    KSToastView.ks_showToast("Mesaage \(message.body)", duration: 5)
        //    KSToastView.ks_showToast("Mesaage \(message.name)", duration: 10)
        }
    }
}

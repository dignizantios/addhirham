//
//  ShippingAddressViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 28/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import NVActivityIndicatorView
import KSToastView
import Firebase

class ShippingAddressViewController: UIViewController,UITextFieldDelegate,NVActivityIndicatorViewable
{
    
    //MARK:- Outlet ZOne
    
    @IBOutlet var txtEmailAddress: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var btn_SideMenu: UIButton!

    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        [txtEmailAddress,txtPassword].forEach({(textfield) in
            textfield?.delegate = self
        })
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.redirectToShippingAddress), name: NSNotification.Name(rawValue: "RedirectToAddrees"), object: nil)
      //  btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Private Method
    
    @objc func redirectToShippingAddress()
    {
        self.dismiss(animated: false, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RedirectToShipAddrees"), object: nil)
    }   
    
    
   
    //MARK: - textfield delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == self.txtEmailAddress
        {
            return txtPassword.becomeFirstResponder()
        }
        else{
            return txtPassword.resignFirstResponder()
        }
    }
    
    //MARK:-Acion Zone

    @IBAction func btnLoginAction(_ sender: Any)
    {
        if ((self.txtEmailAddress.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
        {
            KSToastView.ks_showToast("Please enter email address", duration: ToastDuration)
        }
        else
        {
            let emailid: String = self.txtEmailAddress.text!
            let myStringMatchesRegEx: Bool = isValidEmail(emailAddressString: emailid)
            if myStringMatchesRegEx == false
            {
                 KSToastView.ks_showToast("Please enter valid email address", duration: ToastDuration)
            }
            else if (self.txtPassword.text?.isEmpty)!
            {
                 KSToastView.ks_showToast("Please enter password", duration: ToastDuration)
            }
            else
            {
                self.view.endEditing(true)
                UserLogin()
            }
            
        }
        
        
    }
    @IBAction func btnRegisterAction(_ sender: Any)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "UserRegistrationViewController") as! UserRegistrationViewController
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: true, completion: nil)
       // self.navigationController?.pushViewController(method, animated: true)
    }
    @IBAction func btnCheckoutAsGuestAction(_ sender: Any)
    {
        redirectToShippingAddress()
        
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnForgotPasswordAction(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.present(obj, animated: false, completion: nil)
    }
    //MARK:- Service
    
    func UserLogin()
    {
        let kRegiURL = "\(AddhirhamMainURL)customer.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey":HashKey,
                         "email" :self.txtEmailAddress.text ?? "",
                         "password" :self.txtPassword.text ?? "",
                         "device_token" :Messaging.messaging().fcmToken ?? "",
                         "device_type" :DeviceType,
                         "type":"login",
                         "quoteId":getCartDetail("quote_id")]
            
            print("parm \(param)")
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
               self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        /* guard let rowdata = try? json.rawData() else {return}
                         Defaults.setValue(rowdata, forKey: "cartDetail")
                         Defaults.synchronize()*/
                        // print(json)
                        let data = json["message"]
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetail")
                        Defaults.setValue(rowdata, forKey: "cartDetail")
                        Defaults.synchronize()
                        let address = json["message"]["address"]
                        guard let rowAdddata = try? address.rawData() else {return}
                        Defaults.setValue(rowAdddata, forKey: "userAddressDetail")
                        Defaults.synchronize()
                        strTotalCartItem = json["message"]["customerdata"]["count"].stringValue
                        Defaults.setValue(strTotalCartItem, forKey: "CartItem")
                        Defaults.synchronize()
                        Defaults.setValue(json["customerdata"]["profile_picture"].stringValue, forKey: "userProfilePic")
                        Defaults.synchronize()
                        self.dismiss(animated: true, completion: nil)
                        
                        dictLoggedUser = getLoggedUserDetail()
                       /* let method = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
                        self.navigationController?.pushViewController(method, animated: true)*/
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}

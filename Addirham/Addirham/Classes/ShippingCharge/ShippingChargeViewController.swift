//
//  ShippingChargeViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 28/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView

class ShippingChargeViewController: UIViewController,NVActivityIndicatorViewable,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    //MARK:- Variable Declaration
    
    var arrOfShippingCharge:[JSON] = []
    var isComeFromGuestUser = Bool()
    
    //MARK:- Outlet Zone
    
    @IBOutlet var lblCharge: UILabel!
    @IBOutlet var btn_SideMenu: UIButton!
    @IBOutlet weak var colectionOfShippingCharge: UICollectionView!
    @IBOutlet weak var heightOfCollectionShippingCharge: NSLayoutConstraint!
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        self.lblCharge.text = "OMR 0.0"
        getShippingChargeList()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        colectionOfShippingCharge.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
       
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        colectionOfShippingCharge.removeObserver(self, forKeyPath: "contentSize")
        
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UICollectionView {
            
            print("contentSize:= \(colectionOfShippingCharge.contentSize.height)")
            self.heightOfCollectionShippingCharge.constant = colectionOfShippingCharge.contentSize.height
        }
    }
    
    //MARK:- Action ZOne
    
    @IBAction func btnNextStepAction(_ sender: Any)
    {
        var isSelectedCharge = Bool()
        for i in 0..<arrOfShippingCharge.count
        {
            var dict = arrOfShippingCharge[i]
            if dict["selected"].stringValue == "1"
            {
                dictShippingCharge = dict
                isSelectedCharge = true
                break
            }
        }
        if isSelectedCharge == false
        {
            KSToastView.ks_showToast("Please select the Shipping rate", duration: ToastDuration)
        }
        else
        {
            let method = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            method.fltShippingCharge = CGFloat(dictShippingCharge["Shippingprice"].floatValue)
            method.isComeFromGuestUser = isComeFromGuestUser
            self.navigationController?.pushViewController(method, animated: true)
        }
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelectChargeAction(_ sender: UIButton)
    {
        for i in 0..<arrOfShippingCharge.count
        {
            var dict = arrOfShippingCharge[i]
            dict["selected"] = "0"
            arrOfShippingCharge[i] = dict
        }
        var dict = arrOfShippingCharge[sender.tag]
        dict["selected"] = "1"
        arrOfShippingCharge[sender.tag] = dict
        self.colectionOfShippingCharge.reloadData()
    }
    
    
    //MARK:- Collectionview Delegate Method
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrOfShippingCharge.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell:ShippingChargeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShippingChargeCell", for: indexPath) as! ShippingChargeCell
        let dict = arrOfShippingCharge[indexPath.row]
        cell.lblChargeName.text = dict["Shippingtitle"].stringValue
        cell.btnSelectedChargeOutlet.tag = indexPath.row
        cell.lblCharge.text = "OMR \(dict["Shippingprice"].stringValue)"
        if dict["selected"].stringValue == "1"
        {
            cell.btnSelectedChargeOutlet.isSelected = true
            self.lblCharge.text = "OMR \(dict["Shippingprice"].stringValue)"
        }
        else
        {
           cell.btnSelectedChargeOutlet.isSelected = false
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = collectionView.frame.size.width/2
        return CGSize(width: width, height: 80)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
            for i in 0..<arrOfShippingCharge.count
            {
                var dict = arrOfShippingCharge[i]
                dict["selected"] = "0"
                arrOfShippingCharge[i] = dict
            }
            var dict = arrOfShippingCharge[indexPath.row]
            dict["selected"] = "1"
            arrOfShippingCharge[indexPath.row] = dict
            self.colectionOfShippingCharge.reloadData()
    }
    
    //MARK:- Service
    
    func getShippingChargeList()
    {
        let kRegiURL = "\(AddhirhamMainURL)shippingList.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param:[String:String] = [:]
            
            if isComeFromGuestUser == true{
                 param = ["hashkey":HashKey,
                          "region_code":dictGuestUserAddress["state_code"].stringValue]
            }
            else
            {
                param = ["hashkey":HashKey,
                         "region_code":getUserAddressDetail("state_code")       ]
            }
           
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        let dictData = json["ShippingCharges"]
                        self.arrOfShippingCharge = []
                        self.arrOfShippingCharge = dictData["sucess"].arrayValue
                        for i in 0..<self.arrOfShippingCharge.count
                        {
                            var dict = self.arrOfShippingCharge[i]
                            dict["selected"] = "0"
                            self.arrOfShippingCharge[i] = dict
                        }
                        self.colectionOfShippingCharge.reloadData()
                       
                    }
                    else
                    {
                        self.arrOfShippingCharge = []
                        self.colectionOfShippingCharge.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
}

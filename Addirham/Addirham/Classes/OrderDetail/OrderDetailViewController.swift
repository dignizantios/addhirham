//
//  OrderDetailViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 30/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView

class OrderDetailViewController: UIViewController,NVActivityIndicatorViewable,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    //MARK:- Varible Declaration
    
    var arrRecommondationList:[JSON] = []
    var dictDetail:JSON!
    //var dictLoggedUser = JSON()
    var strNoRecordMessage = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet var tblOrderDetail: UITableView!
    /*@IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblOrderDate: UILabel!
    @IBOutlet var lblOrderNo: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblTotalAmountPay: UILabel!
    @IBOutlet var lblShippingType: UILabel!
    @IBOutlet var lblTotalAmountSmall: UILabel!
    @IBOutlet var lblTotalAmountBig: UILabel!*/
    @IBOutlet var btn_SideMenu: UIButton!
   // @IBOutlet weak var lblDeliveryType: UILabel!
    @IBOutlet weak var lblTotalCount: UILabel!
    
    
    //MARK:- ViewLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dictLoggedUser = getLoggedUserDetail()
        btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        getOrdreDetail()
        
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tblOrderDetail.addSubview(refreshControl)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)     
        
        appDelagte.tabBarController.tabBar.isHidden = true
        self.lblTotalCount.text = strTotalCartItem
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       
      /*  if let cell = tblOrderDetail.cellForRow(at: IndexPath(row: 0, section: 0)) as? OrderDetailCell
        {
            cell.tblInnerDetail.removeObserver(self, forKeyPath: "contentSize")
        }*/
       
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            
           /* if let cell = tblOrderDetail.cellForRow(at: IndexPath(row: 0, section: 0)) as? OrderDetailCell
            {
                cell.heightOfInnerTable.constant = cell.tblInnerDetail.contentSize.height
                print("finalHeight:= \(cell.heightOfInnerTable.constant)")
            }*/
        }
    }
  
    //MARK:- Refresh Method
    @objc func refresh()
    {
        getOrdreDetail()
        refreshControl.endRefreshing()
    }
    //MARK:- Action Zone

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCartAction(_ sender: UIButton)
    {
        goToRootOfTab(index: 2)
    }
    
    @IBAction func btnAddQuantiyAction(_ sender: UIButton)
    {
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tblcell : ProductListCell? = self.tblOrderDetail.cellForRow(at: indexPath as IndexPath) as! ProductListCell?
        if let item = tblcell?.collectionOfQuantity
        {
            print("Visible Cell \(item.indexPathsForVisibleItems)")
            let arrOfIndexpath:[IndexPath] = item.indexPathsForVisibleItems
            let sortedArray = arrOfIndexpath.sorted {$0.row < $1.row}
            print("sortedArray \(arrOfIndexpath.sorted {$0.row < $1.row})")
            var dictInner = arrRecommondationList[sender.tag]
            // let currentIndex:Int = dictInner["current_index"].intValue
            let arrQuntity = dictInner["arrquantiy"]
            /* if currentIndex == (arrQuntity.count - 1)
             {
             return
             }*/
            //   item.scrollToItem(at: arrOfIndexpath[(arrOfIndexpath.count - 1)], at:.right, animated: true)
            //dictInner["current_index"] = JSON(currentIndex+1)
            // arrOfProductList[sender.tag] = dictInner
            if sortedArray.count == 6
            {
                item.scrollToItem(at: sortedArray[5], at:.right, animated: true)
            }
            else
            {
                let lastIndexPath = sortedArray[4]
                if (lastIndexPath.row) + 1 == arrQuntity.count
                {
                    return
                }
                item.scrollToItem(at: IndexPath(item: (lastIndexPath.row) + 1, section: 0), at:.right, animated: true)
            }
            
        }
    }
    
    @IBAction func btnMinusQuantiyAction(_ sender: UIButton) {
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tblcell : ProductListCell? = self.tblOrderDetail.cellForRow(at: indexPath as IndexPath) as! ProductListCell?
        if let item = tblcell?.collectionOfQuantity
        {
            print("Visible Cell \(item.indexPathsForVisibleItems)")
            let arrOfIndexpath:[IndexPath] = item.indexPathsForVisibleItems
            let sortedArray = arrOfIndexpath.sorted {$0.row < $1.row}
            print("sortedArray \(arrOfIndexpath.sorted {$0.row < $1.row})")
            /*  var dictInner = arrOfProductList[sender.tag]
             let currentIndex:Int = dictInner["current_index"].intValue
             if currentIndex == 4
             {
             return
             }*/
            //  var indexpath: NSIndexPath = (arrOfIndexpath[0] as! NSIndexPath)
            //item.scrollToItem(at: arrOfIndexpath[0], at:.left, animated: true)
            // dictInner["current_index"] = JSON(currentIndex-1)
            // arrOfProductList[sender.tag] = dictInner
            let lastIndexPath = sortedArray[0]
            if lastIndexPath.row == 0
            {
                return
            }
            item.scrollToItem(at: IndexPath(item: (lastIndexPath.row) - 1, section: 0), at:.left, animated: true)
        }
    }
    
    @IBAction func btnFavoriteAction(_ sender: UIButton)
    {
        if dictLoggedUser["customerId"].stringValue == ""
        {
            let alertController = UIAlertController(title: AppName, message: "You are not access this feature because you are not login!!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Login", style: .default) { (action) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let obj = storyboard.instantiateViewController(withIdentifier: "ShippingAddressViewController") as! ShippingAddressViewController
             //   obj.modalPresentationStyle = .overCurrentContext
               // obj.modalTransitionStyle = .crossDissolve
                self.present(obj, animated: true, completion: nil)
            }
            alertController.addAction(okAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true)
        }
        else
        {
            var dictQuantity = arrRecommondationList[sender.tag]
            addToWishList(strProductID:dictQuantity["product_id"].stringValue,strCustId:dictLoggedUser["customerId"].stringValue)
        }
    }
    @IBAction func btnComapareAction(_ sender: UIButton)
    {
        var dictQuantity = arrRecommondationList[sender.tag]
        if dictQuantity["is_compare"].stringValue == "1"
        {
            for i in 0..<strComapareID.count
            {
                if strComapareID[i] == dictQuantity["product_id"].stringValue
                {
                    strComapareID.remove(at: i)
                    break
                }
            }
            dictQuantity["is_compare"] = "0"
            arrRecommondationList[sender.tag] = dictQuantity
            self.tblOrderDetail.reloadData()
            return
        }
        
        if strComapareID.count == 2
        {
            KSToastView.ks_showToast("You can compare only two items.", duration: ToastDuration)
            return
        }
        
        strComapareID.append(dictQuantity["product_id"].stringValue)
        if strComapareID[0] == "0"
        {
            strComapareID.remove(at: 0)
        }
        print("strComapareID \(strComapareID)")
        if dictQuantity["is_compare"].stringValue == "0"
        {
            dictQuantity["is_compare"] = "1"
        }
        else
        {
            dictQuantity["is_compare"] = "0"
        }
        arrRecommondationList[sender.tag] = dictQuantity
        self.tblOrderDetail.reloadData()
        /*let strTemp = strArrayCompare.removeDuplicates()
         // strComapareID = String()
         strComapareID = strTemp.joined(separator: ",")
         print("strComapareID \(strComapareID)")*/
    }
    
    @IBAction func btnAddToCartAction(_ sender: UIButton)
    {
        var isSelected = Bool()
        var strQunty = String()
        var dictQuantity = arrRecommondationList[sender.tag]
        var arrQuntity = dictQuantity["arrquantiy"]
        for i in 0..<arrQuntity.count
        {
            var dictInner = arrQuntity[i]
            if dictInner["selected"].stringValue == "1"
            {
                isSelected = true
                strQunty = dictInner["quanity_name"].stringValue
                break
            }
        }
        if isSelected == false
        {
            KSToastView.ks_showToast("Please select at least one quantity", duration: ToastDuration)
        }
        else
        {
            addToCartProduct(strProductID: dictQuantity["product_id"].stringValue, strQuantiy: strQunty,type:1)
        }
    }
    
    @IBAction func btnBuyNowAction(_ sender: UIButton)
    {
        var isSelected = Bool()
        var strQunty = String()
        var dictQuantity = arrRecommondationList[sender.tag]
        var arrQuntity = dictQuantity["arrquantiy"]
        for i in 0..<arrQuntity.count
        {
            var dictInner = arrQuntity[i]
            if dictInner["selected"].stringValue == "1"
            {
                isSelected = true
                strQunty = dictInner["quanity_name"].stringValue
                break
            }
        }
        if isSelected == false
        {
            KSToastView.ks_showToast("Please select at least one quantity", duration: ToastDuration)
        }
        else
        {
            addToCartProduct(strProductID: dictQuantity["product_id"].stringValue, strQuantiy: strQunty,type:2)
        }
    }
    
    //MARK:- Setup Quantity Array
    
    func SetupQuantityArray(qnt:Int) ->  [JSON]
    {
        var arrOfQuantity:[JSON] = []
        for i in 0..<qnt
        {
            var dict = [String:String]()
            dict["quanity_name"] = String(i+1)
            dict["selected"] = "0"
            arrOfQuantity.append(JSON(dict))
        }
        return arrOfQuantity
    }
    
    //MARK:- TableView delagte Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblOrderDetail
        {
            if arrRecommondationList.count == 0
            {
                let lbl = UILabel()
                lbl.text = strNoRecordMessage
                lbl.textAlignment = NSTextAlignment.center
                lbl.textColor = UIColor.black
                lbl.center = tableView.center
                tableView.backgroundView = lbl
                return 0
            }
            tableView.backgroundView = nil
            return arrRecommondationList.count
           // return 1
        }
        else
        {
            let dictOfOrderList = arrRecommondationList[0]
            print("Total Count\((dictOfOrderList["product_list"].arrayValue).count)")
            return (dictOfOrderList["product_list"].arrayValue).count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblOrderDetail
        {
            if indexPath.row == 0
            {
                let cell:OrderDetailCell = self.tblOrderDetail.dequeueReusableCell(withIdentifier: "OrderDetailCell") as! OrderDetailCell
                let dictOfOrderList = arrRecommondationList[indexPath.row]
                print("dictOfOrderList \(dictOfOrderList)")
                cell.tblInnerDetail.tag = indexPath.row
                cell.tblInnerDetail.delegate = self
                cell.tblInnerDetail.dataSource = self
                cell.tblInnerDetail.reloadData()
                cell.heightOfInnerTable.constant = CGFloat(((dictOfOrderList["product_list"].arrayValue).count) * 120)
                print("height:- \(cell.heightOfInnerTable.constant)")
                cell.lblShippingAddress.text = dictOfOrderList["shipping_address"].stringValue
                cell.lblOrderStatus.text = dictOfOrderList["order_status"].stringValue
                cell.lblTotalAmountPay.text = "OMR \(dictOfOrderList["grand_total"].stringValue)"
                cell.lblTotalAmountSmall.text = "OMR \(dictOfOrderList["grand_total"].stringValue)"
                cell.lblTotalAmountBig.text =  "OMR \(dictOfOrderList["grand_total"].stringValue)"
                cell.lblShippingType.text =  "OMR \(dictOfOrderList["shippingcharges"].stringValue)"
                cell.lblDeliveryType.text =  "\(dictOfOrderList["paymentmethode"].stringValue)"
                return cell
            }
            let cell:ProductListCell = self.tblOrderDetail.dequeueReusableCell(withIdentifier: "ProductListCell") as! ProductListCell
            let dictInner = arrRecommondationList[indexPath.row]
            cell.lblProductName.text = dictInner["name"].stringValue
            let strImage = dictInner["image"].stringValue
            let urlImage:URL = URL(string: strImage)!
            cell.imgOfProduct.sd_setImage(with: urlImage as URL, placeholderImage:#imageLiteral(resourceName: "phone"))
            cell.lblProductNewPrice.text = "OMR \(dictInner["regular_price"].stringValue)"
          //  cell.lblProductOldPrice.text = "OMR \(dictInner["regular_price"].stringValue)"
            cell.lblNewOutlet.roundLabelCorners([.topRight], radius: 5)
            if dictInner["new_product"].boolValue == true
            {
                cell.lblNewOutlet.isHidden = true
            }
            else
            {
                cell.lblNewOutlet.isHidden = true
            }
            if dictInner["qty"].intValue <= 5
            {
                cell.btnAddOutlet.isHidden = true
                cell.btnMinusOutlet.isHidden = true
            }
            else
            {
                cell.btnAddOutlet.isHidden = false
                cell.btnMinusOutlet.isHidden = false
            }
            if dictInner["qty"].intValue <= 0
            {
                cell.btnAddtoCartOutlet.isHidden = true
                cell.btnBuyNowOutlet.isHidden = true
            }
            else
            {
                cell.btnAddtoCartOutlet.isHidden = false
                cell.btnBuyNowOutlet.isHidden = false
            }
            
            cell.btnAddOutlet.tag = indexPath.row
            cell.btnMinusOutlet.tag = indexPath.row
            cell.btnAddtoCartOutlet.tag = indexPath.row
            cell.btnBuyNowOutlet.tag = indexPath.row
            cell.btnFavOutlet.tag = indexPath.row
            cell.btnCompareOutlet.tag = indexPath.row
            cell.collectionOfQuantity.tag = indexPath.row
            cell.collectionOfQuantity.delegate = self
            cell.collectionOfQuantity.dataSource = self
            cell.collectionOfQuantity.reloadData()
            
            //  let currentIndex:Int = dictInner["current_index"].intValue
            // cell.collectionOfQuantity.scrollToItem(at: IndexPath(item: currentIndex, section: 0), at:.right, animated: true)
            return cell
        }
        else
        {
            let cell:OrderDetailInnerCell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailInnerCell") as! OrderDetailInnerCell
            let dict = (arrRecommondationList[tableView.tag]["product_list"].arrayValue)[indexPath.row]
            cell.lblProductName.text = dict["name"].stringValue
            cell.imgOfProduct.sd_setImage(with: dict["image"].url, placeholderImage:#imageLiteral(resourceName: "phone"))
            cell.lblProductPrice.text = "OMR \(dict["price"].stringValue)"
            cell.lblTotalQuantity.text = "Quantity: \(dict["qty"].intValue)"
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        obj.dict = arrRecommondationList[indexPath.row]
       // obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:- CollectionView Delegate & Datasources
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let dictQuantity = arrRecommondationList[collectionView.tag]
        let arrQuntity = dictQuantity["arrquantiy"]
        return arrQuntity.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:QuantityCell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuantityCell", for: indexPath) as! QuantityCell
        let dictQuantity = arrRecommondationList[collectionView.tag]
        //  if indexPath.row == 0
        // print("dictQuantity:- \(dictQuantity)")
        let arrQuntity = dictQuantity["arrquantiy"]
        // print("arrQuntity:- \(arrQuntity)")
        let dictInnerQuantity = arrQuntity[indexPath.row]
        cell.lblQuantity.text = dictInnerQuantity["quanity_name"].stringValue
        cell.lblQuantity.cornerRadius = cell.lblQuantity.frame.size.width/2
        cell.lblQuantity.clipsToBounds = true
        if dictInnerQuantity["selected"].stringValue == "1"
        {
            cell.lblQuantity.textColor = UIColor.white
            cell.lblQuantity.backgroundColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1)
            // cell.collectionOfQuantity.scrollToItem(at: indexPath, at:.right, animated: true)
        }
        else
        {
            cell.lblQuantity.textColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1)
            cell.lblQuantity.backgroundColor = UIColor.white
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/5
        let height = collectionView.frame.size.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        var dictQuantity = arrRecommondationList[collectionView.tag]
        var arrQuntity = dictQuantity["arrquantiy"]
        for i in 0..<arrQuntity.count{
            var dictInner = arrQuntity[i]
            dictInner["selected"] = "0"
            arrQuntity[i] = dictInner
        }
        dictQuantity["arrquantiy"] = arrQuntity
        arrRecommondationList[collectionView.tag] = dictQuantity
        
        dictQuantity = arrRecommondationList[collectionView.tag]
        arrQuntity = dictQuantity["arrquantiy"]
        var dictInnerQuantity = arrQuntity[indexPath.row]
        dictInnerQuantity["selected"] = "1"
        arrQuntity[indexPath.row] = dictInnerQuantity
        dictQuantity["arrquantiy"] = arrQuntity
        arrRecommondationList[collectionView.tag] = dictQuantity
        
        let indexPath = NSIndexPath(row: collectionView.tag, section: 0)
        let tblcell : ProductListCell? = self.tblOrderDetail.cellForRow(at: indexPath as IndexPath) as! ProductListCell?
        if let item = tblcell?.collectionOfQuantity
        {
            item.reloadData()
        }
    }
    
    //MARK:- Service
    
    func getOrdreDetail()
    {
        let kRegiURL = "\(AddhirhamMainURL)orderDetails.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "order_id" :dictDetail["order_id"].stringValue] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                       
                        let dictOfOrderList = json["Orderlist"]["sucess"]
                      /*  self.lblProductName.text = dictOfOrderList["name"].stringValue
                        let date = self.stringTodate(Formatter: "yyyy-MM-dd HH:mm:ss", strDate: dictOfOrderList["orderdate"].stringValue)
                        self.lblOrderDate.text = self.DateToString(Formatter: "dd-MM-yyyy", date: date)
                        self.lblOrderNo.text = dictOfOrderList["order#"].stringValue
                        self.lblAmount.text = "OMR \(dictOfOrderList["grand_total"].stringValue)"
                        
                        self.lblTotalAmountPay.text = "OMR \(dictOfOrderList["grand_total"].stringValue)"
                        self.lblTotalAmountSmall.text =  "OMR \(dictOfOrderList["grand_total"].stringValue)"
                        self.lblTotalAmountBig.text =  "OMR \(dictOfOrderList["grand_total"].stringValue)"
                        self.lblShippingType.text =  "OMR \(dictOfOrderList["shippingcharges"].stringValue)"
                        self.lblDeliveryType.text =  "\(dictOfOrderList["paymentmethode"].stringValue)"*/
                        
                        self.arrRecommondationList = []
                        self.arrRecommondationList.append(dictOfOrderList)
                        let arr = json["Orderlist"]["recommend"].arrayValue
                      //  self.arrRecommondationList.append(JSON(json["Orderlist"]["recommend"].arrayValue))
                        for i in 0..<arr.count
                        {
                            if i != 0
                            {
                                var dict = arr[i]
                                let arrQnt = self.SetupQuantityArray(qnt:dict["qty"].intValue)
                                dict["arrquantiy"] = JSON(arrQnt)
                                dict["current_index"] = "4"
                                self.arrRecommondationList.append(dict)
                            }
                        }
                        print("arrRecommondationList:- \(self.arrRecommondationList)")
                        self.tblOrderDetail.reloadData()
                    }
                    else
                    {
                        self.arrRecommondationList = []
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblOrderDetail.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func addToCartProduct(strProductID:String,strQuantiy:String,type:Int)
    {
        let kRegiURL = "\(AddhirhamMainURL)addtoCart.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "prodid" :strProductID,
                         "custid":dictLoggedUser["customerId"].stringValue,
                         "prodqty":strQuantiy,
                         "quoteId":getCartDetail("quote_id")] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        guard let rowdata = try? json.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "cartDetail")
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                        strTotalCartItem = json["count"].stringValue
                        Defaults.setValue(strTotalCartItem, forKey: "CartItem")
                        Defaults.synchronize()
                        self.lblTotalCount.text = strTotalCartItem
                        if type == 2
                        {
                            self.goToRootOfTab(index: 2)
                        }                        
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func addToWishList(strProductID:String,strCustId:String)
    {
        let kRegiURL = "\(AddhirhamMainURL)addToWhishlist.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "product_id" :strProductID,
                         "custid":strCustId] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        KSToastView.ks_showToast(json["AddToWhishlist"]["sucess"].stringValue, duration: ToastDuration)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

}

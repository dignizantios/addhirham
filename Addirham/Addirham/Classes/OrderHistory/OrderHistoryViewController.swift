//
//  OrderHistoryViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 30/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView

class OrderHistoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,NVActivityIndicatorViewable,UITextFieldDelegate
{
    
    //MARK:- Varible Declaration
    
    var arrOfOrderList:[JSON] = []
 //   var dictLoggedUser = JSON()
    var strNoRecordMessage = String()
    
    //MARK:- Outlet ZOne
    
    @IBOutlet weak var lblTotalCount: UILabel!
    @IBOutlet var txtSearch: CustomTextField!
    @IBOutlet var tblOrderHistory: UITableView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblTotalItems: UILabel!
    @IBOutlet var btn_SideMenu: UIButton!

    //MARK:- ViewLifeCycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        self.lblTotalItems.text = "Total Order (0 order)"
        if dictLoggedUser["customerId"].stringValue != ""
        {
            self.lblUserName.text = "Hello, \(dictLoggedUser["firstname"].stringValue) \(dictLoggedUser["lastname"].stringValue)"
        }
        dictLoggedUser = getLoggedUserDetail()
        if dictLoggedUser["customerId"].stringValue != ""
        {
            getUserOrderList()
        }
        else
        {
            strNoRecordMessage = "No record found"
            self.arrOfOrderList = []
            self.tblOrderHistory.reloadData()
        }
        txtSearch.delegate = self
        
        //self.tblOrderHistory.contentInset = UIEdgeInsetsMake(-70, 0, 0, 0);

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblTotalCount.text = strTotalCartItem
        appDelagte.tabBarController.tabBar.isHidden = false
       // setUnselectedTab()        
    }
    
    //MARK:- Textfield Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(obj, animated: true)
        return false
    }

    //MARK:- TableView delagte Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrOfOrderList.count == 0
        {
            let lbl = UILabel()
            lbl.text = strNoRecordMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrOfOrderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:OrderHistoryCell = self.tblOrderHistory.dequeueReusableCell(withIdentifier: "OrderHistoryCell") as! OrderHistoryCell
       
        let dict = arrOfOrderList[indexPath.row]
        cell.lblProduName.text = "\(dict["order_#"].stringValue)"
        
        cell.imOfProduct.sd_setImage(with: dict["image_url"].url, placeholderImage:#imageLiteral(resourceName: "phone"))
        cell.lblProductPrice.text = "OMR \(dict["grand_total"].stringValue)"
        cell.viewOfRatting.setScore(dict["review"].floatValue, withAnimation: false)
        //2017-11-07 11:35:25
        let date = stringTodate(Formatter: "yyyy-MM-dd HH:mm:ss", strDate: dict["orderdate"].stringValue)
        cell.lblOrderDate.text = DateToString(Formatter: "MM-dd-yyyy", date: date)
        cell.lblPaymentMethod.text = dict["paymentmethode"].stringValue
        cell.lblStatus.text = dict["order_status"].stringValue
        cell.btnViewOrderDetailOutlet.tag = indexPath.row
        cell.btnTrackOrderOutleet.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnViewOrderDetailAction(_ sender: UIButton)
    {
        let method = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
        method.dictDetail = arrOfOrderList[sender.tag]
      //  method.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(method, animated: true)
    }
    @IBAction func btnTrackOrderAction(_ sender: UIButton)
    {
        let method = self.storyboard?.instantiateViewController(withIdentifier: "TrackOrderViewController") as! TrackOrderViewController
        method.dict = arrOfOrderList[sender.tag]
       // method.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(method, animated: true)
    }
    
    @IBAction func btnCartAction(_ sender: UIButton)
    {
        goToRootOfTab(index: 2)
    }
    //MARK:- Service
    
    func getUserOrderList()
    {
        let kRegiURL = "\(AddhirhamMainURL)orderList.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "cust_id" :dictLoggedUser["customerId"].stringValue] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        
                        self.arrOfOrderList = []
                        self.arrOfOrderList = json["Orderlist"]["sucess"].arrayValue
                        self.tblOrderHistory.reloadData()
                        if json["total_order"].intValue < 1
                        {
                            self.lblTotalItems.text = "Total Order (\(json["total_order"].stringValue) order)"
                        }
                        else
                        {
                            self.lblTotalItems.text = "Total Order (\(json["total_order"].stringValue) orders)"
                        }
                        
                    }
                    else
                    {
                        self.arrOfOrderList = []
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblOrderHistory.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

}

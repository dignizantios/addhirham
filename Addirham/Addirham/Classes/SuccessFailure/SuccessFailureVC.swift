//
//  SuccessFailureVC.swift
//  Addirham
//
//  Created by Jaydeep on 27/02/18.
//  Copyright © 2018 Jaydeep Virani. All rights reserved.
//

import UIKit

class SuccessFailureVC: UIViewController {
    
    //MARK:- Variable Declration
    
    var isPaymentSucceess = Bool()
    
    //MARK:- OutletZone
    
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnMessageOutlet: CustomButton!
    

    //MARK:- ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if isPaymentSucceess == true
        {
            self.lblPaymentStatus.text = "Payment Success !"
            self.lblSubTitle.text = "It our pleasure to offer you our products."
        }
        else{
            self.lblPaymentStatus.text = "Payment Failed !"
            self.lblSubTitle.text = "It seems we have not received money."
            self.btnMessageOutlet.setTitle("Try again", for: .normal)
            self.btnMessageOutlet.setTitleColor(UIColor.init(red: 249/255, green: 69/255, blue: 69/255, alpha: 1), for: .normal)
            self.btnMessageOutlet.borderColor = UIColor.init(red: 249/255, green: 69/255, blue: 69/255, alpha: 1)
        }
    }

    //MARK:- Action Zone
    @IBAction func btnMessageAction(_ sender: Any) {
        if isPaymentSucceess == true
        {
             goToRootOfTab(index: 0)
        }
        else
        {
            goToRootOfTab(index: 2)
        }
       
    }
}

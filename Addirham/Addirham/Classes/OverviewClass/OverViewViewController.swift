
//
//  OverViewViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 28/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView
import WebKit

class OverViewViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,NVActivityIndicatorViewable,WKNavigationDelegate,WKScriptMessageHandler
{
    
    //MARK:- Varible Declaration
    
    var arrOfOverviewList:[JSON] = []
   // var dictLoggedUser = JSON()
    var strNoRecordMessage = String()
    var isObserveRemove = Bool()
    var isSelectedCreditCard = Bool()
    var fltShippingCharge = CGFloat()
    var strOrderId = String()
    var dictPaymentOverview:JSON?
    var scriptName:String = "AndroidFunction"
    var isComeFromGuestUser = Bool()
    
    //MARK:- Outlet Zone
    
    @IBOutlet var tblOverview: UITableView!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblTaxPrice: UILabel!
    @IBOutlet var lblTotalPrice: UILabel!
    @IBOutlet var btn_SideMenu: UIButton!
    @IBOutlet var heightOfOverView: NSLayoutConstraint!
    @IBOutlet weak var webViewOfPayment: WKWebView!
    // @IBOutlet weak var webViewOfPayment: UIWebView!
    @IBOutlet weak var viewOfPayment: UIView!
    

    //MARK:- ViewlifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        dictLoggedUser = getLoggedUserDetail()
        self.tblOverview.separatorStyle = .none
        
      /*  if dictLoggedUser["customerId"].stringValue != ""
        {
            getUserCartList()
        }
        else*/ if getCartDetail("quote_id") != "0"
        {
            getGuestUserCartList()
        }
        else
        {
            strNoRecordMessage = "No record found"
            self.arrOfOverviewList = []
            self.tblOverview.reloadData()
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        tblOverview.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       
    }
    
    deinit {
        if isObserveRemove == false
        {
            tblOverview.removeObserver(self, forKeyPath: "contentSize")
        }
    }
    
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblOverview.contentSize.height)")
            self.heightOfOverView.constant = tblOverview.contentSize.height
            //let finalHeight = self.heightOftblCommon.constant + 100
            // print("finalHeight:= \(finalHeight)")
            
        }
    }

    //MARK:- TableView delagte Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrOfOverviewList.count == 0
        {
            let lbl = UILabel()
            lbl.text = strNoRecordMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrOfOverviewList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:OverviewCell = self.tblOverview.dequeueReusableCell(withIdentifier: "OverviewCell") as! OverviewCell
        
        
      //  cell.viewOfDelEdit.roundCorners([.topRight,.bottomRight], radius: 5)
        let dict = arrOfOverviewList[indexPath.row]
        cell.lblProductName.text = dict["name"].stringValue
        cell.imgOfProduct.sd_setImage(with: dict["image"].url, placeholderImage:#imageLiteral(resourceName: "phone"))
        //cell.lblPrice.text = "OMR \(dict["special_price"].stringValue)"
        cell.lblProductTotalPrice.text = "OMR \(dict["total"].stringValue)"
        cell.lblQuantity.text = dict["qty"].stringValue
        if dict["special_price"].intValue > 0
        {
            cell.lblPrice.text  = "OMR \(dict["special_price"].stringValue)"
        }
        else{
            cell.lblPrice.text  = "OMR \(dict["price"].stringValue)"
           
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK:- Delegate
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("Print \(message.body)")
        KSToastView.ks_showToast("Called Webview", duration: ToastDuration)
        if message.name == scriptName {
            if let url = message.body as? String {
                //if url.contains("http://money.cnn.com/") {
                    print("!!! \(url)")
                    //orderConfirm()
               // }
            }
        }
        else
        {
            KSToastView.ks_showToast("Mesaage \(message.body)", duration: 5)
            KSToastView.ks_showToast("Mesaage \(message.name)", duration: 10)
        }
    }
    
    
    
    //MARK:- Action Zone
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnPlaceOrderAction(_ sender: Any) {
       /* if isSelectedCreditCard == true
        {
            saveOrder(strPaymentType: "mygateway")
        }
        else
        {
            saveOrder(strPaymentType: "cashondelivery")
        }*/
        
        saveOrder(strPaymentType: dictPaymentOverview?["code"].stringValue ?? "cashondelivery")
        
    }
    
    
    //MARK:- Service
    
   
    func getGuestUserCartList()
    {
        let kRegiURL = "\(AddhirhamMainURL)guestCartInfo.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "quote_id":getCartDetail("quote_id")] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        
                        self.arrOfOverviewList = []
                        self.arrOfOverviewList = json["cart_info"]["product"].arrayValue
                        self.tblOverview.reloadData()
                        self.lblPrice.text = "\(json["cart_info"]["global_currency_code"].stringValue) \(json["cart_info"]["subtotal"].stringValue)"
                        self.lblTaxPrice.text = "\(json["cart_info"]["global_currency_code"].stringValue) \(String(describing: self.fltShippingCharge))"
                        //print("int value \()")
                        guard let fltValue = NumberFormatter().number(from: (json["cart_info"]["grand_total"].stringValue).replacingOccurrences(of: ",", with: "")) else {
                            print("return")
                            self.lblTotalPrice.text = "\(json["cart_info"]["global_currency_code"].stringValue) 0"
                            return
                        }
                        let totalCount:CGFloat = self.fltShippingCharge + CGFloat(truncating: fltValue)
                        let strTotal = String(format: "%.2f", totalCount)
                      //  self.lblTotalPrice.text = "\(json["cart_info"]["global_currency_code"].stringValue) \(json["cart_info"]["subtotal_with_discount"].stringValue)"
                        print("add Comma \(strTotal.doubleValue)")
                        print("add Comma Currency \(strTotal.doubleValue.currency)")
                        print("add Comma Currency \((strTotal.doubleValue.currency).dropFirst())")
                        self.lblTotalPrice.text = "\(json["cart_info"]["global_currency_code"].stringValue) \((strTotal.doubleValue.currency).dropFirst())"
                        // self.btnTotalPrice.setTitle("\(json["cart_info"]["global_currency_code"].stringValue) \(json["cart_info"]["grand_total"].stringValue)", for: .normal)
                    }
                    else
                    {
                        self.arrOfOverviewList = []
                        self.lblPrice.text = "\(json["cart_info"]["global_currency_code"].stringValue) \(json["cart_info"]["subtotal"].stringValue)"
                        self.lblTaxPrice.text = "\(json["cart_info"]["global_currency_code"].stringValue) \(json["cart_info"]["subtotal_with_discount"].stringValue)"
                        self.lblTotalPrice.text = "\(json["cart_info"]["global_currency_code"].stringValue) \(json["cart_info"]["grand_total"].stringValue)"
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblOverview.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func saveOrder(strPaymentType:String)
    {
        let kRegiURL = "\(AddhirhamMainURL)saveOrder.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param:[String:String] = [:]
            if isComeFromGuestUser == false
            {
                param = ["hashkey" :HashKey,
                         "quote_id" :getCartDetail("quote_id"),
                         "firstname" :getUserAddressDetail("firstname"),
                         "lastname" :getUserAddressDetail("lastname"),
                         "street" :getUserAddressDetail("address_line"),
                         "city" :getUserAddressDetail("city"),
                         "region" :getUserAddressDetail("state_code"),
                         "postcode" :getUserAddressDetail("zipcode"),
                         "country_id" :getUserAddressDetail("country_id"),
                         "telephone" :getUserAddressDetail("telephone"),
                         "defaultbill" :"false",
                         "defaultship" :"false",
                         "shippingmethode":dictShippingCharge["Shippingname"].stringValue,
                         "paymentmethode":strPaymentType]
            }
            else
            {
                param = ["hashkey" :HashKey,
                         "quote_id" :getCartDetail("quote_id"),
                         "firstname" :dictGuestUserAddress["firstname"].stringValue,
                         "lastname" :dictGuestUserAddress["lastname"].stringValue,
                         "street" :dictGuestUserAddress["street"].stringValue,
                         "city" :dictGuestUserAddress["city"].stringValue,
                         "region" :dictGuestUserAddress["state_code"].stringValue,
                         "postcode" :dictGuestUserAddress["postcode"].stringValue,
                         "country_id" :dictGuestUserAddress["country_id"].stringValue,
                         "telephone" :dictGuestUserAddress["telephone"].stringValue,
                         "defaultbill" :"false",
                         "defaultship" :"false",
                         "shippingmethode":dictShippingCharge["Shippingname"].stringValue,
                         "paymentmethode":strPaymentType]
            }
            
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    
                    if json["code"].stringValue == "100" //json["code"].stringValue == "100" || json["code"].stringValue == "101"
                    {
                        //print(json)
                        if self.isSelectedCreditCard == true{
                           
                            let obj = self.storyboard?.instantiateViewController(withIdentifier: "WebPaymentVC") as! WebPaymentVC
                            obj.dict = json["mygetway"]
                            obj.strOrderPlace = json["orderplace"].stringValue
                            //obj.hidesBottomBarWhenPushed = true
                            self.navigationController?.pushViewController(obj, animated: false)

                            KSToastView.ks_showToast("your order is successfully placed", duration: ToastDuration)
                           // var scriptName:String = "AndroidFunction" which is used in javascript
                         /*   self.webViewOfPayment.navigationDelegate = self
                            let contentController = WKUserContentController()
                           // let script = "webkit.messageHandlers.\(self.scriptName).postMessage(document.URL)"
                            let userScript = WKUserScript(source: self.scriptName, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
                            contentController.addUserScript(userScript)
                            contentController.add(self, name: self.scriptName)
                            let config = WKWebViewConfiguration()
                            config.userContentController = contentController
                            self.webViewOfPayment = WKWebView(frame: UIScreen.main.bounds, configuration: config)
                           // self.webViewOfPayment.delegate = self
                            self.view.addSubview(self.webViewOfPayment)
                            let dict = json["mygetway"]
                            self.strOrderId = dict["orderid"].stringValue
                            var urlRequest = URLRequest(url: URL(string: dict["url"].stringValue)!)
                            urlRequest.httpMethod = "POST"
                            let query :[String:String] = ["grandtotal":dict["grandtotal"].stringValue,
                                                          "orderid":dict["orderid"].stringValue,
                                                          "mobile":"mobile"]
                            let jsonData = try? JSONSerialization.data(withJSONObject: query, options: [])
                            urlRequest.httpBody = jsonData
                            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                            //urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
                            self.webViewOfPayment.load(urlRequest)*/
                            
                           // let strUrl = "\(AddirhamPaymentURL)?grandtotal=\(dict["grandtotal"].stringValue)&orderid=\(dict["orderid"].stringValue)&mobile=mobile"
                          //  print("strUrl \(strUrl)")
                          //  KSToastView.ks_showToast(strUrl, duration: 120)
                         //   self.webViewOfPayment.loadRequest(URLRequest(url: URL(string: strUrl)!))
                         //   self.webViewOfPayment.loadRequest(urlRequest as URLRequest)
                        }
                        else
                        {
                            self.strOrderId = json["orderplace"].stringValue
                            let alertController = UIAlertController(title: AppName, message: "Thank you for your Purchase!! \n Your Oreder ID is : \(json["orderplace"].stringValue)", preferredStyle: UIAlertControllerStyle.alert)
                            
                            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                                //self.orderConfirm()
                                guard let userDetail = UserDefaults.standard.value(forKey: "cartDetail") as? Data else { return }
                                var data = JSON(userDetail)
                                data["quote_id"] = "0"
                                guard let rowdata = try? data.rawData() else {return}
                                Defaults.setValue(rowdata, forKey: "cartDetail")
                                strTotalCartItem = "0"
                                Defaults.setValue(strTotalCartItem, forKey: "CartItem")
                                Defaults.synchronize()
                                Defaults.synchronize()
                                self.isObserveRemove = true
                                self.tblOverview.removeObserver(self, forKeyPath: "contentSize")
                                self.goToRootOfTab(index: 0)
                                
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                           /* guard let userDetail = UserDefaults.standard.value(forKey: "cartDetail") as? Data else { return }
                            var data = JSON(userDetail)
                            data["quote_id"] = "0"
                            guard let rowdata = try? data.rawData() else {return}
                            Defaults.setValue(rowdata, forKey: "cartDetail")
                            Defaults.synchronize()
                         //  self.isObserveRemove = true
                        //    self.tblOverview.removeObserver(self, forKeyPath: "contentSize")
                         //   self.goToRootOfTab(index: 0)*/
                        }                    
                      
                    }
                    else
                    {
                        KSToastView.ks_showToast("Your Order is not sucessfully placed", duration: ToastDuration)
                        
                        //KSToastView.ks_showToast(json["code"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func orderConfirm()
    {
        let kRegiURL = "\(AddhirhamMainURL)orderconfirm.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "quote_id" :getCartDetail("quote_id"),
                         "flag" :"1",
                         "order_id" :strOrderId] as [String : Any]
            
            print("parm \(param)")            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    KSToastView.ks_showToast("Called API Order Confirm", duration: ToastDuration)
                    if json["code"].stringValue == "101" //json["code"].stringValue == "100" || json["code"].stringValue == "101"
                    {
                        //print(json)
                        KSToastView.ks_showToast(json["order"]["sucess"].stringValue, duration: ToastDuration)
                        guard let userDetail = UserDefaults.standard.value(forKey: "cartDetail") as? Data else { return }
                        var data = JSON(userDetail)
                        data["quote_id"] = "0"
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "cartDetail")
                        strTotalCartItem = "0"
                        Defaults.setValue(strTotalCartItem, forKey: "CartItem")
                        Defaults.synchronize()
                        self.isObserveRemove = true
                        self.tblOverview.removeObserver(self, forKeyPath: "contentSize")
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SuccessFailureVC") as! SuccessFailureVC
                        obj.isPaymentSucceess = true
                        self.navigationController?.pushViewController(obj, animated: true)
                        
                    }
                    else
                    {
                        KSToastView.ks_showToast("Your Order is not sucessfully placed", duration: ToastDuration)
                      //  KSToastView.ks_showToast("this Response contaibn order confirm", duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}

extension OverViewViewController:UIWebViewDelegate
{
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if request.url?.query != nil{
             print("URL \(request.url?.query)")
        }
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
       /* if let currentURL = webView.request?.url?.absoluteString
        {
            KSToastView.ks_showToast(currentURL, duration: ToastDuration)
            Alamofire.request(currentURL, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                if let json = respones.result.value
                {
                    KSToastView.ks_showToast("this is right", duration: ToastDuration)
                }
                else
                {
                    KSToastView.ks_showToast("This is wrong", duration: ToastDuration)
                }
            }
        }*/
        let html = webView.stringByEvaluatingJavaScript(from: "document.body.innerHTML")! as String
        let dict = convertToDictionary(text: html) as NSMutableDictionary
        print("dict \(dict)")
       // KSToastView.ks_showToast(dict)
    }
    
    func convertToDictionary(text: String) -> NSMutableDictionary
    {
        var dict = NSMutableDictionary()
        if let data = text.data(using: .utf8)
        {
            do
            {
                dict = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
                return dict
            } catch
            {
                print(error.localizedDescription)
            }
        }
        return dict
    }
}



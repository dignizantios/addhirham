//
//  CompairViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 30/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView
import UserNotifications


class CompairViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,NVActivityIndicatorViewable
{
    
    //MARK:- Variable Declaration
    
    var arrOfCompare:[JSON] = []
    var strFinalComapareID = String()
    var strNoRecordMessage = String()
    
    //MARK:- Outlet Zone
  
    @IBOutlet var btn_SideMenu: UIButton!
    @IBOutlet weak var collectionOfComapre: UICollectionView!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var lblProduct1: UILabel!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var lblProduct2: UILabel!
    @IBOutlet weak var lblProductDes1: UILabel!
    @IBOutlet weak var lblProductDes2: UILabel!
    @IBOutlet weak var lblProductColor1: UILabel!
    @IBOutlet weak var lblProductColor2: UILabel!
    @IBOutlet weak var lblProductPrice1: UILabel!
    @IBOutlet weak var lblProductPrice2: UILabel!
    
    //MARK:- ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelagte.tabBarController.tabBar.isHidden = false
         btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        
        let strTemp = strComapareID.removeDuplicates()
        // strComapareID = String()
        strFinalComapareID = strTemp.joined(separator: ",")
        print("strComapareID \(strFinalComapareID)")
        if strFinalComapareID != ""
        {
             getCompareList()
        }       
        
     //   let flow = collectionOfComapre.collectionViewLayout as! UICollectionViewFlowLayout
     //   flow.sectionHeadersPinToVisibleBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appDelagte.tabBarController.tabBar.isHidden = false
    }

    override func viewWillDisappear(_ animated: Bool) {
       strComapareID = [String]()
    }
    
    //MARK:- Setup Compare Detail
    
    func setUpDetail(arrDetail:[JSON])
    {
        // setup 1 product
        var dict = arrDetail[0]
        self.lblProduct1.text = dict["name"].stringValue
        self.img1.sd_setImage(with: dict["image"].url, placeholderImage:#imageLiteral(resourceName: "phone"))
        self.lblProductPrice1.text = "OMR \(dict["price"].stringValue)"
        self.lblProductColor1.text = dict["color"].stringValue
        self.lblProductDes1.text =  (dict["description"].stringValue).htmlToString
        
        // setup 2 product
        dict = arrDetail[1]
        self.lblProduct2.text = dict["name"].stringValue
        self.img2.sd_setImage(with: dict["image"].url, placeholderImage:#imageLiteral(resourceName: "phone"))
        self.lblProductPrice2.text = "OMR \(dict["price"].stringValue)"
        self.lblProductColor2.text = dict["color"].stringValue
        self.lblProductDes2.text =  (dict["description"].stringValue).htmlToString
    }
   

    //MARK:- Action Zone
    @IBAction func btnAddtoCartAction(_ sender: UIButton)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        obj.strProductId = strComapareID[sender.tag]
        obj.isComeFromCompare = true
       // obj.dict = arrOfCompare[sender.tag]
      //  obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:- CollectionView Delegate & Datasources
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrOfCompare.count == 0
        {
            let lbl = UILabel()
            lbl.text = strNoRecordMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = collectionView.center
            collectionView.backgroundView = lbl
            return 0
        }
        collectionView.backgroundView = nil
        return arrOfCompare.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CompareCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CompareCell", for: indexPath) as! CompareCell
      
        let dictCompare = arrOfCompare[indexPath.row]
        cell.lblProductName.text = dictCompare["name"].stringValue
        let strImage = dictCompare["image"].stringValue
        let urlImage:URL = URL(string: strImage)!       
        cell.imgOfProduct.sd_setImage(with: urlImage as URL, placeholderImage:#imageLiteral(resourceName: "phone"))
        cell.lblPrice.text = "OMR \(dictCompare["price"].stringValue)"
        cell.lblColorName.text = dictCompare["color"].stringValue
        cell.lblDesciption.text =  dictCompare["description"].stringValue
        cell.btnAddToCartOutlet.tag = indexPath.row
        return cell
    }
    
  /*  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/2
        let height = collectionView.frame.size.height
        return CGSize(width: width, height: height)
    }*/
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
       // let leftRightInset = self.view.frame.size.width / 90
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let commentView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CompareHeaderView", for: indexPath) as! CompareHeaderView
        
       // commentView.commentLabel.text = "Supplementary view of kind \(kind)"
        
        return commentView
    }
    
    
    //MARK:- Service
    
    func getCompareList()
    {
        let kRegiURL = "\(AddhirhamMainURL)addToCompare.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey":HashKey,
                         "product_id":strFinalComapareID] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        let dictData = json["compare_product"]
                        self.arrOfCompare = []
                        self.arrOfCompare = dictData["product_list"].arrayValue
                        self.setUpDetail(arrDetail: self.arrOfCompare)
                       /* var dict = [String:String]()
                        dict["name"] = "Product"
                        dict["description"] = "Description"
                        dict["color"] = "Color"
                        dict["price"] = "Price"
                        self.arrOfCompare.append(JSON(dict))
                        self.arrOfCompare.append(JSON(arrData))*/
                      //  self.collectionOfComapre.reloadData()
                    }
                    else
                    {
                     //   self.arrOfCompare = []
                     //   strNoRecordMessage = json["message"].stringValue
                      //  self.collectionOfComapre.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}

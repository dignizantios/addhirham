//
//  AboutViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 30/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import NVActivityIndicatorView
import KSToastView

class AboutViewController: UIViewController,NVActivityIndicatorViewable
{
    
    //MARK:- Outlet ZOne
    
    @IBOutlet weak var webOfAboutUs: UIWebView!
    @IBOutlet var lblAboutUs: UILabel!
    @IBOutlet var btn_SideMenu: UIButton!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var heightOfwebView: UIWebView!
    @IBOutlet weak var heightOfScroll: NSLayoutConstraint!
    
    //MARK:- ViewLifeCyclle

    override func viewDidLoad() {
        super.viewDidLoad()
         btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        getCMSBlock()
        // Do any additional setup after loading the view.
    }

    //MARK:- Memory Managament
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Action Zone
    
    @IBAction func btnFBAction(_ sender: Any) {
    }
    
    @IBAction func btnTwitterAction(_ sender: Any) {
    }
    
    @IBAction func btnPinInterestAcion(_ sender: Any) {
    }
    @IBAction func btnGmailAction(_ sender: Any) {
    }
    @IBAction func btnBlogAction(_ sender: Any) {
    }
    
    //MARK:- Service
    
    func getCMSBlock()
    {
        let kRegiURL = "\(AddhirhamMainURL)cmsBlock.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "static_id" :"about_us" ] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.webOfAboutUs.loadHTMLString( json["CmsBlock"].stringValue, baseURL: nil)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

}

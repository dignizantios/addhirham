//
//  ProductListViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 25/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView

class ProductListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate
{
    //MARK:- Variable Declaration
    
    var arrOfProductList:[JSON] = []
    var dict:JSON?
   // var dictLoggedUser = JSON()
    var dictFilter = JSON()
    var dictSelectedData = JSON()
    var isComeFromCategory = Bool()
    var dictCategory:JSON?
    var strNoRecordMessage = String()
    var isComeFromViewAll = Bool()
    
    //MARK:- Outlet Zone
    @IBOutlet var lblScreenTitle: UILabel!
    @IBOutlet var txtSearchProduct: CustomTextField!
    @IBOutlet var tblProductList: UITableView!
    @IBOutlet var btn_SideMenu: UIButton!
    @IBOutlet weak var lblTotalCount: UILabel!
    
    
    //MARK:- ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

     
       /* let numberOfItems = CGFloat((appDelagte.tabBarController.tabBar.items!.count))
        let tabBarItemSize = CGSize(width: (appDelagte.tabBarController.tabBar.frame.width) / numberOfItems,
                                    height: (appDelagte.tabBarController.tabBar.frame.height))
        appDelagte.tabBarController.tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor(red: 255/255.0, green: 255/255.0, blue:255/255.0, alpha: 1.0), size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.zero)
        appDelagte.tabBarController.tabBar.tintColor = .clear*/
      //  appDelagte.tabBarController.tabBar.selectionIndicatorImage =  UIImage(named: "ic_bottom_tab_home_unselected.png")?.withRenderingMode(.alwaysOriginal)
       // appDelagte.tabBarController.tabBar.barTintColor = UIColor.init(red: 90/255.0, green: 43/255.0, blue:123/255.0, alpha: 1.0)
      //  appDelagte.tabBarController.tabBar.items![0].image = UIImage(named: "ic_bottom_tab_home_unselected.png")?.withRenderingMode(.alwaysOriginal)
        btn_SideMenu.addTarget(self, action: #selector(backAction(_:)), for: .touchUpInside)
      //  SetupQuantityArray()
        print("dict \(dict)")
        if isComeFromCategory == false
        {
            getProductList(strParentId: "")
            if isComeFromViewAll == true
            {
                self.lblScreenTitle.text = "VIEW ALL"
            }
            else{
                self.lblScreenTitle.text = dict?["name"].stringValue
            }
            
        }
        else{
            getProductList(strParentId: dictCategory!["id"].stringValue)
            self.lblScreenTitle.text = dictCategory!["sub-category"].stringValue
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tblProductList.addSubview(refreshControl)
        
        txtSearchProduct.delegate = self
        txtSearchProduct.addTarget(self, action: #selector(self.didChangeText(textField:)), for: .editingChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getFilterList), name: NSNotification.Name(rawValue: "ProductListFilter"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getResetList), name: NSNotification.Name(rawValue: "ResetProductListFilter"), object: nil)
        
        
        self.lblTotalCount.text = strTotalCartItem
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        appDelagte.tabBarController.tabBar.isHidden = true
        dictLoggedUser = getLoggedUserDetail()
        NotificationCenter.default.addObserver(self, selector: #selector(self.getResetList), name: NSNotification.Name(rawValue: "ListRefresh"), object: nil)
    }
    
    //MARK:- Refresh Method
    
    @objc func refresh() {
        if isComeFromCategory == false
        {
            getProductList(strParentId: "")
        }
        else{
            getProductList(strParentId: dictCategory!["id"].stringValue)
        }
        refreshControl.endRefreshing()
    }
    
    //MARK:- Setup Quantity Array
    
    func SetupQuantityArray(qnt:Int) ->  [JSON]
    {
        var arrOfQuantity:[JSON] = []
        for i in 0..<qnt
        {
            var dict = [String:String]()
            dict["quanity_name"] = String(i+1)
            dict["selected"] = "0"
            arrOfQuantity.append(JSON(dict))
        }
        return arrOfQuantity
    }
    
    //MARK:- Textfield Delegate
    
    @objc func didChangeText(textField:UITextField)
    {
       //getSearchProductList()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        obj.strCategoryId = dict?["id"].stringValue ?? "2"
        //obj.arrOfSearchList = arrOfProductList
        self.navigationController?.pushViewController(obj, animated: true)
        return false
    }
    
    //MARK:- Action Zone
   
    @IBAction func btnAddQuantiyAction(_ sender: UIButton)
    {
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tblcell : ProductListCell? = self.tblProductList.cellForRow(at: indexPath as IndexPath) as! ProductListCell?
        if let item = tblcell?.collectionOfQuantity
        {
            print("Visible Cell \(item.indexPathsForVisibleItems)")
            let arrOfIndexpath:[IndexPath] = item.indexPathsForVisibleItems
            let sortedArray = arrOfIndexpath.sorted {$0.row < $1.row}
            print("sortedArray \(arrOfIndexpath.sorted {$0.row < $1.row})")
            var dictInner = arrOfProductList[sender.tag]
           // let currentIndex:Int = dictInner["current_index"].intValue
            let arrQuntity = dictInner["arrquantiy"]
           /* if currentIndex == (arrQuntity.count - 1)
            {
                return
            }*/
          //   item.scrollToItem(at: arrOfIndexpath[(arrOfIndexpath.count - 1)], at:.right, animated: true)
            //dictInner["current_index"] = JSON(currentIndex+1)
           // arrOfProductList[sender.tag] = dictInner
            if sortedArray.count == 6
            {
                item.scrollToItem(at: sortedArray[5], at:.right, animated: true)
            }
            else
            {
                let lastIndexPath = sortedArray[4]
                if (lastIndexPath.row) + 1 == arrQuntity.count
                {
                     return
                }
                item.scrollToItem(at: IndexPath(item: (lastIndexPath.row) + 1, section: 0), at:.right, animated: true)
            }
            
        }
    }
    
    @IBAction func btnMinusQuantiyAction(_ sender: UIButton) {
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tblcell : ProductListCell? = self.tblProductList.cellForRow(at: indexPath as IndexPath) as! ProductListCell?
        if let item = tblcell?.collectionOfQuantity
        {
            print("Visible Cell \(item.indexPathsForVisibleItems)")
            let arrOfIndexpath:[IndexPath] = item.indexPathsForVisibleItems
            let sortedArray = arrOfIndexpath.sorted {$0.row < $1.row}
            print("sortedArray \(arrOfIndexpath.sorted {$0.row < $1.row})")
          /*  var dictInner = arrOfProductList[sender.tag]
            let currentIndex:Int = dictInner["current_index"].intValue
            if currentIndex == 4
            {
                return
            }*/
          //  var indexpath: NSIndexPath = (arrOfIndexpath[0] as! NSIndexPath)
            //item.scrollToItem(at: arrOfIndexpath[0], at:.left, animated: true)
           // dictInner["current_index"] = JSON(currentIndex-1)
          // arrOfProductList[sender.tag] = dictInner
            let lastIndexPath = sortedArray[0]
            if lastIndexPath.row == 0
            {
                return
            }
            item.scrollToItem(at: IndexPath(item: (lastIndexPath.row) - 1, section: 0), at:.left, animated: true)
        }
    }
    
    @IBAction func btnFavoriteAction(_ sender: UIButton)
    {
        if dictLoggedUser["customerId"].stringValue == ""
        {
            //KSToastView.ks_showToast("Please Login First", duration: ToastDuration)
            let alertController = UIAlertController(title: AppName, message: "You are not access this feature because you are not login!!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Login", style: .default) { (action) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let obj = storyboard.instantiateViewController(withIdentifier: "ShippingAddressViewController") as! ShippingAddressViewController
               // obj.modalPresentationStyle = .overCurrentContext
              //  obj.modalTransitionStyle = .crossDissolve
                self.present(obj, animated: true, completion: nil)
            }
            alertController.addAction(okAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true)
        }
        else
        {
            var dictQuantity = arrOfProductList[sender.tag]
            if dictQuantity["wish_list"].boolValue == true
            {
                KSToastView.ks_showToast("Product alredy added to wishlist.", duration: ToastDuration)
                return
            }
            addToWishList(strProductID:dictQuantity["product_id"].stringValue,strCustId:dictLoggedUser["customerId"].stringValue)
        }
    }
    @IBAction func btnComapareAction(_ sender: UIButton)
    {
        var dictQuantity = arrOfProductList[sender.tag]
        if dictQuantity["is_compare"].stringValue == "1"
        {
            for i in 0..<strComapareID.count
            {
                if strComapareID[i] == dictQuantity["product_id"].stringValue
                {
                    strComapareID.remove(at: i)
                    break
                }
            }
            dictQuantity["is_compare"] = "0"
            arrOfProductList[sender.tag] = dictQuantity
            self.tblProductList.reloadData()
            return
        }
        
        if strComapareID.count == 2
        {
            KSToastView.ks_showToast("You can compare only two items.", duration: ToastDuration)
            return
        }
        
        strComapareID.append(dictQuantity["product_id"].stringValue)
        if strComapareID[0] == "0"
        {
            strComapareID.remove(at: 0)
        }
        print("strComapareID \(strComapareID)")
        if dictQuantity["is_compare"].stringValue == "0"
        {
           dictQuantity["is_compare"] = "1"
        }
        else
        {
            dictQuantity["is_compare"] = "0"
        }
        arrOfProductList[sender.tag] = dictQuantity
        self.tblProductList.reloadData()
        /*let strTemp = strArrayCompare.removeDuplicates()
       // strComapareID = String()
        strComapareID = strTemp.joined(separator: ",")
        print("strComapareID \(strComapareID)")*/
    }
    
    @IBAction func btnAddToCartAction(_ sender: UIButton)
    {
        var isSelected = Bool()
        var strQunty = String()
        var dictQuantity = arrOfProductList[sender.tag]
        var arrQuntity = dictQuantity["arrquantiy"]
        for i in 0..<arrQuntity.count
        {
            var dictInner = arrQuntity[i]
            if dictInner["selected"].stringValue == "1"
            {
                isSelected = true
                strQunty = dictInner["quanity_name"].stringValue
                break
            }
        }
        if isSelected == false
        {
            KSToastView.ks_showToast("Please select at least one quantity", duration: ToastDuration)
        }
        else
        {
            addToCartProduct(strProductID: dictQuantity["product_id"].stringValue, strQuantiy: strQunty,type:1)
           
            for i in 0..<arrQuntity.count{
                var dictInner = arrQuntity[i]
                dictInner["selected"] = "0"
                arrQuntity[i] = dictInner
            }
            dictQuantity["arrquantiy"] = arrQuntity
            arrOfProductList[sender.tag] = dictQuantity
        }
    }
    
    @IBAction func btnBuyNowAction(_ sender: UIButton)
    {
        var isSelected = Bool()
        var strQunty = String()
        var dictQuantity = arrOfProductList[sender.tag]
        var arrQuntity = dictQuantity["arrquantiy"]
        for i in 0..<arrQuntity.count
        {
            var dictInner = arrQuntity[i]
            if dictInner["selected"].stringValue == "1"
            {
                isSelected = true
                strQunty = dictInner["quanity_name"].stringValue
                break
            }
        }
        if isSelected == false
        {
            KSToastView.ks_showToast("Please select at least one quantity", duration: ToastDuration)
        }
        else
        {
            addToCartProduct(strProductID: dictQuantity["product_id"].stringValue, strQuantiy: strQunty,type:2)
        }
    }
    
    @IBAction func btnFilterAction(_ sender: UIButton)
    {
        let obj:FilterViewController = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        obj.modalPresentationStyle = .overCurrentContext
        obj.dictOfFilter = dictFilter
        obj.dictSelectedData = dictSelectedData
        obj.strComeFromScreen = "Product"
        obj.strCategoryId = dict?["id"].stringValue ?? "2"
        self.present(obj, animated: false, completion: nil)
    }
    
    @IBAction func btnCartAction(_ sender: UIButton)
    {
        goToRootOfTab(index: 2)
    }
    //MARK:- TableView Delegate Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrOfProductList.count == 0
        {
            let lbl = UILabel()
            lbl.text = strNoRecordMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrOfProductList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ProductListCell = self.tblProductList.dequeueReusableCell(withIdentifier: "ProductListCell") as! ProductListCell
        let dictInner = arrOfProductList[indexPath.row]
        cell.lblProductName.text = dictInner["name"].stringValue
        let strImage = dictInner["image"].stringValue
        let urlImage:URL = URL(string: strImage)!
        cell.imgOfProduct.sd_setImage(with: urlImage as URL, placeholderImage:#imageLiteral(resourceName: "phone"))
        cell.lblProductNewPrice.text = "OMR \(dictInner["regular_price"].stringValue)"
     //   cell.lblProductOldPrice.text = "OMR \(dictInner["regular_price"].stringValue)"
        cell.lblNewOutlet.roundLabelCorners([.topRight], radius: 5)
        if dictInner["new_product"].boolValue == true
        {
            cell.lblNewOutlet.isHidden = true
        }
        else
        {
           cell.lblNewOutlet.isHidden = true
        }
        if dictInner["qty"].intValue <= 5
        {
            cell.btnAddOutlet.isHidden = true
            cell.btnMinusOutlet.isHidden = true
        }
        else
        {
            cell.btnAddOutlet.isHidden = false
            cell.btnMinusOutlet.isHidden = false
        }
        if dictInner["qty"].intValue <= 0
        {
            cell.btnAddtoCartOutlet.isHidden = true
            cell.btnBuyNowOutlet.isHidden = true
        }
        else
        {
            cell.btnAddtoCartOutlet.isHidden = false
            cell.btnBuyNowOutlet.isHidden = false
        }
        
        /*if dictInner["is_compare"].stringValue == "1"
        {
            for i in 0..<strComapareID.count
            {
                if strComapareID[i] == dictInner["product_id"].stringValue
                {
         
                    break
                }
                else
                {
                    cell.btnCompareOutlet.isSelected = false
                    cell.btnCompareOutlet.backgroundColor = UIColor.white
                }
            }
        }*/
        if dictInner["is_compare"].stringValue == "1"
        {
            cell.btnCompareOutlet.isSelected = true
            cell.btnCompareOutlet.backgroundColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1)
        }
        else{
            cell.btnCompareOutlet.isSelected = false
            cell.btnCompareOutlet.backgroundColor = UIColor.white
        }
        
        if dictInner["wish_list"].boolValue == false
        {
            cell.btnFavOutlet.isSelected = false
            cell.btnFavOutlet.backgroundColor = UIColor.white
        }
        else
        {
            cell.btnFavOutlet.isSelected = true
            cell.btnFavOutlet.backgroundColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1)
        }
        
        cell.btnAddOutlet.tag = indexPath.row
        cell.btnMinusOutlet.tag = indexPath.row
        cell.btnAddtoCartOutlet.tag = indexPath.row
        cell.btnBuyNowOutlet.tag = indexPath.row
        cell.btnFavOutlet.tag = indexPath.row
        cell.btnCompareOutlet.tag = indexPath.row
        cell.collectionOfQuantity.tag = indexPath.row
        cell.collectionOfQuantity.delegate = self
        cell.collectionOfQuantity.dataSource = self
        cell.collectionOfQuantity.reloadData()
        
      //  let currentIndex:Int = dictInner["current_index"].intValue
       // cell.collectionOfQuantity.scrollToItem(at: IndexPath(item: currentIndex, section: 0), at:.right, animated: true)
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        obj.dict = arrOfProductList[indexPath.row]
       // obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:- CollectionView Delegate & Datasources
   
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let dictQuantity = arrOfProductList[collectionView.tag]
        let arrQuntity = dictQuantity["arrquantiy"]
        return arrQuntity.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:QuantityCell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuantityCell", for: indexPath) as! QuantityCell
        let dictQuantity = arrOfProductList[collectionView.tag]
      //  if indexPath.row == 0
       // print("dictQuantity:- \(dictQuantity)")
        let arrQuntity = dictQuantity["arrquantiy"]
       // print("arrQuntity:- \(arrQuntity)")
        let dictInnerQuantity = arrQuntity[indexPath.row]
        cell.lblQuantity.text = dictInnerQuantity["quanity_name"].stringValue
        cell.lblQuantity.cornerRadius = cell.lblQuantity.frame.size.width/2
        cell.lblQuantity.clipsToBounds = true
        if dictInnerQuantity["selected"].stringValue == "1"
        {
            cell.lblQuantity.textColor = UIColor.white
            cell.lblQuantity.backgroundColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1)
           // cell.collectionOfQuantity.scrollToItem(at: indexPath, at:.right, animated: true)
        }
        else
        {
            cell.lblQuantity.textColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1)
            cell.lblQuantity.backgroundColor = UIColor.white
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/5
        let height = collectionView.frame.size.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        var dictQuantity = arrOfProductList[collectionView.tag]
        var arrQuntity = dictQuantity["arrquantiy"]
        for i in 0..<arrQuntity.count{
            var dictInner = arrQuntity[i]
            dictInner["selected"] = "0"
            arrQuntity[i] = dictInner
        }
        dictQuantity["arrquantiy"] = arrQuntity
        arrOfProductList[collectionView.tag] = dictQuantity
        
        dictQuantity = arrOfProductList[collectionView.tag]
        arrQuntity = dictQuantity["arrquantiy"]
        var dictInnerQuantity = arrQuntity[indexPath.row]
        dictInnerQuantity["selected"] = "1"
        arrQuntity[indexPath.row] = dictInnerQuantity
        dictQuantity["arrquantiy"] = arrQuntity
        arrOfProductList[collectionView.tag] = dictQuantity       
        
        let indexPath = NSIndexPath(row: collectionView.tag, section: 0)
        let tblcell : ProductListCell? = self.tblProductList.cellForRow(at: indexPath as IndexPath) as! ProductListCell?
        if let item = tblcell?.collectionOfQuantity
        {
            item.reloadData()
        }        
    }
    
    //MARK:- Service
    
    @objc func getResetList(notification:Notification)
    {
        dictSelectedData = JSON()
        getProductList(strParentId: notification.object as? String ?? "")
    }
    
    @objc func getProductList(strParentId:String)
    {
        let kRegiURL = "\(AddhirhamMainURL)categoryProduct.php"
        
        var dictUser:JSON = getLoggedUserDetail()

        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param:[String:Any] = [:]
            
            if self.isComeFromCategory == false{
                
                if self.isComeFromViewAll == true
                {
                    param = ["hashkey" :HashKey,
                             "cate_id" :"2",
                             "p_cate_id":strParentId,
                             "cust_id":dictUser["customerId"].stringValue] as [String : Any]
                }
                else
                {
                    param = ["hashkey" :HashKey,
                             "cate_id" :dict?["id"].stringValue ?? "",
                             "p_cate_id":strParentId,
                             "cust_id":dictUser["customerId"].stringValue] as [String : Any]
                }
               
            }
            else{
                param = ["hashkey" :HashKey,
                         "cate_id" :strParentId,
                         "p_cate_id":dict?["id"].stringValue ?? "",
                         "cust_id":dictUser["customerId"].stringValue] as [String : Any]
            }
            
            print("parm \(param)")           
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                   // print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        
                        self.arrOfProductList = []
                        self.arrOfProductList = json["categoryProduct"].arrayValue
                        for i in 0..<self.arrOfProductList.count
                        {
                            var dict = self.arrOfProductList[i]
                            let arrQnt = self.SetupQuantityArray(qnt:dict["qty"].intValue)
                            dict["arrquantiy"] = JSON(arrQnt)
                            dict["current_index"] = "4"
                            dict["is_compare"] = "0"
                            for i in 0..<strComapareID.count
                            {
                                if strComapareID[i] == dict["product_id"].stringValue
                                {
                                    dict["is_compare"] = "1"
                                    break
                                }
                            }
                            self.arrOfProductList[i] = dict
                        }
                        
                        print("arrOfProductList:- \(self.arrOfProductList)")
                        self.tblProductList.reloadData()
                        self.dictFilter = json["filter"]
                    }
                    else
                    {
                        self.arrOfProductList = []
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblProductList.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func addToCartProduct(strProductID:String,strQuantiy:String,type:Int)
    {
        let kRegiURL = "\(AddhirhamMainURL)addtoCart.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "prodid" :strProductID,
                         "custid":dictLoggedUser["customerId"].stringValue,
                         "prodqty":strQuantiy,
                         "quoteId":getCartDetail("quote_id")] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        guard let rowdata = try? json.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "cartDetail")
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                        strTotalCartItem = json["count"].stringValue
                        Defaults.setValue(strTotalCartItem, forKey: "CartItem")
                        self.lblTotalCount.text = strTotalCartItem
                        Defaults.synchronize()
                        if type == 2
                        {
                             self.goToRootOfTab(index: 2)
                        }                        
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func addToWishList(strProductID:String,strCustId:String)
    {
        let kRegiURL = "\(AddhirhamMainURL)addToWhishlist.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "product_id" :strProductID,
                         "custid":strCustId] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.refresh()
                        KSToastView.ks_showToast(json["AddToWhishlist"]["sucess"].stringValue, duration: ToastDuration)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func getSearchProductList()
    {
        let kRegiURL = "\(AddhirhamMainURL)productSearch.php"
        
        var dictUser:JSON = getLoggedUserDetail()

        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "name" :self.txtSearchProduct.text ?? "",
                         "custid":dictUser["customerId"].stringValue,
                         "parent_cid" :strFilterCategoryId.joined(separator: ","),
                         "color":strFilterColorId.joined(separator: ","),
                         "max_price":strFilterMaxPrice,
                         "min_price":strFilterMinPrice] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        
                        self.arrOfProductList = []
                        self.arrOfProductList = json["categoryProduct"].arrayValue
                        for i in 0..<self.arrOfProductList.count
                        {
                            var dict = self.arrOfProductList[i]
                            let arrQnt = self.SetupQuantityArray(qnt:dict["qty"].intValue)
                            dict["arrquantiy"] = JSON(arrQnt)
                            dict["current_index"] = 4
                            for i in 0..<strComapareID.count
                            {
                                if strComapareID[i] == dict["product_id"].stringValue
                                {
                                    dict["is_compare"] = "1"
                                    break
                                }
                            }
                            self.arrOfProductList[i] = dict
                        }
                        // print("arrOfProductList:- \(self.arrOfProductList)")
                        self.tblProductList.reloadData()
                      //  self.arrFilter = []
                        self.dictFilter = json["filter"]
                    }
                    else
                    {
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblProductList.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    @objc func getFilterList(notification:Notification)
    {
        let kRegiURL = "\(AddhirhamMainURL)filterProduct.php"
        
         var dictUser:JSON = getLoggedUserDetail()
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                       //  "name":notification.object as? String ?? "",
                         "parent_cid":notification.object as? String ?? "",
                         "sub_cid" :strFilterCategoryId.joined(separator: ","),
                         "color":strFilterColorId.joined(separator: ","),
                         "max_price":strFilterMaxPrice,
                         "min_price":strFilterMinPrice] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.arrOfProductList = []
                        self.arrOfProductList = json["categoryProduct"].arrayValue
                        for i in 0..<self.arrOfProductList.count
                        {
                            var dict = self.arrOfProductList[i]
                            let arrQnt = self.SetupQuantityArray(qnt:dict["qty"].intValue)
                            dict["arrquantiy"] = JSON(arrQnt)
                            dict["current_index"] = 4
                            for i in 0..<strComapareID.count
                            {
                                if strComapareID[i] == dict["product_id"].stringValue
                                {
                                    dict["is_compare"] = "1"
                                    break
                                }
                            }
                            self.arrOfProductList[i] = dict
                        }
                        // print("arrOfProductList:- \(self.arrOfProductList)")
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblProductList.reloadData()
                        self.dictSelectedData = json["select_data"]
                        self.removeFilterData()
                    }
                    else
                    {
                        self.arrOfProductList = []
                        self.strNoRecordMessage = json["message"].stringValue
                        self.tblProductList.reloadData()
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}



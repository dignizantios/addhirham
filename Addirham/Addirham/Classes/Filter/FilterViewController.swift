//
//  FilterViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 26/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import RangeSeekSlider
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import NVActivityIndicatorView
import KSToastView

class FilterViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,RangeSeekSliderDelegate,NVActivityIndicatorViewable
{
    //MARK:- Variable Declaration
    
    var arrOfColor:[JSON] = []
    var arrOfCategory:[JSON] = []
  //  var arrFilter:[JSON] = []
    var dictOfFilter:JSON!
    var strComeFromScreen = String()
    var dictSelectedData:JSON!
    var strCatId = String()
    var isComeFromClose = Bool()
    var strCategoryId = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var collectionOfCategory: UICollectionView!
    @IBOutlet weak var heightOfCategoryCollection: NSLayoutConstraint!
    @IBOutlet var leadingOfFilter: NSLayoutConstraint!
    @IBOutlet var heightOfCollectionColor: NSLayoutConstraint!
    @IBOutlet var collectionOfColor: UICollectionView!
    @IBOutlet var priceRangeSlider: RangeSeekSlider!
    @IBOutlet var lblMinPrice: UILabel!
    @IBOutlet var lblMaxPrice: UILabel!
    @IBOutlet weak var heightOfCategoryView: NSLayoutConstraint!
    @IBOutlet weak var heightOfColorView: NSLayoutConstraint!
    @IBOutlet weak var heightOfPriceView: NSLayoutConstraint!
    @IBOutlet weak var viewOfCategory: UIView!
    @IBOutlet weak var viewOfColor: UIView!
    @IBOutlet weak var viewOfPrice: UIView!
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.leadingOfFilter.constant = self.view.frame.width
        self.priceRangeSlider.delegate = self
       
        resetFilter()
      
      //  NotificationCenter.default.addObserver(self, selector: #selector(dissmisFiltermenu), name: NSNotification.Name(rawValue: "HideFilterMenu"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        collectionOfColor.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        collectionOfCategory.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        dissmisViewAnimation(interval: 0.4,userInfo:"bye")
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {        super.viewDidAppear(animated)
        presentViewAnimation(interval: 0.4)
    }
    
    //MARK:- Private Method
    
    func presentViewAnimation(interval:TimeInterval)
    {
        UIView.animate(withDuration: interval)
        {
            self.leadingOfFilter.constant = 0
            self.view.layoutIfNeeded()
        }
        
    }
    func dissmisViewAnimation(interval:TimeInterval,userInfo:String)
    {
        
        UIView.animate(withDuration: interval, animations: {
            self.leadingOfFilter.constant = self.view.frame.width
            self.view.layoutIfNeeded()
        })
        { (completion) in
            if userInfo != "bye"
            {
                self.collectionOfColor.removeObserver(self, forKeyPath: "contentSize")
                self.collectionOfCategory.removeObserver(self, forKeyPath: "contentSize")
                
                if self.strComeFromScreen == "Product"
                {
                    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ProductListFilter"), object: nil)
                    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ResetProductListFilter"), object: nil)
                }
                else if self.strComeFromScreen == "Search"
                {
                    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SearchListFilter"), object: nil)
                    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ResetSearchListFilter"), object: nil)
                }
                else
                {
                    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "TopListFilter"), object: nil)
                    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ResetTopListFilter"), object: nil)
                }
                 self.dismiss(animated: false, completion: nil)
            }
            else{
                if self.isComeFromClose == false
                {
                    self.collectionOfColor.removeObserver(self, forKeyPath: "contentSize")
                    self.collectionOfCategory.removeObserver(self, forKeyPath: "contentSize")
                }
                 self.dismiss(animated: false, completion: nil)
            }
           
            
        }
        
    }

    func resetFilter()
    {
        self.priceRangeSlider.minValue = CGFloat(dictOfFilter["price"]["minprice"].floatValue)
        self.priceRangeSlider.maxValue = CGFloat(dictOfFilter["price"]["maxprice"].floatValue)
        
        arrOfCategory = dictOfFilter["category"].arrayValue
        arrOfColor = dictOfFilter["color"].arrayValue
        
        if arrOfCategory.count == 0
        {
            self.heightOfCategoryView.constant = 0
            self.viewOfCategory.isHidden = true
        }
        
        if arrOfColor.count == 0
        {
            self.heightOfColorView.constant = 0
            self.viewOfPrice.isHidden = true
        }
        
        
        if dictSelectedData.count == 0
        {
            self.priceRangeSlider.selectedMinValue = CGFloat(dictOfFilter["price"]["minprice"].floatValue)
            self.priceRangeSlider.selectedMaxValue = CGFloat(dictOfFilter["price"]["maxprice"].floatValue)
            
            self.lblMinPrice.text = "OMR \(dictOfFilter["price"]["minprice"].stringValue)"
            self.lblMaxPrice.text = "OMR \(dictOfFilter["price"]["maxprice"].stringValue)"
            for i in 0..<arrOfCategory.count
            {
                var dict = arrOfCategory[i]
                dict["selected"] = "0"
                arrOfCategory[i] = dict
            }
            
            for i in 0..<arrOfColor.count
            {
                var dict = arrOfColor[i]
                dict["selected"] = "0"
                arrOfColor[i] = dict
            }
        }
        else
        {
            print("dictSelectedData \(dictSelectedData)")
            print("dictOfFilter \(dictOfFilter)")
            
            if (dictSelectedData["minprice"].stringValue).trimmingCharacters(in: .whitespaces) == ""
            {
                self.priceRangeSlider.selectedMinValue = CGFloat(dictOfFilter["price"]["minprice"].floatValue)
                self.lblMinPrice.text = "OMR \(dictOfFilter["price"]["minprice"].stringValue)"
            }
            else
            {
                self.priceRangeSlider.selectedMinValue = CGFloat(dictSelectedData["minprice"].floatValue)
                self.lblMinPrice.text = "OMR \(dictSelectedData["minprice"].stringValue)"
            }
            
            if (dictSelectedData["maxprice"].stringValue).trimmingCharacters(in: .whitespaces) == ""
            {
                self.priceRangeSlider.selectedMaxValue = CGFloat(dictOfFilter["price"]["maxprice"].floatValue)
                self.lblMaxPrice.text = "OMR \(dictOfFilter["price"]["maxprice"].stringValue)"
            }
            else
            {
                self.priceRangeSlider.selectedMaxValue = CGFloat(dictSelectedData["maxprice"].floatValue)
                self.lblMaxPrice.text = "OMR \(dictSelectedData["maxprice"].stringValue)"
            }
            
            var arrSelectedId:[JSON] = []
            arrSelectedId = dictSelectedData["sub_cat_id"].arrayValue
            for i in 0..<arrOfCategory.count
            {
                var dict = arrOfCategory[i]
                for j in 0..<arrSelectedId.count
                {
                    if dict["catid"].stringValue == arrSelectedId[j].stringValue
                    {
                        dict["selected"] = "1"
                        break
                    }
                    else
                    {
                        dict["selected"] = "0"
                    }
                }
                arrOfCategory[i] = dict
            }
            print("arrOfCategory \(arrOfCategory)")
            var arrSelectedColor:[JSON] = []
            arrSelectedColor = dictSelectedData["hexa_code"].arrayValue
            for i in 0..<arrOfColor.count
            {
                var dict = arrOfColor[i]
                for j in 0..<arrSelectedColor.count
                {
                    if dict["value"].stringValue == arrSelectedColor[j].stringValue
                    {
                        dict["selected"] = "1"
                        break
                    }
                    else
                    {
                        dict["selected"] = "0"
                    }
                }
                arrOfColor[i] = dict
            }
        }
       
        self.priceRangeSlider.layoutIfNeeded()
        self.priceRangeSlider.layoutSubviews()
       
        
        self.collectionOfCategory.reloadData()
        self.collectionOfColor.reloadData()
    }

    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UICollectionView {
                print("contentSize:= \(collectionOfColor.contentSize.height)")
                self.heightOfCollectionColor.constant = collectionOfColor.contentSize.height
                print("contentSize:= \(collectionOfCategory.contentSize.height)")
                self.heightOfCategoryCollection.constant = collectionOfCategory.contentSize.height
        }
    }
  
    //MARK:- Hexcolor to RGB
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //MARK: - RangeSeekSlider delegate
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, stringForMaxValue maxValue: CGFloat) -> String?
    {
        print(maxValue)
        self.lblMaxPrice.text = "OMR \(String(format: "%.00f", Float(maxValue)))"
        return String(format: "OMR %.0f", Float(maxValue))
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, stringForMinValue minValue: CGFloat) -> String? {
       // print("slider.tag \(slider.tag)")
        
        self.lblMinPrice.text = "OMR \(String(format: "%.00f", Float(minValue)))"
        return String(format: "OMR %.0f", Float(minValue))
    }
    
    //MARK:- Action Zone
  
    @IBAction func btnResetAction(_ sender: Any)
    {
      //  resetFilter()
        isComeFromClose = true
        
        dictSelectedData = JSON()
        removeFilterData()
        if strComeFromScreen == "Product"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ResetProductListFilter"), object: strCategoryId)
        }
        else if strComeFromScreen == "Search"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ResetSearchListFilter"), object: strCategoryId)
        }
        else
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ResetTopListFilter"), object: strCategoryId)
        }
        dissmisViewAnimation(interval: 0.4,userInfo:"gone")
    }
    
    @IBAction func btnApplyAction(_ sender: UIButton)
    {
        isComeFromClose = true
        strFilterCategoryId = [String]()
        strFilterColorId = [String]()
        for i in 0..<arrOfCategory.count
        {
            var dict = arrOfCategory[i]
            if dict["selected"].stringValue == "1"
            {
                strFilterCategoryId.append(dict["catid"].stringValue)
            }
        }
        
        for i in 0..<arrOfColor.count
        {
            var dict = arrOfColor[i]
            if dict["selected"].stringValue == "1"
            {
                strFilterColorId.append(dict["value"].stringValue )
            }
        }
        
       /* if strFilterCategoryId.count != 0
        {
            strFilterCategoryId.remove(at: 0)
        }
        
        if strFilterColorId.count != 0
        {
            strFilterColorId.remove(at: 0)
        }*/
        strFilterMaxPrice = String(describing: self.priceRangeSlider.selectedMaxValue)
        strFilterMinPrice = String(describing: self.priceRangeSlider.selectedMinValue)
        
        print("strFilterMaxPrice \(strFilterMaxPrice)")
        print("strFilterMinPrice \(strFilterMinPrice)")
        print("strFilterColorId \(strFilterColorId)")
        print("strFilterCategoryId \(strFilterCategoryId)")
        

        if strComeFromScreen == "Product"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ProductListFilter"), object: strCategoryId)
        }
        else if strComeFromScreen == "Search"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SearchListFilter"), object: strCategoryId)
        }
        else
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TopListFilter"), object: nil)
        }
        
         dissmisViewAnimation(interval: 0.4,userInfo:"gone")
    }
    
    @IBAction func btnClearDataAction(_ sender: Any) {
        dictSelectedData = JSON()
        removeFilterData()
        resetFilter()
    }
    @IBAction func btnHideFilterAction(_ sender: Any)
    {
        isComeFromClose = true
        dissmisViewAnimation(interval: 0.4,userInfo:"gone")
    }
    //MARK:- Collectionview Delegate Method
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == collectionOfColor
        {
            return arrOfColor.count
        }
        return arrOfCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == collectionOfColor{
            let cell:FilterColorCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterColorCell", for: indexPath) as! FilterColorCell
            let dict = arrOfColor[indexPath.row]
            cell.viewOfColor.backgroundColor = hexStringToUIColor(hex: dict["hexa_code"].stringValue)
            
            if dict["selected"].stringValue == "1"
            {
                cell.imgOfColor.isHidden = false
            }
            else
            {
                cell.imgOfColor.isHidden = true
            }
            return cell
        }
        
        let cell:FilterCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCategoryCell", for: indexPath) as! FilterCategoryCell
        let dict = arrOfCategory[indexPath.row]
        cell.lblCategoryName.text = dict["catName"].stringValue
        if dict["selected"].stringValue == "1"
        {
            cell.lblCategoryName.textColor = UIColor.white
            cell.lblCategoryName.backgroundColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1.0)
        }
        else
        {
            cell.lblCategoryName.textColor = UIColor.init(red: 90/255, green: 43/255, blue: 123/255, alpha: 1.0)
            cell.lblCategoryName.backgroundColor = UIColor.white
        }
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == collectionOfColor
        {
           // let width = collectionView.frame.size.width/5
            //let height = collectionView.frame.size.width/5
            return CGSize(width: 45, height: 45)
        }
        let width = (collectionView.frame.size.width - 10)/2
        return CGSize(width: width, height: 40)
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == collectionOfColor
        {
           /* for i in 0..<arrOfColor.count
            {
                var dict = arrOfColor[i]
                dict["selected"] = "0"
                arrOfColor[i] = dict
            }*/
            var dict = arrOfColor[indexPath.row]
            if dict["selected"].stringValue == "1"
            {
                dict["selected"].stringValue = "0"
            }
            else
            {
                dict["selected"].stringValue = "1"
            }
            arrOfColor[indexPath.row] = dict
            self.collectionOfColor.reloadData()
        }
        else
        {
           /* for i in 0..<arrOfCategory.count
            {
                var dict = arrOfCategory[i]
                dict["selected"] = "0"
                arrOfCategory[i] = dict
            }*/
            var dict = arrOfCategory[indexPath.row]
            if dict["selected"].stringValue == "1"
            {
                dict["selected"].stringValue = "0"
            }
            else
            {
                dict["selected"].stringValue = "1"
            }
            arrOfCategory[indexPath.row] = dict
            self.collectionOfCategory.reloadData()
        }
    }
    
}

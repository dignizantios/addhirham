//
//  TabBarController.swift
//  Addirham
//
//  Created by Haresh on 11/14/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON


class TabBarController: UITabBarController,UITabBarControllerDelegate
{

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.delegate = self
        self.delegate = self
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let view = storyboard.instantiateViewController(withIdentifier: "ViewController")
        let wishList = storyboard.instantiateViewController(withIdentifier: "WishListViewController")
        let myCart = storyboard.instantiateViewController(withIdentifier: "MyCartViewController")
        let Profile = storyboard.instantiateViewController(withIdentifier: "ProfileViewController")
        let topDeals = storyboard.instantiateViewController(withIdentifier: "TopDealViewController")
        
        let navi1 = UINavigationController(rootViewController: view)
        let navi2 = UINavigationController(rootViewController: topDeals)
        let navi3 = UINavigationController(rootViewController: myCart)
        let navi4 = UINavigationController(rootViewController: wishList)
        let navi5 = UINavigationController(rootViewController: Profile)
        
        navi1.isNavigationBarHidden = true
        navi2.isNavigationBarHidden = true
        navi3.isNavigationBarHidden = true
        navi4.isNavigationBarHidden = true
        navi5.isNavigationBarHidden = true
        
        /*navi1.hidesBottomBarWhenPushed = true
         navi2.hidesBottomBarWhenPushed = true
         navi3.hidesBottomBarWhenPushed = true
         navi4.hidesBottomBarWhenPushed = true
         navi5.hidesBottomBarWhenPushed = true*/
        
        navi1.tabBarItem.title = "Home"
        navi2.tabBarItem.title = "Hot Deals"
        navi3.tabBarItem.title = "My Cart"
        navi4.tabBarItem.title = "WishList"
        navi5.tabBarItem.title = "Profile"
        
        navi1.tabBarItem.image = UIImage(named: "ic_bottom_tab_home_unselected.png")?.withRenderingMode(.alwaysOriginal)
        navi2.tabBarItem.image = UIImage(named: "ic_bottom_tab_hot_deals_unselected.png")?.withRenderingMode(.alwaysOriginal)
        navi3.tabBarItem.image = UIImage(named: "ic_bottom_tab_my_cart_unselected.png")?.withRenderingMode(.alwaysOriginal)
        navi4.tabBarItem.image = UIImage(named: "ic_bottom_tab_favorite_unselected.png")?.withRenderingMode(.alwaysOriginal)
        navi5.tabBarItem.image = UIImage(named: "ic_bottom_tab_profile_unselected.png")?.withRenderingMode(.alwaysOriginal)
        
        navi1.tabBarItem.selectedImage = UIImage(named: "ic_bottom_tab_home_selected.png")
        navi2.tabBarItem.selectedImage = UIImage(named: "ic_bottom_tab_hot_deals_selected.png")
        navi3.tabBarItem.selectedImage = UIImage(named: "ic_bottom_tab_my_cart_selected.png")
        navi4.tabBarItem.selectedImage = UIImage(named: "ic_bottom_tab_favorite_selected.png")
        navi5.tabBarItem.selectedImage = UIImage(named: "ic_bottom_tab_profile_selected.png")
        
        
        self.viewControllers = [navi1, navi2,navi3, navi4,navi5]
        
        tabBarController?.tabBar.tintColor = UIColor(red: 90/255.0, green: 43/255.0, blue:123/255.0, alpha: 1.0)
       /* var tabFrame = self.tabBarController?.tabBar.frame
        //self.TabBar is IBOutlet of your TabBar
        tabFrame?.size.height = 48
        tabFrame?.origin.y = UIScreen.main.bounds.size.height - 48
        
        // self.tabBarController.tabBar.backgroundColor = UIColor.red
        self.tabBarController?.tabBar.frame = tabFrame!
        self.tabBarController?.tabBar.autoresizesSubviews = false
        self.tabBarController?.tabBar.clipsToBounds = true*/

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
       
    }
    
    //MARK:- Tababar Delegate Method
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
    {
        // print("viewController \(viewController)")
     //   NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HideSideMenu"), object: nil)
     //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HideFilterMenu"), object: nil)
        print("Privious index:=\(tabBarController.selectedIndex)")
        if let vc = viewController as? UINavigationController
        {
            vc.popToRootViewController(animated: false)
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool
    {
        
        guard
            let tab = tabBarController.viewControllers?.index(of: viewController), [3, 4].contains(tab)
            else {
                if let vc = tabBarController.viewControllers?[tabBarController.selectedIndex] as? UINavigationController {
                    vc.popToRootViewController(animated: false)
                }
                return true
        }
                var dict:JSON = getLoggedUserDetail()
                if dict["customerId"].stringValue == ""
                {
                    let alertController = UIAlertController(title: AppName, message: "You are not access this feature because you are not login!!", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Login", style: .default) { (action) in
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let obj = storyboard.instantiateViewController(withIdentifier: "ShippingAddressViewController") as! ShippingAddressViewController
                    //    obj.modalPresentationStyle = .overCurrentContext
                     //   obj.modalTransitionStyle = .crossDissolve
                        self.present(obj, animated: true, completion: nil)
                    }
                    alertController.addAction(okAction)
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true)
                    return false
                }
                else
                {
                    if let vc = tabBarController.viewControllers?[tabBarController.selectedIndex] as? UINavigationController {
                        vc.popToRootViewController(animated: false)
                    }
                    return true
                }
        
             //return false
        
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        /*if self.tabBarController?.selectedIndex == 4
        {
         
        }*/
    }

   

}

//
//  PaymentViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 28/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView

class PaymentViewController: UIViewController {
    
    //MARK:- variaqble Declaration
    
    var fltShippingCharge = CGFloat()
    var arrPayment:[JSON] = []
    var strMessage = String()
    var isComeFromGuestUser = Bool()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnCreditCardPaymentOutlet: UIButton!
    @IBOutlet var btnCashOnDeliveryOutlet: UIButton!
    @IBOutlet var txtNameCard: UITextField!
    @IBOutlet var txtCardNumber: UITextField!
    @IBOutlet var txtCardExpireYear: UITextField!
    @IBOutlet var txtCardExpireMonth: UITextField!
    @IBOutlet var btn_SideMenu: UIButton!
    @IBOutlet weak var heightOfTblPayment: NSLayoutConstraint!
    @IBOutlet weak var tblPayment: UITableView!
    @IBOutlet weak var btnNextStepOutlet: CustomButton!
    
    
    //MARK:- View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        self.btnCashOnDeliveryOutlet.isSelected = true
        getPaymentMethodList()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblPayment.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tblPayment.removeObserver(self, forKeyPath: "contentSize")
        
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblPayment.contentSize.height)")
            if tblPayment.contentSize.height == 0
            {
                self.heightOfTblPayment.constant = 40
            }
            else
            {
                self.heightOfTblPayment.constant = tblPayment.contentSize.height
            }
        }
    }
    
    //MARK:- Action Zone

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCashOnDeliveryAction(_ sender: UIButton) {
        self.btnCashOnDeliveryOutlet.isSelected  = true
        self.btnCreditCardPaymentOutlet.isSelected = false
    }
    @IBAction func btnNextStepAction(_ sender: Any) {
        var isSelectedCharge = Bool()
        var dictPayment:JSON?
        for i in 0..<arrPayment.count
        {
            var dict = arrPayment[i]
            if dict["selected"].stringValue == "1"
            {
                dictPayment = dict
                isSelectedCharge = true
                break
            }
        }
        if isSelectedCharge == false
        {
            KSToastView.ks_showToast("Please select at least one charge", duration: ToastDuration)
        }
        else
        {
            let method = self.storyboard?.instantiateViewController(withIdentifier: "OverViewViewController") as! OverViewViewController
            method.fltShippingCharge = fltShippingCharge
            method.dictPaymentOverview = dictPayment
            if dictPayment?["code"].stringValue == "mygateway"
            {
                method.isSelectedCreditCard = true
            }
            else
            {
                method.isSelectedCreditCard = false
            }
            method.isComeFromGuestUser = isComeFromGuestUser
            self.navigationController?.pushViewController(method, animated: true)
        }
       
    }
    @IBAction func btnCreditCatdAction(_ sender: UIButton) {
        self.btnCashOnDeliveryOutlet.isSelected  = false
        self.btnCreditCardPaymentOutlet.isSelected = true
    }
    @IBAction func btnCheckedPayAction(_ sender: UIButton) {
        for i in 0..<arrPayment.count
        {
            var dict = arrPayment[i]
            dict["selected"] = "0"
            arrPayment[i] = dict
        }
        var dict = arrPayment[sender.tag]
        dict["selected"] = "1"
        arrPayment[sender.tag] = dict
        self.tblPayment.reloadData()
    }
}


//MARK:- TableView Delegate

extension PaymentViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrPayment.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrPayment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "PaymentCell") as! PaymentCell
        let dict = arrPayment[indexPath.row]
        cell.lblPaymentMethod.text = dict["title"].stringValue
        cell.btnCheckedOutlet.tag = indexPath.row
        if dict["selected"].stringValue == "1"
        {
            cell.btnCheckedOutlet.isSelected = true
        }
        else
        {
            cell.btnCheckedOutlet.isSelected = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<arrPayment.count
        {
            var dict = arrPayment[i]
            dict["selected"] = "0"
            arrPayment[i] = dict
        }
        var dict = arrPayment[indexPath.row]
        dict["selected"] = "1"
        arrPayment[indexPath.row] = dict
        self.tblPayment.reloadData()
    }
}

 //MARK:- Service


extension PaymentViewController:NVActivityIndicatorViewable
{
    func getPaymentMethodList()
    {
        let kRegiURL = "\(AddhirhamMainURL)paymentMethodelist.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param:[String:String] = [:]
            if isComeFromGuestUser == true
            {
                param = ["hashkey":HashKey,
                         "country_code":dictGuestUserAddress["country_id"].stringValue]
            }
            else
            {
                param = ["hashkey":HashKey,
                         "country_code":getUserAddressDetail("country_id")]
            }
            
            print("parm \(param)")
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.arrPayment = []
                        self.arrPayment = json["paymentMethode"].arrayValue
                        for i in 0..<self.arrPayment.count
                        {
                            var dict = self.arrPayment[i]
                            dict["selected"] = "0"
                            self.arrPayment[i] = dict
                        }
                        self.tblPayment.reloadData()
                        
                    }
                    else
                    {
                        self.btnNextStepOutlet.isHidden = true
                        self.arrPayment = []
                        self.strMessage = json["paymentMethode"].stringValue
                        self.tblPayment.reloadData()                        
                        KSToastView.ks_showToast(json["paymentMethode"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}

//
//  ViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 17/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KSToastView
import DropDown

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,NVActivityIndicatorViewable,UITextFieldDelegate,CommonDropDownDelegate
{
    //MARK:- Variable Declaration
    
    var arrOfLatestProduct:[JSON] = []
    var arrOfCategory:[JSON] = []
    var arrOfSlider:[JSON] = []
    var arrOfAds:[JSON] = []
    var sortingDelegate : CommonDropDownDelegate?
    var shortDD = DropDown()
    var timer = Timer()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnFilterOutlet: UIButton!
    @IBOutlet var txtSearch: CustomTextField!
    @IBOutlet var tblHomeCategory: UITableView!
    @IBOutlet var scrlOfInner: UIScrollView!
    @IBOutlet var pageControllerInner: UIPageControl!
    @IBOutlet var collectionOfCategory: UICollectionView!
    @IBOutlet var collectionOfAds: UICollectionView!
    
    @IBOutlet weak var lblTotalCount: UILabel!
    @IBOutlet var btn_SideMenu: UIButton!
    
    //MARK:- ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.sortingDelegate = self
        printFonts()
        self.navigationController?.isNavigationBarHidden = true
        btn_SideMenu.addTarget(self, action: #selector(sideMenuAction(_:)), for: .touchUpInside)
        txtSearch.delegate  = self
        var array = [String]()
        array = ["Rate App","Share App"]
        configDropDown(dropdown: shortDD, sender: btnFilterOutlet,width: 150)
        dropDownDataSource(dropdown: shortDD,delegate : sortingDelegate!, array: array,type:2)
        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateTableHeader"), object: nil)
       
        // Do any additional setup after loading the view.
    }
   
    override func viewWillAppear(_ animated: Bool)
    {
        appDelagte.tabBarController.tabBar.isHidden = false
        self.lblTotalCount.text = strTotalCartItem
        getHomeList()
       // setSelectedTab()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    deinit {
        appDelagte.tabBarController = tabBarController!
    }

    //MARK: - Delegate method
    
    func setSelectedSortValue(type: String, index: Int,array:[String],arrType:Int) {
        
        print("type - ",type)
        print("index - ",index)
        if index == 1
        {
            let strShare = strAppStoreLink
            let activityVC = UIActivityViewController(activityItems: [strShare], applicationActivities: nil)
            self.present(activityVC, animated: true, completion: nil)
        }
        else
        {
           // UIApplication.sharedApplication.openURL(NSURL(string: "itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4")! as URL)
            UIApplication.shared.open(URL(string: strAppStoreLink)!, options: [:], completionHandler: nil)
            
        }
    }
    
    
     //MARK:- Textfield Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(obj, animated: true)
        return false
    }
    
    //MARK:- Setup Quantity Array
    
    func SetupQuantityArray(qnt:Int) ->  [JSON]
    {
        var arrOfQuantity:[JSON] = []
        for i in 0..<qnt
        {
            var dict = [String:String]()
            dict["quanity_name"] = String(i+1)
            dict["selected"] = "0"
            arrOfQuantity.append(JSON(dict))
        }
        return arrOfQuantity
    }
    
    //MARK:- Private Zone
  
    
    func setUpImageSlider()
    {
        self.pageControllerInner.currentPage = 0
        self.pageControllerInner.numberOfPages = arrOfSlider.count
        
        self.scrlOfInner.frame.size.width = UIScreen.main.bounds.size.width
        var x = CGFloat()
        x = 0
        
        for i in 0..<arrOfSlider.count
        {
            let imgOfPromotion = UIImageView()
            imgOfPromotion.contentMode = .scaleToFill
            imgOfPromotion.clipsToBounds = true
            
            imgOfPromotion.frame = CGRect(x: x, y: 0, width:self.scrlOfInner.frame.size.width, height: self.scrlOfInner.frame.height)
            print("ScrollPreviewFrame :- \(self.scrlOfInner.frame.size.width)")
            print("ImagePreviewFrame :- \(imgOfPromotion.frame)")
            x += self.scrlOfInner.frame.size.width
            let dict = arrOfSlider[i]
            let strImage = dict["url"].stringValue
            let urlImage:URL = URL(string: strImage)!
            let strPlaceImage = "placeholder_banner_home_screen"
            imgOfPromotion.sd_setImage(with: urlImage as URL, placeholderImage:UIImage(named:strPlaceImage))
          //  imgOfPromotion.tag = i
          //  let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
          //  tapGestureRecognizer.numberOfTapsRequired = 1
          //  imgOfPromotion.isUserInteractionEnabled = true
          //  imgOfPromotion.addGestureRecognizer(tapGestureRecognizer)
            self.scrlOfInner.addSubview(imgOfPromotion)
        }
        
        self.scrlOfInner.contentSize = CGSize(width:x, height: scrlOfInner.frame.size.height)
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    
    @objc func update()
    {
        var scrollxpos = scrlOfInner.contentOffset.x
        
        var visibleRect = CGRect()
        
        if scrollxpos + scrlOfInner.frame.size.width == scrlOfInner.contentSize.width
        {
            scrollxpos = 0
            visibleRect = CGRect(x: scrollxpos, y: scrlOfInner.frame.origin.y, width: scrlOfInner.frame.size.width, height: scrlOfInner.frame.size.height)
            let pageNumber = round(scrollxpos / scrlOfInner.frame.size.width)
            pageControllerInner.currentPage = Int(pageNumber)
        }
        else
        {
            visibleRect = CGRect(x: (scrollxpos +  scrlOfInner.frame.size.width), y: scrlOfInner.frame.origin.y, width: scrlOfInner.frame.size.width, height: scrlOfInner.frame.size.height)
            let pageNumber = round((scrollxpos +  scrlOfInner.frame.size.width)/scrlOfInner.frame.size.width)
            pageControllerInner.currentPage = Int(pageNumber)
        }
        
        scrlOfInner.scrollRectToVisible(visibleRect, animated: true)
        
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnFilterAction(_ sender: UIButton) {
        showDropDown(dropDown: shortDD, sender: sender)
    }    
   
    @IBAction func btnCartAction(_ sender: UIButton)
    {
        goToRootOfTab(index: 2)
    }
    
    @IBAction func btnViewAllAction(_ sender: Any)
    {
        let method = self.storyboard?.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        method.isComeFromCategory = false
        method.isComeFromViewAll = true
      //  method.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(method, animated: true)
    }
    //MARK:- TableView Delagate Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfLatestProduct.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:HomePoductCell = self.tblHomeCategory.dequeueReusableCell(withIdentifier: "HomePoductCell") as! HomePoductCell
        if indexPath.row == 0
        {
            cell.btnViewAllOutlet.isHidden = false
        }
        else{
             cell.btnViewAllOutlet.isHidden = true
        }
        let dict = arrOfLatestProduct[indexPath.row]
        cell.lblTitleName.text = dict["name"].stringValue
        cell.collectionOfLatestPoduct.tag = indexPath.row
        cell.collectionOfLatestPoduct.delegate = self
        cell.collectionOfLatestPoduct.dataSource = self
        cell.collectionOfLatestPoduct.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK:- Collection View Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionOfCategory
        {
            return arrOfCategory.count
        }
        else if  collectionView == collectionOfAds
        {
            return arrOfAds.count
        }
        else
        {
            return (arrOfLatestProduct[collectionView.tag]["arr_product"].arrayValue).count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionOfCategory
        {
            let cell:HomeCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCategoryCell", for: indexPath) as! HomeCategoryCell
            let dict = arrOfCategory[indexPath.row]
            let strImage = dict["image"].url
            let strPlaceImage = "ic_category_all_home_page.png"
            cell.imgOfCategory.sd_setImage(with:strImage, placeholderImage:UIImage(named:strPlaceImage))
            cell.lblCategoryName.text = dict["name"].stringValue
            return cell
        }
        else if  collectionView == collectionOfAds
        {
            let cell:HomeAdsCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeAdsCell", for: indexPath) as! HomeAdsCell
            let dict = arrOfAds[indexPath.row]
            let strImage = dict["url"].stringValue
            let urlImage:URL = URL(string: strImage)!
            let strPlaceImage = "placeholder_banner_home_screen"
            cell.imgOfAds.sd_setImage(with: urlImage as URL, placeholderImage:UIImage(named:strPlaceImage))
            return cell
        }
        else
        {
            let cell:HomeLatestProductCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeLatestProductCell", for: indexPath) as! HomeLatestProductCell
            let dict = (arrOfLatestProduct[collectionView.tag]["arr_product"].arrayValue)[indexPath.row]
            if collectionView.tag == 0
            {
                let strImage = dict["image"].stringValue
                let urlImage:URL = URL(string: strImage)!
                cell.imgOfProduct.sd_setImage(with: urlImage as URL, placeholderImage:#imageLiteral(resourceName: "phone"))
                cell.lblProductName.text = dict["name"].stringValue
                cell.lblProductPrice.text = "OMR \(dict["regular_price"].stringValue)"
            }
            else
            {
                let strImage = dict["image"].stringValue
                let urlImage:URL = URL(string: strImage)!
                cell.imgOfProduct.sd_setImage(with: urlImage as URL, placeholderImage:#imageLiteral(resourceName: "phone"))
                cell.lblProductName.text = dict["name"].stringValue
                cell.lblProductPrice.text = "OMR \(dict["price"].stringValue)"
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionOfCategory
        {
           // let width = self.collectionOfCategory.frame.size.width/CGFloat(arrOfCategory.count)
          //  let height = self.collectionOfCategory.frame.size.height
            return CGSize(width: 80, height: 100)
        }
        else if  collectionView == collectionOfAds
        {
            let width = self.collectionOfAds.frame.size.width/2
            let height = self.collectionOfAds.frame.size.height
            return CGSize(width: width, height: height)
        }
        else
        {
           // let width = collectionView.frame.size.width/3
          //  let height = collectionView.frame.size.height
            return CGSize(width: 120, height: 170)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == collectionOfCategory
        {
           /* if indexPath.row == 4
            {
                goToRootOfTab(index: 1)
            }
            else
            {*/
                let method = self.storyboard?.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
                method.dict = arrOfCategory[indexPath.row]
                method.isComeFromCategory = false
              //  method.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(method, animated: true)
          //  }
           
        }
        else if collectionView != collectionOfAds
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
            obj.dict = (arrOfLatestProduct[collectionView.tag]["arr_product"].arrayValue)[indexPath.row]
           // obj.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    //MARK:- Service
    
    func getHomeList()
    {
        let kRegiURL = "\(AddhirhamMainURL)homepage.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "siderId" :SliderIdKey,
                         "bannerId" :BannerIDKey] as [String : Any]
            
            print("parm \(param)")
            
          
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                        self.arrOfSlider = []
                        //self.arrOfSlider = json["bannerImages"].arrayValue
                        self.arrOfSlider = json["sliderImages"].arrayValue
                        self.setUpImageSlider()
                        self.arrOfCategory = []
                        self.arrOfCategory = json["categoryCollection"].arrayValue
                        self.collectionOfCategory.reloadData()
                        self.arrOfAds = []
                       // self.arrOfAds = json["sliderImages"].arrayValue
                        self.arrOfAds = json["bannerImages"].arrayValue
                        self.collectionOfAds.reloadData()
                        self.arrOfLatestProduct = []
                        var dict = JSON()
                        dict["name"] = "LATEST PRODUCTS"
                        dict["arr_product"] = JSON(json["latestProducts"].arrayValue)
                        self.arrOfLatestProduct.append(dict)
                        
                        dict = JSON()
                        dict["name"] = "MOBILE COLLECTIONS"
                        dict["arr_product"] = JSON(json["mobileCollection"].arrayValue)
                        self.arrOfLatestProduct.append(dict)
                        
                        for i in 0..<self.arrOfLatestProduct.count
                        {
                            var dict = self.arrOfLatestProduct[i]
                            var arrInner = dict["arr_product"].arrayValue
                            for j in 0..<arrInner.count
                            {
                                var dictInner = arrInner[j]
                                let arrQnt = self.SetupQuantityArray(qnt:dictInner["qty"].intValue)
                                dictInner["arrquantiy"] = JSON(arrQnt)
                                dictInner["current_index"] = 4
                                arrInner[j] = dictInner
                            }
                            dict["arr_product"] = JSON(arrInner)
                            self.arrOfLatestProduct[i] = dict
                        }
                        self.tblHomeCategory.reloadData()
                        self.lblTotalCount.text = strTotalCartItem
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}

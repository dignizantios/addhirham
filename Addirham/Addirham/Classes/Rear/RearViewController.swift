//
//  RearViewController.swift
//  Addirham
//
//  Created by Jaydeep Virani on 17/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import NVActivityIndicatorView
import KSToastView

class RearViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable
{
    //MARK:- Variable Declaration
    
     var arrOfRear:[JSON] = []
    // var dictLoggedUser = JSON()
    
    //MARK:- Outlet Zone
    
    @IBOutlet var leadingOfRear: NSLayoutConstraint!
    @IBOutlet var tblRear: UITableView!
    @IBOutlet var imgOfUserProfile: UIImageView!
    @IBOutlet var lblUserName: UILabel!
   
    
    //MARK:- ViewLife cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.leadingOfRear.constant = -self.view.frame.width
        self.imgOfUserProfile.clipsToBounds = true
        self.imgOfUserProfile.layer.cornerRadius = self.imgOfUserProfile.frame.height/2
        
       // NotificationCenter.default.addObserver(self, selector: #selector(dissmisSidemenu), name: NSNotification.Name(rawValue: "HideSideMenu"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        dictLoggedUser = getLoggedUserDetail()
        self.tabBarController?.tabBar.isHidden = true
        SetupRearArray()
        
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        dissmisViewAnimation(interval: 0.3, userInfo: "bye")
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presentViewAnimation(interval: 0.4)
    }
    
    //MARK:- Private Method
    func presentViewAnimation(interval:TimeInterval) {
        UIView.animate(withDuration: interval) {
            self.leadingOfRear.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    func dissmisViewAnimation(interval:TimeInterval,userInfo:String) {
        UIView.animate(withDuration: interval, animations: {
            self.leadingOfRear.constant = -self.view.frame.width
            self.tabBarController?.tabBar.isHidden = false
            self.view.layoutIfNeeded()
        }) { (completion) in
            if userInfo != "bye"
            {
              NotificationCenter.default.post(name: ksideMenu_Notification, object: userInfo)
            }
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @objc func dissmisSidemenu() {
        UIView.animate(withDuration: 0.3, animations: {
            self.leadingOfRear.constant = -self.view.frame.width
            self.view.layoutIfNeeded()
        }) { (completion) in
          //  NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "HideSideMenu"), object: nil)
            self.dismiss(animated: false, completion: nil)
            
        }
    }
    
   
    
    //MARK:- Setup Array
    @objc func SetupRearArray()
    {
        arrOfRear = []
        
        var dict = [String:String]()
        dict["menu_name"] = "HOME"
        dict["selected"] = "0"
        dict["menu_image"] = "ic_home_sidemenu.png"
        arrOfRear.append(JSON(dict))
        
        dict = [String:String]()
        dict["menu_name"] = "HOT DEALS"
        dict["selected"] = "0"
        dict["menu_image"] = "ic_top_deals_sidemenu.png"
        arrOfRear.append(JSON(dict))
       
        dict = [String:String]()
        dict["menu_name"] = "SHOP BY CATEGORIES"
        dict["selected"] = "0"
        dict["menu_image"] = "ic_shop_by_category_sidemenu.png"
        arrOfRear.append(JSON(dict))
        
        dict = [String:String]()
        dict["menu_name"] = "COMPARE"
        dict["selected"] = "1"
        dict["menu_image"] = "ic_compair_phone_unselected.png"
        arrOfRear.append(JSON(dict))
        
        dict = [String:String]()
        dict["menu_name"] = "ORDERS HISTORY"
        dict["selected"] = "0"
        dict["menu_image"] = "ic_order_history_sidemenu.png"
        arrOfRear.append(JSON(dict))
        
        dict = [String:String]()
        dict["menu_name"] = "SHOPPING GUIDE"
        dict["selected"] = "0"
        dict["menu_image"] = "ic_shoping_guide_sidemenu.png"
        arrOfRear.append(JSON(dict))
        
        dict = [String:String]()
        dict["menu_name"] = "WISH LIST"
        dict["selected"] = "1"
        dict["menu_image"] = "ic_wish_list_sidemenu.png"
        arrOfRear.append(JSON(dict))
        
        dict = [String:String]()
        dict["menu_name"] = "ABOUT"
        dict["selected"] = "0"
        dict["menu_image"] = "ic_about_us_new.png"
        arrOfRear.append(JSON(dict))
        
        dict = [String:String]()
        dict["menu_name"] = "PRIVACY POLICY"
        dict["selected"] = "0"
        dict["menu_image"] = "ic_privacy_policy_sidemenu.png"
        arrOfRear.append(JSON(dict))
        
        dict = [String:String]()
        dict["menu_name"] = "RETURNS & EXCHANGE"
        dict["selected"] = "0"
        dict["menu_image"] = "ic_return_sidemenu.png"
        arrOfRear.append(JSON(dict))
        
        dict = [String:String]()
        dict["menu_name"] = "GIFT VOUCHER"
        dict["selected"] = "1"
        dict["menu_image"] = "ic_gift_voucher_sidemenu.png"
        arrOfRear.append(JSON(dict))
        
        dict = [String:String]()
        dict["menu_name"] = "CONTACT US"
        dict["selected"] = "0"
        dict["menu_image"] = "ic_contact_us_sidemenu.png"
        arrOfRear.append(JSON(dict))
        
        dict = [String:String]()
        dict["menu_name"] = "FOLLOW US"
        dict["selected"] = "0"
        dict["menu_image"] = "ic_follow_us_sidemenu.png"
        arrOfRear.append(JSON(dict))
        
        if dictLoggedUser["customerId"].stringValue != ""
        {
            dict = [String:String]()
            dict["menu_name"] = "LOGOUT"
            dict["selected"] = "0"
            dict["menu_image"] = "ic_logout.png"
            arrOfRear.append(JSON(dict))
            
            self.lblUserName.text = "\(dictLoggedUser["firstname"].stringValue) \(dictLoggedUser["lastname"].stringValue)"
            if  let str = Defaults.value(forKey: "userProfilePic") as? String
            {
                self.imgOfUserProfile.sd_setImage(with: URL(string: str), placeholderImage: #imageLiteral(resourceName: "user_placeholder_register_screen"), options: .lowPriority, completed: nil)
            }
            else
            {
                self.imgOfUserProfile.image = #imageLiteral(resourceName: "user_placeholder_register_screen")
            }
            
        }
        
        self.tblRear.reloadData()
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnHideAction(_ sender: Any) {
        dissmisViewAnimation(interval: 0.3, userInfo: "cancel")
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        if dictLoggedUser["customerId"].stringValue == ""
        {
            dissmisViewAnimation(interval: 0.3, userInfo: "OrderHistory")
        }
        else
        {
            dissmisViewAnimation(interval: 0.3, userInfo: "ProfileViewController")
        }
    }
    //MARK:- TableView delagte Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfRear.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RearCell = self.tblRear.dequeueReusableCell(withIdentifier: "RearCell") as! RearCell
        let dict = arrOfRear[indexPath.row]
        cell.lblMenuName.text = dict["menu_name"].stringValue
        cell.imgOfMenu.image = UIImage.init(named:dict["menu_image"].stringValue )
        if dict["selected"].stringValue == "1"
        {
            cell.viewOfSep.isHidden = false
        }
        else
        {
            cell.viewOfSep.isHidden = true
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            dissmisViewAnimation(interval: 0.3, userInfo: "Home")
        }
        else if indexPath.row == 1
        {
            dissmisViewAnimation(interval: 0.3, userInfo: "TopDeal")
        }
        else if indexPath.row == 2
        {
            dissmisViewAnimation(interval: 0.3, userInfo: "CategoryViewController")
        }
        else if  indexPath.row == 3
        {
            if strComapareID.count == 0
            {
                dissmisViewAnimation(interval: 0.3, userInfo: "NoCompareItem")
            }
            else
            {
                dissmisViewAnimation(interval: 0.3, userInfo: "CompairViewController")
            }
            //
        }
        else if indexPath.row == 4
        {
            if dictLoggedUser["customerId"].stringValue == ""
            {
                dissmisViewAnimation(interval: 0.3, userInfo: "OrderHistory")
            }
            else
            {
                dissmisViewAnimation(interval: 0.3, userInfo: "OrderHistoryViewController")
            }
            
        }
        else if indexPath.row == 5
        {
            dissmisViewAnimation(interval: 0.3, userInfo: "ShoppingGuideViewController")
        }
        else if indexPath.row == 6
        {
            if dictLoggedUser["customerId"].stringValue == ""
            {
                dissmisViewAnimation(interval: 0.3, userInfo: "OrderHistory")
            }
            else
            {
                dissmisViewAnimation(interval: 0.3, userInfo: "wishlist")
            }
            
        }
        else if indexPath.row == 7
        {
            dissmisViewAnimation(interval: 0.3, userInfo: "AboutViewController")
        }
        else if indexPath.row == 8
        {
            dissmisViewAnimation(interval: 0.3, userInfo: "PrivacyViewController")
        }
        else if indexPath.row == 9
        {
            dissmisViewAnimation(interval: 0.3, userInfo: "ReturnExchangeViewController")
           // dissmisViewAnimation(interval: 0.3, userInfo: "CompairViewController")
            
        }
        else if indexPath.row == 10
        {
            dissmisViewAnimation(interval: 0.3, userInfo: "GiftVoucherViewController")
        }
        else if indexPath.row == 11
        {            
            dissmisViewAnimation(interval: 0.3, userInfo: "ContactUsViewController")
        }
        else if indexPath.row == 12
        {
            dissmisViewAnimation(interval: 0.3, userInfo: "FollowViewController")
        }
        else if indexPath.row == 13
        {
            let alertController = UIAlertController(title: AppName, message: "Are you sure want to logout?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                self.userLogout()
                
            }
            let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
                print("Cancel")
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK:- Service
    
    func userLogout()
    {
        let kRegiURL = "\(AddhirhamMainURL)customer.php"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["hashkey" :HashKey,
                         "type" :"logout",
                         "email":dictLoggedUser["email"].stringValue,
                         "defaultship":"TRUE"] as [String : Any]
            
            print("parm \(param)")
            
            
            startAnimating(Loadersize, message: "Loading...", type: NVActivityIndicatorType(rawValue:29))
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                //                dismiss
                self.stopAnimating()
                if let json = respones.result.value
                {
                    print("json \(json)")
                    if json["code"].stringValue == "100"
                    {
                        //print(json)
                       // Defaults.removeObject(forKey: "cartDetail")
                        guard let userDetail = UserDefaults.standard.value(forKey: "cartDetail") as? Data else { return }
                        var data = JSON(userDetail)
                        data["quote_id"] = "0"
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "cartDetail")
                        Defaults.removeObject(forKey: "userAddressDetail")
                        Defaults.removeObject(forKey: "userDetail")
                        Defaults.removeObject(forKey: "userProfilePic")
                        strTotalCartItem = "0"
                        Defaults.setValue(strTotalCartItem, forKey: "CartItem")
                        Defaults.synchronize()
                        dictLoggedUser = JSON()                        
                        self.dissmisViewAnimation(interval: 0.3, userInfo: "cancel")
                        self.goToRootOfTab(index: 0)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["message"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
}

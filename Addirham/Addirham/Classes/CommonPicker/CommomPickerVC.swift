//
//  CommomPickerVC.swift
//  GuideApp
//
//  Created by Jaydeep Virani on 06/10/17.
//  Copyright © 2017 Nami. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

public protocol CommonPickerDelegate
{
    func setName(strCode:String, strName:String ,type:Int,strStateCode:String)
}
enum PickerType: Int
{
    case countryPicker = 0
    case statePicker = 1
    case cityPicker = 2
}
class CommomPickerVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource
{
    
    
    //MARK:- Variable Declaration
    
    var PickerDelegate : CommonPickerDelegate?
    var arrPicker:[JSON] = []
    var pickerType = Int()
    
    //MARK:- Outlet Zone
   
    @IBOutlet var commonPickerView: UIPickerView!
    
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.commonPickerView.reloadAllComponents()
        
      
        // Do any additional setup after loading the view.
    }

    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- PickerView Delegate & Datasource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrPicker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        let dict = arrPicker[row]
        if pickerType == 0
        {
            return dict["countryName"].stringValue
        }
        return dict["name"].stringValue
       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("row \(arrPicker[row])")
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnDoneAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
        let dict = arrPicker[self.commonPickerView.selectedRow(inComponent: 0)]
        print("dict \(dict)")
        if pickerType == 0
        {
            PickerDelegate?.setName(strCode: dict["countryID"].stringValue, strName: dict["countryName"].stringValue,type: pickerType, strStateCode: "")
        }
        else
        {
            PickerDelegate?.setName(strCode: dict["state_id"].stringValue, strName: dict["name"].stringValue,type: pickerType, strStateCode:dict["state_code"].stringValue)
        }
        
       
    }
   
    @IBAction func btnCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

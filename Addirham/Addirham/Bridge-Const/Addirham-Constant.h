//
//  Addirham-Constant.h
//  Addirham
//
//  Created by Jaydeep Virani on 17/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

#ifndef Addirham_Constant_h
#define Addirham_Constant_h


//#define AddhirhamMainURL @"http://addirham.adworkstm.com/api/"
#define AddhirhamMainURL @"http://www.addirham.com/api/"
#define AddirhamPaymentURL @"http://www.addirham.com/ipay/ipay.php"
#define HashKey @"FpWLLg9RC958dsNICkXvyxbZZ189WA"
#define SliderIdKey @"1"
#define BannerIDKey @"8"
#define AppName @"Addirham"
#define InternetMessage @"No internet connection, please try again later"
#define RequestMsg @"Request time out"
#define WrongMsg @"Something went wrong, please try again later"
#define DeviceType @"1"

#endif /* Addirham_Constant_h */

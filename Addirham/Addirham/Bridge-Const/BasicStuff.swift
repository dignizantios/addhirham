//
//  BasicStuff.swift
//  Cool Drive
//
//  Created by Jaydeep Virani on 09/03/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import DropDown
import KSToastView

let mapping:StringMapping = StringMapping.shared()
var isEnglish:Bool = true
let Defaults = UserDefaults.standard
let Loadersize = CGSize(width: 30, height: 30)
var ToastDuration:TimeInterval = 2.0
let appDelagte = UIApplication.shared.delegate as! AppDelegate
var TotalQuantity:Int = 25
let ksideMenu_Notification = Notification.Name.init("dismissSidemenu")
var dictShippingCharge = JSON()
var strComapareID = [String]()
var strFilterCategoryId = [String]()
var strFilterColorId = [String]()
var strFilterMinPrice = String()
var strFilterMaxPrice = String()
var strTotalCartItem = String()
var refreshControl: UIRefreshControl!
var dictLoggedUser = JSON()
var dictGuestUserAddress = JSON()
var strAppStoreLink = "https://itunes.apple.com/us/app/addirham/id1358644597?ls=1&mt=8"


/*func setLangauge(language:String)
{    
    if language == Arabic
    {
        isEnglish = false
        Defaults.set(Arabic, forKey: "language")
        Defaults.set(1, forKey: "lang")
        Defaults.synchronize()
    }        
    else
    {
        isEnglish = true
        Defaults.set(English, forKey: "language")
        Defaults.set(0, forKey: "lang")
        Defaults.synchronize()
        
    }
    print("Language - ",Defaults.value(forKey: "language"));

    StringMapping.shared().setLanguage()
}*/


func isValidEmail(emailAddressString:String) -> Bool
{
    var returnValue = true
    let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
    
    do {
        let regex = try NSRegularExpression(pattern: emailRegEx)
        let nsString = emailAddressString as NSString
        let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
        
        if results.count == 0
        {
            returnValue = false
        }
        
    } catch let error as NSError {
        print("invalid regex: \(error.localizedDescription)")
        returnValue = false
    }
    
    return  returnValue
}

func getUserDetail(_ forKey: String) -> JSON
{
    guard let userDetail = UserDefaults.standard.value(forKey: "userDetail") as? Data else { return "" }
    let data = JSON(userDetail)
    return data
    
}
func getLoggedUserDetail() -> JSON
{
    let data:JSON = getUserDetail("customerdata")
    Defaults.setValue(data["customerdata"]["profile_picture"].stringValue, forKey: "userProfilePic")
    Defaults.synchronize()
    print("dictUser \(data)")
    print("customerdata \(data["customerdata"])")
    return data["customerdata"]
}

class BasicStuff: NSObject
{
    //MARK:- Alert
    
    
    
}

extension UIViewController
{
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName)
            print("Font Names = [\(names)]")
        }
    }
    
    func enableGesture()
    {
        sideMenuController?.isLeftViewSwipeGestureDisabled = true
        sideMenuController?.isRightViewSwipeGestureDisabled = true
    }
   
    func disableGesture()
    {
        sideMenuController?.isLeftViewSwipeGestureDisabled = true
        sideMenuController?.isRightViewSwipeGestureDisabled = false
    }
    //MARK:- Date Formatter
    
    func stringTodate(Formatter:String,strDate:String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
       // dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate = dateFormatter.date(from: strDate)!
        return FinalDate
    }
    
    func DateToString(Formatter:String,date:Date) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
     //   dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate:String = dateFormatter.string(from: date)
        return FinalDate
    }
    
    
    func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) -> Void
    {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert);
        let cancelAction = UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
            
        }
        let okAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
           
            
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        vc.present(alert, animated: true, completion: nil)
    }
    
    @objc func sideMenuAction(_ sender:UIButton) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.dismissSidemenu(_:)), name: ksideMenu_Notification, object: nil)
        let obj:RearViewController = self.storyboard?.instantiateViewController(withIdentifier: "RearViewController") as! RearViewController
        obj.modalPresentationStyle = .overCurrentContext
        self.present(obj, animated: false, completion: nil)
    }
    
    @objc func backAction(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissSidemenu(_ notification:Notification) {
        self.dismiss(animated: false, completion: nil)
        guard let userinfo = notification.object else {
            return
        }
        NotificationCenter.default.removeObserver(self, name: ksideMenu_Notification, object: nil)
        if userinfo as! String == "cancel"
        {
            return
        }
        else if userinfo as! String == "Home"
        {
            goToRootOfTab(index: 0)
            //appDelagte.tabBarController.selectedIndex = 0
            return
        }
        else if userinfo as! String == "TopDeal" {
            goToRootOfTab(index: 1)
          //  appDelagte.tabBarController.selectedIndex = 1
            return
        }
        else if userinfo as! String == "wishlist"
        {
            goToRootOfTab(index: 3)
            
          //  appDelagte.tabBarController.selectedIndex = 3
            return
        }
        else if userinfo as! String == "OrderHistory"
        {
            let alertController = UIAlertController(title: AppName, message: "You are not access this feature because you are not login!!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Login", style: .default) { (action) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let obj = storyboard.instantiateViewController(withIdentifier: "ShippingAddressViewController") as! ShippingAddressViewController
              //  obj.modalPresentationStyle = .overCurrentContext
              //  obj.modalTransitionStyle = .crossDissolve
                self.present(obj, animated: true, completion: nil)
                //self.dissmisViewAnimation(interval: 0.3, userInfo: "cancel")
            }
            alertController.addAction(okAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true)
            return
        }
        else if userinfo as! String == "NoCompareItem"
        {
            KSToastView.ks_showToast("Please select any two item for comparison", duration: ToastDuration)
            return
        }
        let controllerName = userinfo as! String
        if controllerName == "PrivacyViewController"
        {
            let obj =  self.storyboard?.instantiateViewController(withIdentifier: "ShoppingGuideViewController") as! ShoppingGuideViewController
            obj.strScreenTitle = "Privacy policy"
            obj.strSubtitle = "Privacy policy of Addirham"
            obj.strDescription = "Privacy policy"
            obj.strId = "privacy_policy"
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if controllerName == "ReturnExchangeViewController"
        {
            let obj =  self.storyboard?.instantiateViewController(withIdentifier: "ShoppingGuideViewController") as! ShoppingGuideViewController
            obj.strScreenTitle = "Return & Exchange"
            obj.strSubtitle = "Return & Exchange of Addirham"
            obj.strDescription = "Return & Exchange"
            obj.strId = "return_and_Exchanges"
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if controllerName == "GiftVoucherViewController"
        {
            let obj =  self.storyboard?.instantiateViewController(withIdentifier: "ShoppingGuideViewController") as! ShoppingGuideViewController
            obj.strScreenTitle = "Gift Voucher"
            obj.strSubtitle = "Gift Voucher of Addirham"
            obj.strDescription = "Gift Voucher"
            obj.strId = "gift_voucher"
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if controllerName == "FollowViewController"
        {
            let obj =  self.storyboard?.instantiateViewController(withIdentifier: "ShoppingGuideViewController") as! ShoppingGuideViewController
            obj.strScreenTitle = "Follow us"
            obj.strSubtitle = "Follow us"
            obj.strDescription = "Follow us"
            obj.strId = "privacy_policy"
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if controllerName == "ShoppingGuideViewController"
        {
            let obj =  self.storyboard?.instantiateViewController(withIdentifier: "ShoppingGuideViewController") as! ShoppingGuideViewController
            obj.strScreenTitle = "Shopping Guide"
            obj.strSubtitle = "Shopping Guide of Addirham"
            obj.strDescription = "Shopping Guide"
            obj.strId = "shopping_guide"
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else
        {
             self.navigationController?.pushViewController(getSidemenuController(controllerName: controllerName), animated: true)
        }
       
    }
    func goToRootOfTab(index: Int) {
        
        // pop to root if navigated before
        
        if let nav = appDelagte.tabBarController.viewControllers?[appDelagte.tabBarController.selectedIndex] as? UINavigationController {
            nav.popToRootViewController(animated: true)
        }
        
        appDelagte.tabBarController.tabBar.isHidden = false
        appDelagte.tabBarController.selectedIndex = index
        
    }
    func getSidemenuController(controllerName:String) -> UIViewController
    {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: controllerName)
        return controller!
    }
    
    //MARK:- Get UIColor From HEX
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor
    {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //MARK:- Store User Data
    
    func getCartDetail(_ forKey: String) -> String{
        guard let userDetail = UserDefaults.standard.value(forKey: "cartDetail") as? Data else { return "" }
        let data = JSON(userDetail)
        return data[forKey].stringValue
    }
    
    func getUserAddressDetail(_ forKey: String) -> String
    {
        guard let userDetail = UserDefaults.standard.value(forKey: "userAddressDetail") as? Data else { return "" }
        let data = JSON(userDetail)
        return data[forKey].stringValue
    }
    
    func removeFilterData()
    {
        strFilterCategoryId = [String]()
        strFilterColorId = [String]()
        strFilterMinPrice = ""
        strFilterMaxPrice = ""
    }
    
    func getAddressDict(_ forKey: String) -> JSON
    {
        guard let userDetail = UserDefaults.standard.value(forKey: "cartDetail") as? Data else { return "" }
        let data = JSON(userDetail)
        return data
        
    }
    
    //MARK:- DropDown Setup
    
    func showDropDown(dropDown : DropDown,sender : UIButton)
    {
        dropDown.show()
    }
    func configDropDown(dropdown: DropDown, sender: UIButton,width:CGFloat)
    {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: 45)
        dropdown.width = width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.selectionBackgroundColor = .clear
        dropdown.cancelAction = { [unowned self] in
            print("Drop down dismissed")
        }
        
    }
    func dropDownDataSource(dropdown: DropDown,delegate : CommonDropDownDelegate,array:[String],type:Int)
    {
        
        dropdown.dataSource = array
        dropdown.selectionAction = { (index, item) in
            
            print("Selected value - ",array[index])
            delegate.setSelectedSortValue(type:array[index],index:index,array: array,arrType:type)
        }
    }
    //MARK:- Get Country Code
    
    func getCountryPhonceCode (_ country : String) -> String
    {
        var countryDictionary  = ["AF":"93",
                                  "AL":"355",
                                  "DZ":"213",
                                  "AS":"1",
                                  "AD":"376",
                                  "AO":"244",
                                  "AI":"1",
                                  "AG":"1",
                                  "AR":"54",
                                  "AM":"374",
                                  "AW":"297",
                                  "AU":"61",
                                  "AT":"43",
                                  "AZ":"994",
                                  "BS":"1",
                                  "BH":"973",
                                  "BD":"880",
                                  "BB":"1",
                                  "BY":"375",
                                  "BE":"32",
                                  "BZ":"501",
                                  "BJ":"229",
                                  "BM":"1",
                                  "BT":"975",
                                  "BA":"387",
                                  "BW":"267",
                                  "BR":"55",
                                  "IO":"246",
                                  "BG":"359",
                                  "BF":"226",
                                  "BI":"257",
                                  "KH":"855",
                                  "CM":"237",
                                  "CA":"1",
                                  "CV":"238",
                                  "KY":"345",
                                  "CF":"236",
                                  "TD":"235",
                                  "CL":"56",
                                  "CN":"86",
                                  "CX":"61",
                                  "CO":"57",
                                  "KM":"269",
                                  "CG":"242",
                                  "CK":"682",
                                  "CR":"506",
                                  "HR":"385",
                                  "CU":"53",
                                  "CY":"537",
                                  "CZ":"420",
                                  "DK":"45",
                                  "DJ":"253",
                                  "DM":"1",
                                  "DO":"1",
                                  "EC":"593",
                                  "EG":"20",
                                  "SV":"503",
                                  "GQ":"240",
                                  "ER":"291",
                                  "EE":"372",
                                  "ET":"251",
                                  "FO":"298",
                                  "FJ":"679",
                                  "FI":"358",
                                  "FR":"33",
                                  "GF":"594",
                                  "PF":"689",
                                  "GA":"241",
                                  "GM":"220",
                                  "GE":"995",
                                  "DE":"49",
                                  "GH":"233",
                                  "GI":"350",
                                  "GR":"30",
                                  "GL":"299",
                                  "GD":"1",
                                  "GP":"590",
                                  "GU":"1",
                                  "GT":"502",
                                  "GN":"224",
                                  "GW":"245",
                                  "GY":"595",
                                  "HT":"509",
                                  "HN":"504",
                                  "HU":"36",
                                  "IS":"354",
                                  "IN":"91",
                                  "ID":"62",
                                  "IQ":"964",
                                  "IE":"353",
                                  "IL":"972",
                                  "IT":"39",
                                  "JM":"1",
                                  "JP":"81",
                                  "JO":"962",
                                  "KZ":"77",
                                  "KE":"254",
                                  "KI":"686",
                                  "KW":"965",
                                  "KG":"996",
                                  "LV":"371",
                                  "LB":"961",
                                  "LS":"266",
                                  "LR":"231",
                                  "LI":"423",
                                  "LT":"370",
                                  "LU":"352",
                                  "MG":"261",
                                  "MW":"265",
                                  "MY":"60",
                                  "MV":"960",
                                  "ML":"223",
                                  "MT":"356",
                                  "MH":"692",
                                  "MQ":"596",
                                  "MR":"222",
                                  "MU":"230",
                                  "YT":"262",
                                  "MX":"52",
                                  "MC":"377",
                                  "MN":"976",
                                  "ME":"382",
                                  "MS":"1",
                                  "MA":"212",
                                  "MM":"95",
                                  "NA":"264",
                                  "NR":"674",
                                  "NP":"977",
                                  "NL":"31",
                                  "AN":"599",
                                  "NC":"687",
                                  "NZ":"64",
                                  "NI":"505",
                                  "NE":"227",
                                  "NG":"234",
                                  "NU":"683",
                                  "NF":"672",
                                  "MP":"1",
                                  "NO":"47",
                                  "OM":"968",
                                  "PK":"92",
                                  "PW":"680",
                                  "PA":"507",
                                  "PG":"675",
                                  "PY":"595",
                                  "PE":"51",
                                  "PH":"63",
                                  "PL":"48",
                                  "PT":"351",
                                  "PR":"1",
                                  "QA":"974",
                                  "RO":"40",
                                  "RW":"250",
                                  "WS":"685",
                                  "SM":"378",
                                  "SA":"966",
                                  "SN":"221",
                                  "RS":"381",
                                  "SC":"248",
                                  "SL":"232",
                                  "SG":"65",
                                  "SK":"421",
                                  "SI":"386",
                                  "SB":"677",
                                  "ZA":"27",
                                  "GS":"500",
                                  "ES":"34",
                                  "LK":"94",
                                  "SD":"249",
                                  "SR":"597",
                                  "SZ":"268",
                                  "SE":"46",
                                  "CH":"41",
                                  "TJ":"992",
                                  "TH":"66",
                                  "TG":"228",
                                  "TK":"690",
                                  "TO":"676",
                                  "TT":"1",
                                  "TN":"216",
                                  "TR":"90",
                                  "TM":"993",
                                  "TC":"1",
                                  "TV":"688",
                                  "UG":"256",
                                  "UA":"380",
                                  "AE":"971",
                                  "GB":"44",
                                  "US":"1",
                                  "UY":"598",
                                  "UZ":"998",
                                  "VU":"678",
                                  "WF":"681",
                                  "YE":"967",
                                  "ZM":"260",
                                  "ZW":"263",
                                  "BO":"591",
                                  "BN":"673",
                                  "CC":"61",
                                  "CD":"243",
                                  "CI":"225",
                                  "FK":"500",
                                  "GG":"44",
                                  "VA":"379",
                                  "HK":"852",
                                  "IR":"98",
                                  "IM":"44",
                                  "JE":"44",
                                  "KP":"850",
                                  "KR":"82",
                                  "LA":"856",
                                  "LY":"218",
                                  "MO":"853",
                                  "MK":"389",
                                  "FM":"691",
                                  "MD":"373",
                                  "MZ":"258",
                                  "PS":"970",
                                  "PN":"872",
                                  "RE":"262",
                                  "RU":"7",
                                  "BL":"590",
                                  "SH":"290",
                                  "KN":"1",
                                  "LC":"1",
                                  "MF":"590",
                                  "PM":"508",
                                  "VC":"1",
                                  "ST":"239",
                                  "SO":"252",
                                  "SJ":"47",
                                  "SY":"963",
                                  "TW":"886",
                                  "TZ":"255",
                                  "TL":"670",
                                  "VE":"58",
                                  "VN":"84",
                                  "VG":"284",
                                  "VI":"340"]
        if countryDictionary[country] != nil {
            return countryDictionary[country]!
        }
            
        else {
            return ""
        }
        
    }
    
    //MARK: - Private Method
    
  
}


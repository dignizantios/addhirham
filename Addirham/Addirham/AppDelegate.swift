//
//  AppDelegate.swift
//  Addirham
//
//  Created by Jaydeep Virani on 17/10/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import SplunkMint
import Firebase
import UserNotifications
import FirebaseMessaging
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UINavigationControllerDelegate,UNUserNotificationCenterDelegate,MessagingDelegate
{

    //MARK:- Variable declaration
    
    var tabBarController:UITabBarController = TabBarController()
    var tabNavigation:UINavigationController = UINavigationController()
    var window: UIWindow?
    
    //MARK:- Appdelegate Method
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
     //   Mint.sharedInstance().initAndStartSession(withAPIKey: "13694e3d")
        
        FirebaseApp.configure()
       
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                print("granted:==\(granted)")
                if granted {
                    
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.registerForRemoteNotifications()
                    })
                }
                else {
                    
                }
            }
        }
        else
        {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        
        Messaging.messaging().delegate = self
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if launchedBefore
        {
            if let obj = Defaults.value(forKey: "CartItem") as? String
            {
               strTotalCartItem = obj
            }
            else{
                strTotalCartItem = "0"
            }
            
            print("Not first launch.")
        } else {
            print("First launch, setting UserDefault.")
            var data = JSON()
            data["quote_id"] = "0"
            guard let rowdata = try? data.rawData() else {return false}
            Defaults.setValue(rowdata, forKey: "cartDetail")
           
            UserDefaults.standard.set(true, forKey: "launchedBefore")
            Defaults.setValue("0", forKey: "CartItem")
            strTotalCartItem = "0"
            Defaults.synchronize()
        }
        /*let method = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        var navigationController = NavigationController(rootViewController: method)
        navigationController.isNavigationBarHidden = true
        let Home = MainViewController()
        Home.rootViewController = navigationController
        Home.setup(type: 6)
        
        let topDeal = storyboard.instantiateViewController(withIdentifier: "TopDealViewController") as! TopDealViewController
        navigationController = NavigationController(rootViewController: topDeal)
        navigationController.isNavigationBarHidden = true
        let myTopDeal = MainViewController()
        myTopDeal.rootViewController = navigationController
        myTopDeal.setup(type: 6)
        
        let cart = storyboard.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
        navigationController = NavigationController(rootViewController: cart)
        navigationController.isNavigationBarHidden = true
        let myCart = MainViewController()
        myCart.rootViewController = navigationController
        myCart.setup(type: 6)
        
        let wishList = storyboard.instantiateViewController(withIdentifier: "WishListViewController") as! WishListViewController
        navigationController = NavigationController(rootViewController: wishList)
        navigationController.isNavigationBarHidden = true
        let myWishList = MainViewController()
        myWishList.rootViewController = navigationController
        myWishList.setup(type: 6)
        
        let Profile = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        navigationController = NavigationController(rootViewController: Profile)
        navigationController.isNavigationBarHidden = true
        let myProfile = MainViewController()
        myProfile.rootViewController = navigationController
        myProfile.setup(type: 6)*/
        
        self.window! = UIWindow(frame: UIScreen.main.bounds)
        
        UITabBar.appearance().tintColor = UIColor.white
        
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor(red: 90/255.0, green: 43/255.0, blue:123/255.0, alpha: 1.0)], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 255/255.0, green: 255/255.0, blue:255/255.0, alpha: 1.0)], for: .selected)
        
       /* var tabFrame = self.tabBarController.tabBar.frame
        //self.TabBar is IBOutlet of your TabBar
        tabFrame.size.height = 48
        tabFrame.origin.y = self.window!.frame.size.height - 48
        
       // self.tabBarController.tabBar.backgroundColor = UIColor.red
        self.tabBarController.tabBar.frame = tabFrame
        self.tabBarController.tabBar.autoresizesSubviews = false
        self.tabBarController.tabBar.clipsToBounds = true*/
        
        let tabBarItemSize = CGSize(width: (UIScreen.main.bounds.size.width / 5),
                                    height: 48)
        print("tabBarItemSize : \(tabBarItemSize)")
        //self.tabBarController.tabBar.selectionIndicatorImage = imageWithColor(color: UIColor(red: 90/255.0, green: 43/255.0, blue:123/255.0, alpha: 1.0), size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.zero)
        self.tabBarController.tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor(red: 90/255.0, green: 43/255.0, blue:123/255.0, alpha: 1.0), size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.zero)
        self.tabBarController.tabBar.frame.size.width = self.window!.frame.width + 4
        self.tabBarController.tabBar.frame.origin.x = -2
       
        tabNavigation = UINavigationController(rootViewController: self.tabBarController)
        tabNavigation.isNavigationBarHidden = true
        self.window!.rootViewController = tabNavigation
        self.window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK:- Push
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
      
        
        print("User Info = ",notification.request.content.userInfo)
        
        completionHandler([.alert, .badge, .sound])
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        
        print("Device Token = ", token)
        
        UserDefaults.standard.set(token, forKey: "device_token")
        UserDefaults.standard.synchronize()
        
        Messaging.messaging().apnsToken = deviceToken
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("ERROR : \(error)")
    }
    
    // While Banner Tap and App in Background mode..
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Tap on Banner Push for ios 10 version")        
        
        
        completionHandler()
    }
    
    //MARK: - Firebase Messeging delegate methods
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        connectToFcm()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    
    func connectToFcm() {
        // Won't connect since there is no token
        guard InstanceID.instanceID().token() != nil else {
            return
        }
        
        // Disconnect previous FCM connection if it exists.
        Messaging.messaging().shouldEstablishDirectChannel = false
        
        Messaging.messaging().connect() { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
  

}



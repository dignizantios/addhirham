//
//  CommonDropDown.swift
//  Addirham
//
//  Created by Jaydeep on 24/01/18.
//  Copyright © 2018 Jaydeep Virani. All rights reserved.
//

import Foundation

public protocol CommonDropDownDelegate
{
    func setSelectedSortValue(type:String,index:Int,array:[String],arrType:Int)
}
